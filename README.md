# Weakly symmetric stress spaces in NGSolve

This repository provides a C++ implementation of the [AFW](http://dx.doi.org/10.1090/S0025-5718-07-01998-9) and [GG](http://dx.doi.org/10.1093/imanum/drq047) stress finite element spaces in 2 and 3 dimensions using the [NGSolve](https://ngsolve.org) library. Further facilities to experiment with mixed methods for the elastostatic and elastodynamic equations are provided using python 3 drivers. The symmetry of the stress tensor is handled weakly. Many types  of boundary conditions and material properties can be handled. 

CONTRIBUTORS: Jay Gopalakrishnan, Paulina Sepúlveda

### Install ###


* Clone this repository: `git clone  https://<yourusername>@bitbucket.org/jayggg/elasticity.git`.
* Do make sure you have the dependencies installed before proceeding: 
    - A working installation of NGSolve and Netgen is required.  Please follow the [instructions online](https://ngsolve.org) for installing the development version of these packages. (Please ensure that the compile script `ngscxx` is in your path after a successful install of NGSolve.)
    - Python packages as `numpy`, `scipy`.

*  Type `make` inside the cloned folder `elasticity`. This should compile the C++ files on Linux or Mac systems (where GNU or other `make` is already installed) and should create the shared library called `libelas.so`.

* This shared library can be added onto the [NGSolve](http://sourceforge.net/projects/ngsolve/) package. To see how, please look at the python drivers in `py` folder.


### Contents ###

Folder structure: 

- [cpp](cpp):  Contains C++ code that implemens AFW and GG finite element spaces (and other needed auxiliary classes) in 2 and 3 dimensions as derived classes of NGSolve's `FiniteElement` class.
- [py](py): Contains Python drivers into the C++ codebase. These files handle several elasticity problems, with different boundary conditions, incompressible materials, elastostatics and elastodynamics, mixed methods with and without hybridization, diferent time stepping schemes, and convergence studies.  
- [tests](tests): Contains python files with examples to test the spaces and several elasticity problems with given exact solutions. Run all tests in this folder by navigating to this folder and typing `py.test *.py`. 


### Gallery ###


- See [beam.py](py/beam.py) for an example of 3D GG elements applied to a cantilever beam: ![picture](figs/beam.png)

- See [flume.py](projects/flume/flume.py) for 2D elastodynamics: ![flume.gif](figs/flume.gif)