"""
 Testing the finite element space
 Project over the GG space
    (s,q) = (S,q) for all q in GGspace
 for some given S.
"""
import ngsolve as ng
from netgen.geom2d import unit_square
from ctypes import CDLL

mylngs = CDLL("../libelas.so")
ng.ngsglobals.msg_level=1
mesh = ng.Mesh(unit_square.GenerateMesh(maxh=0.2))

x = ng.x
y = ng.y

V = ng.FESpace("ggfes", mesh, order=4, dirichlet="right|left")
sigma = V.TrialFunction()
tau = V.TestFunction()

# Give a matrix coefficient function as a 4-vector
#   S = [S[0] S[1]]
#       [S[2] S[3]]
S = ng.CoefficientFunction((0, 1 - 2 * x, 0, 0), dims=(2, 2))

# Project S into GG finite element space
a = ng.BilinearForm(V, symmetric = True, eliminate_internal = True)
a += ng.SymbolicBFI(ng.InnerProduct(sigma, tau))

f = ng.LinearForm(V)
f += ng.SymbolicLFI(ng.InnerProduct(S, tau))

s = ng.GridFunction(V)
c = ng.Preconditioner(a, type="direct")
a.Assemble()
f.Assemble()

bvp = ng.BVP(bf=a, lf=f, gf=s, pre=c, maxsteps=10).Do()

err = ng.sqrt(ng.Integrate(ng.InnerProduct(s - S, s - S), mesh))
print(" || s - S || = ", err)

# Plot of rows of matrix function s
row2 = ng.CoefficientFunction((s[2], s[3]))
ng.Draw(row2, mesh, "row2")
row1 = ng.CoefficientFunction((s[0], s[1]))
ng.Draw(row1, mesh, "row1")

# Visualizing matrix operations on s
n = ng.CoefficientFunction((1, -1))
snn = ng.InnerProduct(s * n, n)
ng.Draw(s * n, mesh, "sn")  # Visualize s*n
ng.Draw(snn, mesh, "snn")       # Add s*n.n to visual menu


def test_proj():
    success = (err < 1.e-11)
    msg = 'Projection error of a linear function = %g != 0' % err
    assert success, msg
