""" Testing that the bubbles have
 vanishing tangential components.

"""
from netgen.csg import Plane, Vec, Pnt, CSGeometry
import ngsolve as ng
from ctypes import CDLL

ng.ngsglobals.msg_level = 1
mylngs = CDLL("../libelas.so")

# a mesh with just one tetrahedron (not the unit tetrahedron)
lft = Plane(Pnt(-1, 0, 0), Vec(-1, 0, 0))
fnt = Plane(Pnt(0, 0, 0), Vec(0, -1, 0))
bot = Plane(Pnt(0, 0, 0), Vec(0, 0, -1))
top = Plane(Pnt(0, 0, 2), Vec(1 / ng.sqrt(3), 1 / ng.sqrt(3), 1 / ng.sqrt(3)))
tetrahedron = CSGeometry()
tetrahedron.Add(fnt * bot * lft * top)
mesh = ng.Mesh(tetrahedron.GenerateMesh(maxh=4))

k = 2

V = ng.FESpace("ggfes3d", mesh, order=k)  # flags={'discontinuous':True})
n = ng.specialcf.normal(mesh.dim)
u1 = V.TrialFunction()
u2 = V.TestFunction()

u1n = ng.CoefficientFunction((u1[0] * n[0] + u1[1] * n[1] + u1[2] * n[2],
                              u1[3] * n[0] + u1[4] * n[1] + u1[5] * n[2],
                              u1[6] * n[0] + u1[7] * n[1] + u1[8] * n[2]))

u2n = ng.CoefficientFunction((u2[0] * n[0] + u2[1] * n[1] + u2[2] * n[2],
                              u2[3] * n[0] + u2[4] * n[1] + u2[5] * n[2],
                              u2[6] * n[0] + u2[7] * n[1] + u2[8] * n[2]))

a = ng.BilinearForm(V)
a += ng.SymbolicBFI(u1n * u2n, element_boundary=True)
a.Assemble()

i, j, v = a.mat.COO()

# z = array of sigma dofs for which sigma * n = 0 on tet bdry
z = []
for s in range(0, i.__len__()):
    if i[s] == j[s]:
        if v[s] < 10e-10:
            # print("i, j, v ", i[s], " ", j[s], " ", v[s])
            z.append(j[s])

bubbles = int(3 * (k + 1) * (k + 2) / 2)          # extra GG bubbles
interiors = int(3 * (k - 1) * (k + 1) * (k + 2) / 2)  # H(div) bubbles

print("==============================================")
print("  number of bubble functions addded is ", bubbles)
print("  total number of zero entries, only counting added bubbles ")
print("  size = ", len(z) - interiors)


def test_checknormal3d():
    success = (len(z) == bubbles + interiors)
    msg = '#{bubbles in GGtet} not equal to #{shapes with sigma n = 0} !'
    assert success, msg
