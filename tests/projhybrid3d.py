"""
 This is for testing the 3D hybrid stress finite element space:

 Project a given S to the GG space using the hybrid formulation
    (s,r) + <l, r.n> = (S,r), for all r in discontinuous GGspace,
            <s.n, m> = 0,     for all m in facet space.
"""
import ngsolve as ng
from ngsolve import InnerProduct, SymbolicBFI, x, y, z
from ctypes import CDLL
from netgen.csg import unit_cube

mylngs = CDLL("../libelas.so")
ng.ngsglobals.msg_level = 1
ng.SetHeapSize(int(1e8))


p = 2  # Polynomial order

""" Give a matrix coefficient function to project as a 9-vector
   S = [S[0] S[1] S[2] ]
       [S[3] S[4] S[5] ]
       [S[6] S[7] S[8] ]
 If S consists of polynomials of degree at most p, then the
"""

# Projection error should be machine zero
S = ng.CoefficientFunction((x*x, 1-2*x*y, z*(1-z),
                            x*(1-x), x*y, z*(1-z),
                            2*x*(1-x), 1-2*x*y+z, z*(1-z)*3), dims=(3, 3))

mesh = ng.Mesh(unit_cube.GenerateMesh(maxh=0.5))
VV = ng.FESpace("ggfes3d", mesh, order=p, flags={'discontinuous': True})
L = ng.FacetFESpace(mesh, order=p, dirichlet=[1, 2, 3, 4, 5, 6])
VL = ng.FESpace([VV, L, L, L])

s, l1, l2, l3 = VL.TrialFunction()
r, m1, m2, m3 = VL.TestFunction()
n = ng.specialcf.normal(mesh.dim)
l = ng.CoefficientFunction((l1, l2, l3))
m = ng.CoefficientFunction((m1, m2, m3))

# Project S into GG finite element space
a = ng.BilinearForm(VL, symmetric=True, eliminate_internal=True)
a += ng.SymbolicBFI(InnerProduct(s, r))
a += ng.SymbolicBFI(l*(r*n), element_boundary=True)
a += ng.SymbolicBFI(m*(s*n), element_boundary=True)

f = ng.LinearForm(VL)
f += ng.SymbolicLFI(InnerProduct(S, r))

sl = ng.GridFunction(VL)
c = ng.Preconditioner(a, type="direct")
a.Assemble()
f.Assemble()

bvp = ng.BVP(bf=a, lf=f, gf=sl, pre=c, maxsteps=10).Do()

sh = ng.CoefficientFunction(sl.components[0])
err = ng.sqrt(ng.Integrate(InnerProduct(sh-S, sh-S), mesh))
print(" || sh - S || = ", err)

# Use clipping option to see:
ng.Draw(sh, mesh, "sh1", draw_surf=False)


def test_projhybrid3d():
    success = (err < 1.e-11)
    msg = 'Hybrid projection error = %g != 0' % err
    assert success, msg
