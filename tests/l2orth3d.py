"""
This file is to test the l2orth space in 3D.

Consists of polynomials of degree k orthogonal to
polynomial of degree k-1 in the reference element.

Note that setting a function of degree k-1, ( in the tetraedral)
the plot will be zero function.

We are testing again all the polynomials of degree k-1 in
the reference tetrahedron,
thus the norm of the mass matrix "a" should be zero.

Important! If this test fails for some version of Ngsolve,
then we need to modify the basis of l2orthfes3d space.

"""
import ngsolve as ng
from netgen.csg import Plane, Pnt, Vec, CSGeometry
from ctypes import CDLL

mylngs = CDLL("../libelas.so")
ng.ngsglobals.msg_level = 1

# reference element tetrahedron
lft = Plane(Pnt(0, 0, 0), Vec(-1, 0, 0))
fnt = Plane(Pnt(0, 0, 0), Vec(0, -1, 0))
bot = Plane(Pnt(0, 0, 0), Vec(0, 0, -1))
top = Plane(Pnt(0, 0, 1), Vec(1 / ng.sqrt(3), 1 / ng.sqrt(3), 1 / ng.sqrt(3)))

tetrahedron = CSGeometry()
tetrahedron.Add(lft * fnt * bot * top)

mesh = ng.Mesh(tetrahedron.GenerateMesh(maxh=1))

k = 5

x = ng.x
y = ng.y
z = ng.z

S = ng.FESpace("l2orthfes3d", mesh, order=k)
L = ng.L2(mesh, order=k - 1)
X = ng.FESpace([S, L])

upp, up = X.TrialFunction()
vpp, vp = X.TestFunction()

a = ng.BilinearForm(X)
a += ng.SymbolicBFI(upp * vp)
a.Assemble()

u = ng.GridFunction(S)
u.Set(x * x)
ng.Draw(u)
err = ng.Norm(a.mat.AsVector())
print(" Norm of a.mat is: ", err)


def test_l2orth3d():
    success = (err < 1.e-11)
    msg = 'Please check the basis of l2orth3d space!'
    assert success, msg
