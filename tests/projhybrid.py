"""
This is for testing the hybrid finite element space in 2D:

Give a matrix coefficient function to project as a 4-vector
S = [S[0] S[1]]
    [S[2] S[3]]
such that  Sn = 0 on left and right boundaries.

Project it to the GG space using the hybrid formulation
(s,r) + <l, r.n> = (S,r), for all r in discontinuous GGspace,
<s.n, m> = 0,     for all m in facet space.

"""
import ngsolve as ng
from netgen.geom2d import unit_square
from ctypes import CDLL

ng.ngsglobals.msg_level = 1
mylngs = CDLL("../libelas.so")
mesh = ng.Mesh(unit_square.GenerateMesh(maxh=0.2))

x = ng.x
y = ng.y

S = ng.CoefficientFunction((x * x * (1 - x) * (1 - x),
                            1 - 2 * x * y * y,
                            x * (1 - x),
                            y * x * x * y), dims=(2, 2))

p = 4
VV = ng.FESpace("ggfes", mesh, order=p, flags={'discontinuous': True})
L = ng.FacetFESpace(mesh, order=p, dirichlet="bottom|top")
VL = ng.FESpace([VV, L, L])

s, l1, l2 = VL.TrialFunction()
r, m1, m2 = VL.TestFunction()
n = ng.specialcf.normal(mesh.dim)
l = ng.CoefficientFunction((l1, l2))
m = ng.CoefficientFunction((m1, m2))


# Project S into GG finite element space
a = ng.BilinearForm(VL, symmetric = True, eliminate_internal = True)
a += ng.SymbolicBFI(ng.InnerProduct(s, r))
# a += ng.SymbolicBFI(l1 * (r[0] * n[0] + r[1] * n[1]) +
#                     l2 * (r[2] * n[0] + r[3] * n[1]), element_boundary=True)
# a += ng.SymbolicBFI(m1 * (s[0] * n[0] + s[1] * n[1]) +
#                     m2 * (s[2] * n[0] + s[3] * n[1]), element_boundary=True)
a += ng.SymbolicBFI(l * (r*n), element_boundary=True)
a += ng.SymbolicBFI((s*n) * m, element_boundary=True)

f = ng.LinearForm(VL)
f += ng.SymbolicLFI(ng.InnerProduct(S, r))

sl = ng.GridFunction(VL)
c = ng.Preconditioner(a, type="direct")
a.Assemble()
f.Assemble()

bvp = ng.BVP(bf=a, lf=f, gf=sl, pre=c, maxsteps=10).Do()

sh = sl.components[0]
err = ng.sqrt(ng.Integrate(ng.InnerProduct(sh - S, sh - S), mesh))
print(" || sh - S || = ", err)

ng.Draw(sh[1], mesh, "sh1")


def test_projybrid():
    success = (err < 1.e-11)
    msg = 'Hybrid projection error of degree 4 pol in GG(4) = %g != 0' % err
    assert success, msg
