"""
 Testing the finite element space in 3D

 Project over the GG space
    (s,q) = (S,q) for all q in GGspace
 for some given S.

"""
from netgen.csg import unit_cube
import ngsolve as ng
from ctypes import CDLL

mylngs = CDLL("../libelas.so")
ng.SetHeapSize(int(1e8))
ng.ngsglobals.msg_level = 1

mesh = ng.Mesh(unit_cube.GenerateMesh(maxh=0.5))

x = ng.x
y = ng.y
z = ng.z

V = ng.FESpace("ggfes3d", mesh, order=4, dirichlet='back|front|bottom|top')

# Note the boundaries of built-in unit cube:
#   Face at x=0; dirichlet='back' ;  dirichlet=[1]
#   Face at x=1; dirichlet='front';  dirichlet=[3]
#   Face at y=0; dirichlet='left' ;  dirichlet=[2]
#   Face at y=1; dirichlet='right';  dirichlet=[4]
#   Face at z=0; dirichlet='bottom'; dirichlet=[5]
#   Face at z=1; dirichlet='top';    dirichlet=[6]

sigma = V.TrialFunction()
tau = V.TestFunction()

# Give a matrix coefficient function as a 4-vector
#   S = [S[0] S[1] S[2]]
#       [S[3] S[4] S[5]]
#       [S[6] S[7] S[8]]
#
S = ng.CoefficientFunction((0, 1 - 2 * x, 0,
                            0, 0, 0,
                            0, 0, 0), dims = (3, 3))

# Project S into GG finite element space
a = ng.BilinearForm(V, symmetric = True, eliminate_internal = True)
a += ng.SymbolicBFI(ng.InnerProduct(sigma, tau))
#    NOTE:  Need InnerProduct to get Frobenius inner product
#           (sigma * tau means matrix multiplication of sigma with tau!)

f = ng.LinearForm(V)
f += ng.SymbolicLFI(ng.InnerProduct(S, tau))

s = ng.GridFunction(V)
c = ng.Preconditioner(a, type="direct")
a.Assemble()
f.Assemble()

bvp = ng.BVP(bf=a, lf=f, gf=s, pre=c, maxsteps=10).Do()


err = ng.sqrt(ng.Integrate(ng.InnerProduct(s - S, s - S), mesh))
print(" || s - S || = ", err)

# Matrix operations on s
smatrix = ng.CoefficientFunction((s[0], s[1], s[2],
                                  s[3], s[4], s[5],
                                  s[6], s[7], s[8]), dims=(3, 3))

n = ng.CoefficientFunction((1, 1, -2))
snn = ng.InnerProduct(smatrix * n, n)
ng.Draw(smatrix * n, mesh, "sn")  # Visualize s*n
ng.Draw(snn, mesh, "snn")   # Add s*n.n to visual menu


def test_proj():
    success = (err < 1.e-11)
    msg = 'Projection error of a linear function = %g != 0' % err
    assert success, msg
