"""
 Testing the surjectivity of divergence of the GG & AFW finite element space.

 div: GG_k \to (L^2)_{k-1} should be surjective.

"""
from netgen.csg import Plane, Vec, Pnt, CSGeometry
import ngsolve as ng
from ngsolve import div
import scipy.sparse as sp
from numpy.linalg import matrix_rank
from ctypes import CDLL

ng.ngsglobals.msg_level = 1
mylngs = CDLL("../libelas.so")

# Unit tetrahedron (mesh has just this element)
lft = Plane(Pnt(0, 0, 0), Vec(-1, 0, 0))
frt = Plane(Pnt(0, 0, 0), Vec(0, -1, 0))
bot = Plane(Pnt(0, 0, 0), Vec(0, 0, -1))
top = Plane(Pnt(0, 0, 1), Vec(1 / ng.sqrt(3), 1 / ng.sqrt(3), 1 / ng.sqrt(3)))
tetrahedron = CSGeometry()
tetrahedron.Add(lft * frt * bot * top)
mesh = ng.Mesh(tetrahedron.GenerateMesh(maxh=1))


k = 3
S = ng.FESpace("ggfes3d", mesh, order=k,
               flags={"nobubbles": False})  # Put nobubbles:True to test AFW
L = ng.VectorL2(mesh, order=k - 1)
V = ng.FESpace([S, L])

sig, u = V.TrialFunction()
tau, v = V.TestFunction()

a = ng.BilinearForm(V)
a += ng.SymbolicBFI(div(sig) * v)

a.Assemble()
i, j, val = a.mat.COO()
l = a.mat.height
B = sp.coo_matrix((val, (i, j)), shape=(l, l)).todense()
r = matrix_rank(B)


print(" rank = ", r)
print(" L.ndof = ", L.ndof)


def test_div():
    success = (r == L.ndof)
    msg = 'Dim(range(div))=%d' % r +\
          'not equal to dim(P_{k-1})=%d' % L.ndof
    assert success, msg
