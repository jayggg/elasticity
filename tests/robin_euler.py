"""
A plane strain P-wave example with 1st order outgoing bc:

A sigma' - eps(u) = 0    on unit square,
v' - div(sigma)   = F    on unit square,
B sigma n +  v    = Gn   on boundaries,

This file tests the implementation of the Robin bc using the same B
which will yield outgoing b.c.  We use GG elements and backward Euler
time discretization.

"""

import ngsolve as ng
from ngsolve import InnerProduct, div, SymbolicBFI
from netgen.geom2d import unit_square
from ctypes import CDLL

x = ng.x

ngsharedlib = CDLL("../libelas.so")
ng.ngsglobals.msg_level = 1

h = 0.1  # Mesh size
k = 1    # Polynomial order

# Lame parameters
lam = 4.0
mu = 0.5

# Density of the material
rho = 1

mesh = ng.Mesh(unit_square.GenerateMesh(maxh=h))

cp = ng.sqrt((lam + 2 * mu) / rho)   # wave speeds
cs = ng.sqrt(mu / rho)
delta = 0.9 * h / (k * k * max(cp, cs))   # time step size ∆t

S = ng.FESpace("ggfes", mesh, order=k)  # , dirichlet="top|bottom|left|right")
V = ng.VectorL2(mesh, order=k - 1)      # Velocity space = U x U
A = ng.L2(mesh, order=k)                # Space of rotations (skw sym mat)
X = ng.FESpace([S, V, A])

sig, v, r = X.TrialFunction()
tau, w, q = X.TestFunction()
n = ng.specialcf.normal(mesh.dim)


def exact(t):

    """Exact solution as a function of time t can be made as follows:

    We choose a right going scalar wave phi = phi( x - ct, y ), with
    c= (lambda + 2 mu) / rho, that solves the wave equation
    dtt(phi) = c^2 ∆(phi) to manufacture a plane strain P wave.

    Given a wave solution phi(x,t) = phi(x-ct), the following
    quantities solve the elastodynamics equations:

    u = grad(phi) = [ phi' ; 0 ]

    grad u = [ phi', 0; 0  0]  = eps(u)

    r(u) = grad(u) - eps(u) = 0

    v = u' = [ - phi'' ; 0]

    sigma = 2 mu eps(u) + lambda tr(eps) I
          = [ (2 mu + lambda) phi'',  0 ;  0   lambda phi'' ]

    """

    # aa = 300; bb = 0.3
    # ddphi = exp( -aa * (x - bb - cp*t) * (x - bb - cp*t) )

    # For this case, the method should recover the solution exactly
    ddphi = (x - cp * t)
    sig = ng. CoefficientFunction((ddphi * (2 * mu + lam), 0, 0, lam * ddphi),
                                  dims=(2,2))
    vel = ng.CoefficientFunction((-cp * ddphi, 0))
    rot = ng.CoefficientFunction(0)

    return (sig, vel, rot)


def B(ss):

    """Return the action the matrix B (in the 1st order absorbing b.c.)
    on a stress matrix ss.  The b.c. is written e.g. in Bamberger et al
    as

    sigma n + c v = 0

    where c is the matrix c = rho * N * diag(cp,cs) * N and
    N = [ nx ny; ny -nx]. We invert c to get B.
    """

    # Compute  c = rho * N * diag(cp,cs) * N
    c = [[rho * (n[0] * n[0] * cp + n[1] * n[1] * cs),
          rho * n[0] * n[1] * (cp - cs)],
         [rho * n[0] * n[1] * (cp - cs),
          rho * (n[1] * n[1] * cp + n[0] * n[0] * cs)]]
    cdet = c[0][0] * c[1][1] - c[0][1] * c[1][0]

    # Compute B =  inv(c)
    B = [[c[1][1] / cdet, -c[0][1] / cdet],
         [-c[1][0] / cdet, c[0][0] / cdet]]
    Bs = (B[0][0] * ss[0] + B[0][1] * ss[2],
          B[0][0] * ss[1] + B[0][1] * ss[3],
          B[1][0] * ss[0] + B[1][1] * ss[2],
          B[1][0] * ss[1] + B[1][1] * ss[3])

    return ng.CoefficientFunction(Bs, dims=(2, 2))


def G(t):
    """To implement  the nonhomogeneous b.c.

    B sigma n +  v  = G n

    we need a matrix G such that G n is on the rhs. We compute such a
    G, namely G = B sigma + v n',  using the exact solution.

    """
    ss, vv, rr = exact(t)
    vnt = ng.CoefficientFunction((vv[0] * n[0], vv[0] * n[1],
                                  vv[1] * n[0], vv[1] * n[1]), dims=(2, 2))
    return B(ss) + vnt


# u stores initial data and will contain future time iterates
u = ng.GridFunction(X)
sinit, vinit, rinit = exact(0)
u.components[0].Set(sinit)
u.components[1].Set(vinit)
u.components[2].Set(rinit)


a1 = 0.5 / mu
a2 = lam / (4.0 * mu * (lam + mu))

Asig = ng.CoefficientFunction((a1 * sig[0] - a2 * (sig[0] + sig[3]),
                               a1 * sig[1], a1 * sig[2],
                               a1 * sig[3] - a2 * (sig[0] + sig[3])),
                              dims=(2, 2))

"""
Formulation:  Weakly symmetric mixed method:

 (sigma',tau) + (v, div tau) + <B sigma.n,tau.n> + (Skw r',tau) = <Gn, tau.n>
 (div sigma, w) - (v', w)   = -(F,w)
 (sigma', Skw q)   = 0

Discretization:   Backward Euler in matrix form:

M * u(t+∆t) = S * u(t) + RHS(t)

      [    A      ∆t B^T    D^T   ]
  M = [  ∆t B       -C       0    ]
      [    D         0       0    ]

      [    A         0       D^T  ]
  S = [    0        -C       0    ]
      [    D         0       0    ]

  RHS(t) = ∆t <G(t+∆t)n, tau n> -  ∆t (F(t+∆t), w)
"""

# Make the M matrix
m = ng.BilinearForm(X)
m += SymbolicBFI(delta * InnerProduct(B(sig.Trace()), tau.Trace()),
                 ng.BND)                           # A, bdry
m += SymbolicBFI(InnerProduct(Asig, tau))          # A, vol part
m += SymbolicBFI(delta * v * div(tau))             # ∆t B^T
m += SymbolicBFI(r * (tau[0, 1] -  tau[1, 0]))  # D^T
m += SymbolicBFI(delta * div(sig) * w)             # ∆t B
m += SymbolicBFI(-rho * (v * w))                   # -C
m += SymbolicBFI((sig[0, 1] - sig[1, 0]) * q)      # D
m.Assemble()
inv_M = m.mat.Inverse(X.FreeDofs())

# Make the S matrix
s = ng.BilinearForm(X, nonassemble=True)
s += ng.SymbolicBFI(InnerProduct(Asig, tau))       # A, only vol part has d_t
s += ng.SymbolicBFI(r * (tau[0, 1] - tau[1, 0]))   # D^T
s += ng.SymbolicBFI(-rho * (v * w))                # -C
s += ng.SymbolicBFI((sig[0, 1] - sig[1, 0]) * q)   # D

f = ng.LinearForm(X)
f.Assemble()
ff = f.vec
t = 0
tend = 0.15


with ng.TaskManager():
    while t < tend:
        s.Apply(u.vec, ff)   # write S*u into ff
        li = ng.LinearForm(X)   # assemble time dependent source
        li += ng.SymbolicLFI(delta * InnerProduct(G(t + delta), tau.Trace()),
                             ng.BND)
        li.Assemble()
        ff.data += li.vec
        u.vec.data = inv_M * ff
        t += delta
        print('t=%g' % t)
        ng.Redraw(blocking=True)


s, v, r = exact(t)
s_err = ng.sqrt(ng.Integrate(InnerProduct(u.components[0] - s,
                                          u.components[0] - s), mesh))
print('\n>>> L2 error in stress at time t=%g is %g' % (t, s_err))


def test_robin_euler():
    success = (s_err < 1.e-11)
    msg = 'Exact linear (in x and t) solution unrecovered!'
    assert success, msg
