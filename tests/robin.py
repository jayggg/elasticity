"""
Solve the static linear elasticity problem

A(sigma) - eps(u) = 0
div(sigma)     = f
sigma.n + u  = g   on boundary

using the following formulation:

(sigma, tau)+(u,div tau)+(skw r, tau)+<sigma.n,tau.n> = <g,tau.n>
(div sigma, v)                                        = (f,w)
(sigma, skw q)                                        = 0.0

where the <sigma.n,tau.n> term is an integral over the boundary
arising from the Robin boundary condtion.

"""

import ngsolve as ng
from ngsolve import Integrate, div, InnerProduct, SymbolicBFI, sqrt
from netgen.geom2d import unit_square

from ctypes import CDLL

mylngs = CDLL("../libelas.so")
ng.ngsglobals.msg_level = 1

x = ng.x
y = ng.y


h = 0.1  # Mesh size
k = 4    # Polynomial order


lam = 4.0  # Lambda lam'e parameter
mu = 0.5   # mu lam'e parameter
rho = 1.0  # Density

mesh = ng.Mesh(unit_square.GenerateMesh(maxh=h))

# Making an exact solution:
phi = x * x * x                      # phi = any function of just x
phx = 3 * x * x                      # phx = x-derivative of phi
pxx = 6 * x                          # pxx = second x-derivative of phi
s = ((2 * mu + lam) * phx, 0, 0, lam * phx)  # s = exact stress
u = (phi, 0)                         # u = exact displacement
f = ((2 * mu + lam) * pxx, 0)        # f = div(s)
exactu = ng. CoefficientFunction(u)
exacts = ng.CoefficientFunction(s, dims=(2, 2))
F = ng.CoefficientFunction(f)
n = ng.specialcf.normal(mesh.dim)
unt = (phi * n[0], phi * n[1], 0, 0)                   # unt = u * n'
g = exacts + ng.CoefficientFunction(unt, dims=(2, 2))  # g = s + u * n'
#  Note: g.n = s.n + u

# Spaces
S = ng.FESpace("ggfes", mesh, order=k)
U = ng.VectorL2(mesh, order=k - 1)
A = ng.L2(mesh, order=k)
X = ng.FESpace([S, U, A])

sig, u, r = X.TrialFunction()
tau, w, q = X.TestFunction()

# Forms
a1 = 0.5 / mu               # Compliance tensor for isotropic mat
a2 = lam / (4.0 * mu * (lam + mu))
Asig = ng.CoefficientFunction((a1 * sig[0] - a2 * (sig[0] + sig[3]),
                               a1 * sig[1], a1 * sig[2],
                               a1 * sig[3] - a2 * (sig[0] + sig[3])))

a = ng.BilinearForm(X, symmetric = True)
a += SymbolicBFI(Asig * tau)
a += SymbolicBFI(u * div(tau))
a += SymbolicBFI(r * (tau[0, 1] - tau[1, 0]))
a += SymbolicBFI(div(sig) * w)
a += SymbolicBFI((sig[0, 1] - sig[1, 0]) * q)
a += SymbolicBFI(InnerProduct(sig.Trace(), tau.Trace()), ng.BND)

f = ng.LinearForm(X)
f += ng.SymbolicLFI(F * w)
f += ng.SymbolicLFI(InnerProduct(g, tau.Trace()), ng.BND)

# Solve
c = ng.Preconditioner(a, type="direct")
a.Assemble()
f.Assemble()

sua = ng.GridFunction(X)
bvp = ng.BVP(bf=a, lf=f, gf=sua, pre=c, maxsteps=10).Do()

# Check error
s_err = sqrt(Integrate(InnerProduct(sua.components[0] - exacts,
                                    sua.components[0] - exacts),
                       mesh))
u_err = sqrt(Integrate((sua.components[1] - exactu) *
                       (sua.components[1] - exactu), mesh))
print('stress err = ', s_err)
print('displacement err = ', u_err)


def test_robin():
    success = (s_err < 1.e-10) and (u_err < 1.e-10)
    msg = 'Exact polynomial solution unrecovered!'
    assert success, msg
