"""Solve the static linear elasticity problem

 A(sigma) - eps(u) = 0
 div(sigma)        = f
 sigma.n + u       = g,   on boundary

using the following formulation:

 (sigma, tau)+(u,div tau)+(skw r, tau)+<sigma.n,tau.n> = <g,tau.n>
 (div sigma, v)                                        = (f,w)
 (sigma, skw q)         	                       = 0.0

where the <sigma.n,tau.n> term is an integral over the boundary
arising from the Robin boundary condtion.

"""
import ngsolve as ng
from ngsolve import div, SymbolicBFI, x, CoefficientFunction, InnerProduct
from netgen.csg import unit_cube
from ctypes import CDLL

mylngs = CDLL("../libelas.so")
ng.ngsglobals.msg_level = 1
ng.SetHeapSize(int(1e8))

h = 0.1    # Mesh size
k = 4      # Polynomial order
lam = 4.0  # Lambda Lam'e parameter
mu = 0.5   # mu Lam'e parameter
rho = 1.0  # Density of the material

mesh = ng.Mesh(unit_cube.GenerateMesh(maxh=1))

# Making an exact solution:
phi = x * x * x                      # phi = any function of just x
phx = 3 * x * x                      # phx = x-derivative of phi
pxx = 6 * x                          # pxx = second x-derivative of phi
s = ((2 * mu + lam) * phx, 0, 0,
     0, lam * phx, 0,
     0, 0, lam * phx)                # s = exact stress
u = (phi, 0, 0)                      # u = exact displacement
f = ((2 * mu + lam) * pxx, 0, 0)     # f = div(s)

exactu = CoefficientFunction(u)
exacts = CoefficientFunction(s, dims=(3, 3))
F = ng.CoefficientFunction(f)
n = ng.specialcf.normal(mesh.dim)
unt = (phi * n[0], phi * n[1], phi * n[2], 0, 0, 0, 0, 0, 0)  # unt = u * n'
#  Note: g = s + u * n',   g.n = s.n + u:
g = exacts + CoefficientFunction(unt, dims=(3, 3))

# Spaces
S = ng.FESpace("ggfes3d", mesh, order=k)
V = ng.HDiv(mesh, order=k)
U = ng.VectorL2(mesh, order=k-1)
A = ng.VectorL2(mesh, order=k)
X = ng.FESpace([S, U, A])

sig, u, r = X.TrialFunction()
tau, w, q = X.TestFunction()


def Skw(r):
    return CoefficientFunction((0, -r[2], r[1],
                                r[2], 0, -r[0],
                                -r[1], r[0], 0), dims=(3, 3))


# Compliance tensor for isotropic mat
a1 = 0.5 / mu
a2 = lam / (2.0 * mu * (3 * lam + 2 * mu))
Asig = CoefficientFunction((a1 * sig[0] - a2 * (sig[0] + sig[4] + sig[8]),
                            a1 * sig[1], a1 * sig[2], a1 * sig[3],
                            a1 * sig[4] - a2 * (sig[0] + sig[4] + sig[8]),
                            a1 * sig[5], a1 * sig[6], a1 * sig[7],
                            a1 * sig[8] - a2 * (sig[0] + sig[4] + sig[8])))

# Forms
a = ng.BilinearForm(X, symmetric = True)
a += SymbolicBFI(InnerProduct(Asig, tau))
a += SymbolicBFI(u * div(tau))
a += SymbolicBFI(InnerProduct(Skw(r), tau))
a += SymbolicBFI(div(sig) * w)
a += SymbolicBFI(InnerProduct(sig, Skw(q)))
a += SymbolicBFI(InnerProduct(sig.Trace(), tau.Trace()), ng.BND)

f = ng.LinearForm(X)
f += ng.SymbolicLFI(F * w)
f += ng.SymbolicLFI(InnerProduct(g, tau.Trace()), ng.BND)

# Solve
c = ng.Preconditioner(a, type="direct")
a.Assemble()
f.Assemble()

uh = ng.GridFunction(X)
bvp = ng.BVP(bf=a, lf=f, gf=uh, pre=c, maxsteps=10).Do()

# Check error
s_err = ng.sqrt(ng.Integrate(InnerProduct(uh.components[0] - exacts,
                                          uh.components[0] - exacts), mesh))
udiff = uh.components[1] - exactu
u_err = ng.sqrt(ng.Integrate(udiff * udiff, mesh))

ng.Draw(uh.components[0], mesh, "sigma")
ng.SetVisualization(deformation=True)
print('stress err = ', s_err)
print('displacement err = ', u_err)


def test_robin3d():
    success = (s_err < 1.e-11) and (u_err < 1.e-11)
    msg = 'Exact polynomial solution unrecovered!'
    assert success, msg
