"""
Cross check GG FE Space's interpolant with built-in H(div) interpolant
"""

from netgen.csg import unit_cube
import ngsolve as ng
from ctypes import CDLL

mylngs = CDLL("../libelas.so")
ng.ngsglobals.msg_level = 1

mesh = ng.Mesh(unit_cube.GenerateMesh(maxh=0.5))

k = 1

x = ng.x
y = ng.y
z = ng.z

U0 = (z, z, z)
U1 = (x, 0, 0)
U2 = (0, z, y)

V = ng.FESpace("ggfes3d", mesh, order=k, flags={"nobubbles": False})
u = ng.GridFunction(V, name="u")
U = ng.CoefficientFunction((U0[0], U0[1], U0[2],
                            U1[0], U1[1], U1[2],
                            U2[0], U2[1], U2[2]))
u.Set(U)


VV = ng.HDiv(mesh, order=k)
u0 = ng.GridFunction(VV, name="u0")
u1 = ng.GridFunction(VV, name="u1")
u2 = ng.GridFunction(VV, name="u2")
u0.Set(ng.CoefficientFunction(U0))
u1.Set(ng.CoefficientFunction(U1))
u2.Set(ng.CoefficientFunction(U2))


def test_interp():

    uu0 = ng.CoefficientFunction((u[0], u[1], u[2]))
    uu1 = ng.CoefficientFunction((u[3], u[4], u[5]))
    uu2 = ng.CoefficientFunction((u[6], u[7], u[8]))

    diff = ng.Integrate((u0 - uu0) * (u0 - uu0) +
                        (u1 - uu1) * (u1 - uu1) +
                        (u2 - uu2) * (u2 - uu2), mesh, order=2 * k)

    print('diff = ', diff)
    success = (abs(diff) < 1.e-15)
    msg = 'Unexpected nonzero interpolation  error!' + \
          '\n  Check DiffOpIdHDivSigma3D?'
    assert success, msg


def test_interp_bdry():

    ubVV0 = ng.GridFunction(VV)
    ubVV1 = ng.GridFunction(VV)
    ubVV2 = ng.GridFunction(VV)
    ubVV0.Set(ng.CoefficientFunction(U0), ng.BND)
    ubVV1.Set(ng.CoefficientFunction(U1), ng.BND)
    ubVV2.Set(ng.CoefficientFunction(U2), ng.BND)

    ubV = ng.GridFunction(V)
    ubV.Set(U, ng.BND)

    ubV0 = ng.CoefficientFunction((ubV[0], ubV[1], ubV[2]))
    ubV1 = ng.CoefficientFunction((ubV[3], ubV[4], ubV[5]))
    ubV2 = ng.CoefficientFunction((ubV[6], ubV[7], ubV[8]))

    diff = ng.Integrate((ubVV0 - ubV0) * (ubVV0 - ubV0) +
                        (ubVV1 - ubV1) * (ubVV1 - ubV1) +
                        (ubVV2 - ubV2) * (ubVV2 - ubV2), mesh, order=2 * k)

    print('diff = ', diff)
    success = (abs(diff) < 1.e-15)
    msg = 'Unexpected nonzero interpolation  error!' + \
          '\n  Check DiffOpBoundIdHDiv3D?'
    assert success, msg


test_interp()
test_interp_bdry()
