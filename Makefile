VPATH = ./cpp

objects = GGfes3d.o integrator3d.o GGfes.o integrator.o l2orthfes.o  l2orthfes3d.o
headers = GGfes.hpp GGtrg.hpp integrator.hpp l2orth.hpp l2orthfes.hpp l2orthfes3d.hpp

%.o : %.cpp $(headers)
	ngscxx -I. -c $< -o $@

libelas.so : $(objects)
	ngsld -shared $(objects) -lngfem -lngcomp -lngsolve -lngbla -lngstd -o $@

clean:
	rm *.o libelas.so
