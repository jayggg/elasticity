"""Example 22.2 from LeVeque's book [Finite volume methods for
hyperbolic problems]

A y-independent plane strain P-wave starts moving from Material 0, is
reflected and trasmitted as it crosses the interface to Material 1.


   P5=(-1,1)
          +----------------------------------------------+ P4=(1,1)
          |                                              |
          |                                              |
          |                                              |
          |        Material 0                            |
          |                                            /-+ P3=(1,0.5)
          |                                        /---  |
          |                                    /---      |
          |                                /---          |
          |                            /---              |
          |                        /---                  |
          |             P6=(0,0) +-                      |
          |                      |                       |
          |                      |                       |
          |                      |                       |
          |                      |                       |
          |                      |      Material 1       |
          |                      |                       |
          |                      |                       |
          |                      |                       |
          |                      |                       |
          |                      |                       |
          +----------------------+-----------------------+
  P0=(-1,-1)                   P1=(0,-1)                   P2=(1,-1)


The boundary conditions are unspecified in the book, so we will make
one up to simulate similar physics.  """

import ngsolve as ng
from ngsolve import sqrt, exp
from ngsolve import InnerProduct, div, x, y
from netgen.geom2d import SplineGeometry
from ctypes import CDLL
mylngs = CDLL("../libelas.so")
ng.ngsglobals.msg_level = 1

h = 0.1          # mesh size away from corner singularity
k = 3            # polynomial degree

# Make and mesh the domain

geom = SplineGeometry()
Ps = [(-1, -1), (0, -1), (1, -1), (1, 0.5), (1, 1), (-1, 1), (0, 0, h / 10)]
P = [geom.AppendPoint(*p) for p in Ps]

# Material 0:

#       (p_start, p_end, ml, mr, bcname, maxh)
lines = [(P[0], P[1], 1, 0, "bot0", h),
         (P[1], P[6], 1, 2, "verticalinterface", h / 3.0),
         (P[6], P[3], 1, 2, "slantedinterface", h / 3.0),
         (P[3], P[4], 1, 0, "right0", h),
         (P[4], P[5], 1, 0, "top", h),
         (P[5], P[0], 1, 0, "left", h)]

for p0, p1, ml, mr, bcname, mh in lines:
    geom.Append(["line", p0, p1], bc=bcname,
                leftdomain=ml, rightdomain=mr, maxh=mh)
# Material 1:

#       (p_start, p_end, ml, mr, bcname)
lines = [(P[1], P[2], 2, 0, "bot1"),
         (P[2], P[3], 2, 0, "right1")]

for p0, p1, ml, mr, bcname in lines:
    geom.Append(["line", p0, p1], bc=bcname, leftdomain=ml, rightdomain=mr)

mesh = ng.Mesh(geom.GenerateMesh(maxh=h))

# Problem parameters

# Density   (from LeVeque's book)
rho = 1
# Lame coefficients lambda and mu:
#    mat0  mat1
lm = [4.0, 2.0]
mu = [0.5, 1.0]

# P and S wave speeds:
#    [ mat0,  mat1 ]
cp = [sqrt((lm[0] + 2 * mu[0]) / rho),
      sqrt((lm[1] + 2 * mu[1]) / rho)]
cs = [sqrt(mu[0] / rho), sqrt(mu[1] / rho)]

# Spatially varying material coefficient functions:

lam = ng.CoefficientFunction(([lm[0], lm[1]]))
mu = ng.CoefficientFunction(([mu[0], mu[1]]))

# Conservative time step size ∆t
delta = 0.9 * h / (k * k * max(cp[0], cs[0], cp[1], cs[1]))

# Boundary coefficients
bdrymat = {'bot0': 0, 'left': 0, 'top': 0, 'right0': 0,
           'bot1': 1, 'right1': 1,
           'verticalinterface': 0, 'slantedinterface': 0}
cpb = ng.CoefficientFunction([cp[bdrymat[b]] for b in mesh.GetBoundaries()])
csb = ng.CoefficientFunction([cs[bdrymat[b]] for b in mesh.GetBoundaries()])
lmb = ng.CoefficientFunction([lm[bdrymat[b]] for b in mesh.GetBoundaries()])

# Initial wave:
aa = 300
bb = -0.2
ddphi = exp(-aa * (x - bb) * (x - bb))
# ddphi = IfPos(x-(bb-0.04), 1, 0) * IfPos(x-(bb+0.04), 0, 1)
sinit = ng.CoefficientFunction((ddphi * (2 * mu + lam), 0, 0, lam * ddphi), dims=(2, 2))
vinit = ng.CoefficientFunction((-cp[0] * ddphi, 0))
rinit = ng.CoefficientFunction(0)

""" These settings imply that if Material 0 = Material 1, then the
exact solution would be obtained as follows:

   phi   = exp( -aa * (x - bb -cp*t) * (x - bb - cp*t) )
   sigma = [ phi*(2*mu+lam),0 ;  0,lam*phi ]
   v     = [ -cp[0]*phi; 0 ]

"""

# Compute:

S = ng.FESpace("ggfes", mesh, order=k)  # dirichlet="top|bottom|left|right")
V = ng.VectorL2(mesh, order=k - 1)      # Velocity space = L2 x L2
A = ng.L2(mesh, order=k)                # Space of rotations (skw sym mat)
X = ng.FESpace([S, V, A])

sig, v, r = X.TrialFunction()
tau, w, q = X.TestFunction()
n = ng.specialcf.normal(mesh.dim)

u = ng.GridFunction(X)
u.components[0].Set(sinit)
u.components[1].Set(vinit)
u.components[2].Set(rinit)

ng.Draw(u.components[0][1], mesh, "s12")
ng.Draw(u.components[0][3], mesh, "s22")
ng.Draw(u.components[0][0], mesh, "s11")
ng.Draw(u.components[0][0] + u.components[0][3],
        mesh, "trace(stress)",
        autoscale=False, min=-0.5, max=1)


def Blr(ss):

    """On left and right, we impose 1st order outgoing b.c. in the form

    B sigma n +  v    = 0

    This funtion returns the action the matrix B on an input stress
    matrix ss.  The b.c. is written, e.g. in Bamberger et al, as
    sigma n + c v = 0, where  c = rho * N * diag(cp,cs) * N
    and N = [ nx ny; ny -nx]. We invert c to get B.

    This bc is meant to be transparent only for normally incident
    waves. On the left and right, our solution is normally incident.

    """

    # Compute  c = rho * N * diag(cp,csb) * N
    c = [[rho * (n[0] * n[0] * cpb + n[1] * n[1] * csb),
          rho * n[0] * n[1] * (cpb - csb)],
         [rho * n[0] * n[1] * (cpb - csb),
          rho * (n[1] * n[1] * cpb + n[0] * n[0] * csb)]]
    cdet = c[0][0] * c[1][1] - c[0][1] * c[1][0]

    # Compute B =  inv(c)
    B = [[c[1][1] / cdet, -c[0][1] / cdet],
         [-c[1][0] / cdet, c[0][0] / cdet]]

    Bs = (B[0][0] * ss[0] + B[0][1] * ss[2],
          B[0][0] * ss[1] + B[0][1] * ss[3],
          B[1][0] * ss[0] + B[1][1] * ss[2],
          B[1][0] * ss[1] + B[1][1] * ss[3])

    return ng.CoefficientFunction(Bs, dims=(2, 2))


def Btb(ss):

    """On the top and bottom boundaries, the situation is more difficult
    (and LeVeque does not write down the bc explicitly).    We put a bc
    of the form

    B sigma n +  v  = 0

    where B is of now different, an ad hoc nonsymmetric matrix suitable
    just for the  plane P wave we have in mind.

    """

    # B = [ [ 0,     cp/lam],
    #       [-cp/lam,     0] ]

    cl = n[1] * cpb / lmb

    Bs = (cl * ss[2], cl * ss[3],
          -cl * ss[0], -cl * ss[1])
    return ng.CoefficientFunction(Bs, dims=(2, 2))


a1 = 0.5 / mu
a2 = lam / (4.0 * mu * (lam + mu))
Asig = ng.CoefficientFunction((a1 * sig[0] - a2 * (sig[0] + sig[3]),
                               a1 * sig[1], a1 * sig[2],
                               a1 * sig[3] - a2 * (sig[0] + sig[3])), dims=(2, 2))

# Make the M matrix
m = ng.BilinearForm(X)
m += ng.SymbolicBFI(delta * InnerProduct(Btb(sig.Trace()), tau.Trace()),
                    ng.BND, definedon=mesh.Boundaries('top|bot0|bot1'))
m += ng.SymbolicBFI(delta * InnerProduct(Blr(sig.Trace()), tau.Trace()),
                    ng.BND, definedon=mesh.Boundaries('left|right0|right1'))
m += ng.SymbolicBFI(InnerProduct(Asig, tau))                                 # A, vol part
m += ng.SymbolicBFI(delta * v * div(tau))  # ∆t B^T
m += ng.SymbolicBFI(r * (tau[0, 1] - tau[1, 0]))  # D^T
m += ng.SymbolicBFI(delta * div(sig)* w)  # ∆t B
m += ng.SymbolicBFI(-v * w)   # -C
m += ng.SymbolicBFI((sig[0, 1] - sig[1, 0]) * q)  # D
m.Assemble()
inv_M = m.mat.Inverse(X.FreeDofs())#, flags={"inverse":"umfpack"})

# Make the S matrix
s = ng.BilinearForm(X, nonassemble=True)
s += ng.SymbolicBFI(InnerProduct(Asig, tau))      # A, only vol part has d_t
s += ng.SymbolicBFI(r * (tau[0, 1] - tau[1, 0]))      # D^T
s += ng.SymbolicBFI(-v* w)           # -C
s += ng.SymbolicBFI((sig[0,1] - sig[1,0]) * q)      # D

f = ng.LinearForm(X)
f.Assemble()
ff = f.vec
t = 0
tend = 0.45

with ng.TaskManager():
    while t < tend:

        s.Apply(u.vec, ff)   # write S*u into ff
        u.vec.data = inv_M * ff
        t += delta
        print('t=%g' % t)
        ng.Redraw(blocking=True)
