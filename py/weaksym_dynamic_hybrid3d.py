"""
 Equations of dynamic linear elasticity for stress sigma, velocity v,
 and rotations r in hybrid form:

  (sigma', tau) + (v, div tau) + (Skw(r'), tau) + <l,tau.n> = 0
  (div sigma, w) - (v', w)                                  = (F,w)
  (sigma', Skw(q))			                    = 0
  <sig.n, m>                                                = 0


 Here  Skw(m) = [ 0 -m3 m2]  and prime denotes time derivative.
                [ m3 0 -m1]
                [-m2 m1  0]

 We perform spatial discretization with GG elements, implicit time
 stepping  using Crank-Nicolson and measure convergence rates
 when the exact solution is

 u(x,y,t) = [ sin(pi*x)*sin(pi*y)*sin(pi*z)*t ]
            [  x(1-x)*y(1-y)*z*(1-z)*t        ]
            [        0                        ].
 Note that with this displacement, the initial velocity is

 v(x,y,0) = [ sin(pi*x)*sin(pi*x)*sin(pi*z) ]
            [ x(1-x)y(1-y)z(1-z)            ]
            [       0                       ]
 which forms the data for the simulation.

 u(x,y,t) is recovered by integrating in time using trapezoidal rule.

"""
from ngsolve import CoefficientFunction, SymbolicBFI, SymbolicLFI, sin, cos
from ngsolve import InnerProduct, div, x, y, z
import ngsolve as ng
from netgen.csg import unit_cube
from numpy import pi

from ctypes import CDLL

mylngs = CDLL("../libelas.so")
ng.ngsglobals.msg_level = 1

# ########## INITIAL DATA ##################################

def initdata():

    """ Return two vectors, representing the initial values of
    div(sig(0)) and velocity v(0). (The remainder of the required
    initial data will be computed automatically.
    """

    divsig0 = CoefficientFunction((0, 0, 0))
    v0 = CoefficientFunction((sin(pi * x) * sin(pi * y) * sin(pi * z),
                              x * (1 - x) * y * (1 - y) * z * (1 - z),
                              0))

    return divsig0, v0

# ========== SOURCE  DATA =====================================


def F(t):  # -vt + div(sig)

    F0 = t * (-2 * pi * pi * sin(pi * x) *
              sin(pi * y) * sin(pi * z) +
              0.5 * (1 - 2 * x) * (1 - 2 * y) * z * (1 - z))

    F1 = t * (-y * (1 - y) * z * (1 - z) -
              2 * x * (1 - x) * z * (1 - z) -
              x * (1 - x) * y * (1 - y) +
              0.5 * pi * pi * cos(pi * x) * cos(pi * y) * sin(pi * z))
    F2 = t * (0.5 * pi * pi * cos(pi * x) * sin(pi * y) * cos(pi * z) +
              0.5 * (1 - 2 * z) * (1 - 2 * y) * x * (1 - x))

    return CoefficientFunction((F0, F1, F2))


def Skw(r):
    return CoefficientFunction((0, -r[2], r[1],
                                r[2], 0, r[0],
                                -r[1], r[0], 0), dims=(3, 3))

# ============ SOLVE =============================================


def weaksym_dynamic_hybrid3d(k, h, l, tend):

    """ PARAMETERS:

    k: determines polynomial order of stress (in P_k + bubbles),
       velocity (in P_{k-1}) and rotations (in P_k).

    h: maximal mesh size (of the mesh of unit square)

    l: sets timestep by  ∆t = h * l

    tend: final simulation time where error is measured.

    """

    mesh = ng.Mesh(unit_cube.GenerateMesh(maxh=h))
    delta = l * h   # time step size ∆t

    S = ng.FESpace("ggfes3d", mesh, order=k, flags={"discontinuous": True})
    U = ng.VectorL2(mesh, order=k - 1)    # Displacement space = L2 x L2 x L2
    A = ng.VectorL2(mesh, order=k)        # Space of rotations (skw sym mat)
    L = ng.FacetFESpace(mesh, order=k, dirichlet=[1, 2, 3, 4, 5, 6])
    XY = ng.FESpace([S, U, A, L, L, L])

    # Setting initial sig, r values by solving a static problem:
    divsig0, v0 = initdata()

    sig, v, r, l1, l2, l3 = XY.TrialFunction()
    tau, w, q, m1, m2, m3 = XY.TestFunction()

    n = ng.specialcf.normal(mesh.dim)
    l = ng.CoefficientFunction((l1, l2, l3))
    m = ng.CoefficientFunction((m1, m2, m3))

    a = ng.BilinearForm(XY, symmetric=True, eliminate_internal=True)
    a += SymbolicBFI(InnerProduct(sig, tau))
    a += SymbolicBFI(v * div(tau))
    a += SymbolicBFI(InnerProduct(Skw(r), tau))
    a += SymbolicBFI(div(sig) * w)
    a += SymbolicBFI(InnerProduct(Skw(q), sig))
    a += SymbolicBFI(l * (tau * n), element_boundary=True)   # <l,tau.n>

    a += SymbolicBFI(m * (sig * n), element_boundary=True)  # <sig.n, m>

    c = ng.Preconditioner(a, type="direct", flags={"inverse": "umfpack"})

    a.Assemble(int(1e9))

    fin = ng.LinearForm(XY)
    fin += SymbolicLFI(divsig0 * w)
    fin.Assemble(int(1e9))

    initial = ng.GridFunction(XY)

    ng.BVP(bf=a, lf=fin, gf=initial, pre=c, maxsteps=10).Do()

    # u stores initial data and will contain future time iterates
    u = ng.GridFunction(XY)
    u.components[0].Set(initial.components[0])  # Set sig(0)
    u.components[2].Set(ng.CoefficientFunction((initial.components[2][0],
                                                initial.components[2][0],
                                                initial.components[2][0])))  # Set r(0)
    # Set initial velocity by projecting v0 into fe space
    u.components[1].Set(v0)

    ng.Draw(u.components[1][1], mesh, "velocity", sd=2)

    """ Crank-Nicolson in matrix form reads as

     M * u(t+∆t) = S * u(t) + F(t)

          [    A     (∆t/2)B^T     D^T   (∆t/2) E^T ]
      M = [ (∆t/2)B      -C       0          0      ]
          [    D          0       0          0      ]
          [ (∆t/2)E       0        0       0        ]

          [    A    -(∆t/2)B^T   D^T  -(∆t/2) E^T ]
      S = [ -(∆t/2)B    -C       0          0     ]
          [    D         0       0          0     ]
          [ -(∆t/2)E     0       0          0     ]

      F(t) = <(∆t/2)(F0(t)+F0(t+∆t)), v_1>
           + <(∆t/2)(F1(t)+F1(t+∆t)), v_2>
    """

    # Make the M matrix
    mm = ng.BilinearForm(XY, symmetric=True, eliminate_internal=True)
    mm += SymbolicBFI(InnerProduct(sig, tau))                        # A
    mm += SymbolicBFI(0.5 * delta * v * div(tau))   # (∆t/2)B^T
    mm += SymbolicBFI(InnerProduct(Skw(r), tau))  # D^T
    mm += SymbolicBFI(0.5 * delta * w * div(sig))  # (∆t/2)B^T
    mm += SymbolicBFI(-v * w)                # -C
    mm += SymbolicBFI(InnerProduct(Skw(q), sig))  # D
    mm += SymbolicBFI(0.5 * delta * l * (tau * n),
                     element_boundary=True)
    mm += SymbolicBFI(0.5 * delta * m * (sig * n),
                     element_boundary=True)

    mm.Assemble()
    inv_m = mm.mat.Inverse(XY.FreeDofs(True))
    m_har = mm.harmonic_extension
    m_in = mm.inner_solve
    m_har_t = mm.harmonic_extension_trans

    # Make the S matrix
    s = ng.BilinearForm(XY, nonassemble=True, symmetric=True)
    s += SymbolicBFI(InnerProduct(sig, tau))                        # A
    s += SymbolicBFI(-0.5 * delta * v * div(tau))    # -(∆t/2)B^T
    s += SymbolicBFI(InnerProduct(Skw(r), tau))      # D^T
    s += SymbolicBFI(-v * w)                    # -C
    s += SymbolicBFI(-0.5 * delta * w * div(sig))  # -(∆t/2)B^T
    s += SymbolicBFI(InnerProduct(Skw(q), sig))          # D
    s += SymbolicBFI(-0.5 * delta * l * (tau * n),
                     element_boundary=True)         # -(∆t/2)E^T
    s += SymbolicBFI(-0.5 * delta * m * (sig * n),  # -(∆t/2)E^T
                     element_boundary=True)

    # Displacement Space recovered by trapezoidal rule
    u_dis = ng.GridFunction(U)
    u_dis.vec.data += initial.components[1].vec.data

    # Vector to store time dependent rhs  S * u + F in Crank Nicolson.
    f = ng.LinearForm(XY)
    f.Assemble()
    t = 0

    with ng.TaskManager():
        while t < tend:

            s.Apply(u.vec, f.vec)   # write S*u into ff

            # assemble time dependent source using F(t)
            li = ng.LinearForm(XY)
            Ft = F(t)
            Ftt = F(t + delta)
            li += SymbolicLFI(0.5 * delta * (Ft * w + Ftt * w))
            li.Assemble()

            f.vec.data += li.vec    # assembled  S * u(t) + F(t)
            f.vec.data += m_har_t * f.vec
            u_dis.vec.data += 0.5 * delta * u.components[1].vec.data

            # Solving U_{N+1} by  u(t+∆t) = M^{-1} *(S*u(t) + F(t))
            u.vec.data = inv_m * f.vec
            u.vec.data += m_har * u.vec
            u.vec.data += m_in * f.vec

            u_dis.vec.data += 0.5 * delta * u.components[1].vec.data

            t += delta
            print('t=%g' % t)
            ng.Redraw(blocking=True)

    # Compare with exact solution

    # Exact velocity
    ex_v1 = CoefficientFunction(sin(pi * x) * sin(pi * y) * sin(pi * z))
    ex_v2 = CoefficientFunction(x * (1 - x) * y * (1 - y) * z * (1 - z))
    ex_v3 = CoefficientFunction(0)

    ex_v = CoefficientFunction((ex_v1, ex_v2, ex_v3))
    # Exact sig
    ex_s0 = (pi * cos(pi * x) * sin(pi * y) * sin(pi * z)) * t
    ex_s1 = (0.5 * (pi * sin(pi * x) * cos(pi * y) * sin(pi * z) +
                    (1 - 2 * x) * y * (1 - y) * z * (1 - z))) * t
    ex_s2 = (0.5 * pi * sin(pi * x) * sin(pi * y) * cos(pi * z)) * t
    ex_s3 = ex_s1
    ex_s4 = (1 - 2 * y) * x * (1 - x) * z * (1 - z) * t
    ex_s5 = (0.5 * (1 - 2 * z) * x * (1 - x) * y * (1 - y)) * t
    ex_s6 = ex_s2
    ex_s7 = ex_s5
    ex_s8 = 0
    # Exact q
    ex_q1 = -0.5 * (1 - 2 * z) * x * (1 - x) * y * (1 - y) * t
    ex_q2 = 0.5 * pi * sin(pi * x) * sin(pi * y) * cos(pi * z) * t
    ex_q3 = -0.5 * (pi * sin(pi * x) * cos(pi * y) * sin(pi * z) -
                    (1 - 2 * x) * y * (1 - y) * z * (1 - z)) * t

    ex_q = CoefficientFunction((ex_q1, ex_q2, ex_q3))

    # Exact u
    ex_u1 = CoefficientFunction(sin(pi * x) * sin(pi * y) * sin(pi * z) * t)
    ex_u2 = CoefficientFunction(x * (1 - x) * y * (1 - y) * z * (1 - z) * t)
    ex_u3 = CoefficientFunction((0.0))
    ex_u = CoefficientFunction((ex_u1, ex_u2, ex_u3))

    ex_s = CoefficientFunction((ex_s0, ex_s1, ex_s2,
                                ex_s3, ex_s4, ex_s5,
                                ex_s6, ex_s7, ex_s8), dims=(3, 3))

    norm_v = ng.sqrt(ng.Integrate((u.components[1] - ex_v) *
                                  (u.components[1] - ex_v), mesh))

    norm_u = ng.sqrt(ng.Integrate((u_dis - ex_u) *
                                  (u_dis - ex_u), mesh))

    norm_s = ng.sqrt(ng.Integrate(InnerProduct(u.components[0] - ex_s,
                                               u.components[0] - ex_s), mesh))

    norm_r = ng.sqrt(ng.Integrate((u.components[2] - ex_q) *
                                  (u.components[2] - ex_q), mesh))

    return (norm_s, norm_v, norm_u, norm_r)


def hconvergencetable(e_1, e_2, e_3, e_4, maxr):
    print("===========================================" +
          "==================================")
    print(" Mesh  Errors_s   Order    Error_v    Order ",
          "Error_u   Order  Error_r   Order")
    print("------------------------------------------" +
          "----------------------------------")
    rate1 = []
    rate2 = []
    rate3 = []
    rate4 = []
    for i in range(maxr):
        rate1.append('  *  ')
        rate2.append('  *  ')
        rate3.append('  *  ')
        rate4.append('  *  ')
    for i in range(1, maxr):
        if abs(e_1[i]) > 1.e-15 and abs(e_2[i]) > 1.e-15:
            rate1[i] = format(ng.log(e_1[i - 1] / e_1[i]) / ng.log(2), '+5.2f')
            rate2[i] = format(ng.log(e_2[i - 1] / e_2[i]) / ng.log(2), '+5.2f')
            rate3[i] = format(ng.log(e_3[i - 1] / e_3[i]) / ng.log(2), '+5.2f')
            rate4[i] = format(ng.log(e_4[i - 1] / e_4[i]) / ng.log(2), '+5.2f')
    for i in range(maxr):
        print("h/ % -4d %8.2e   %s   %8.2e   %s  %8.2e  %s  %8.2e  %s" %
              (pow(2, i + 1), e_1[i], rate1[i], e_2[i], rate2[i],
               e_3[i], rate3[i], e_4[i], rate4[i]))

    print("======================================" +
          "=======================================")


def collecterrors(k, maxr, lam, tend):
    l2e_s = []
    l2e_v = []
    l2e_u = []
    l2e_r = []

    for l in range(0, maxr):
        hl = 2 ** (-l) / 2
        er_1, er_2, er_3, er_4 = weaksym_dynamic_hybrid3d(k, hl, lam, tend)
        l2e_s.append(er_1)
        l2e_v.append(er_2)
        l2e_u.append(er_3)
        l2e_r.append(er_4)
    return l2e_s, l2e_v, l2e_u, l2e_r

# ================= MAIN DRIVER ===============================


maxlevels = 3
tend = 2
l = 1
p = 1
er_s, er_v, er_u, er_r = collecterrors(p, maxlevels, l, tend)
hconvergencetable(er_s, er_v, er_u, er_r, maxlevels)


#  Rates:

# p=1, tend = 2 , l=1, maxlevels = 3
# ===============================================================
# Mesh  Errors_s   Order   Error_v    Order  Error_u   Order  Error_r   Order
# ----------------------------------------------------------------------------
# h/2    1.21e+00     *     1.91e-01     *    3.96e-01    *    6.91e-01    *
# h/4    2.08e-01   +2.53   8.59e-02   +1.16  1.66e-01  +1.25  1.33e-01  +2.38
# h/8    5.99e-02   +1.80   4.50e-02   +0.93  8.78e-02  +0.92  3.96e-02  +1.75
# ================================================================
# p=2, tend = 2 , l=1, maxlevels = 3
# ===============================================================
# Mesh  Errors_s   Order   Error_v    Order  Error_u   Order  Error_r   Order
# ----------------------------------------------------------------------------
# h/2    1.12e-01     *     3.93e-02     *    6.98e-02    *    6.50e-02    *
# h/4    1.61e-02   +2.80   1.22e-02   +1.69  2.40e-02  +1.54  1.12e-02  +2.53
# h/8    2.60e-03   +2.63   3.29e-03   +1.89  6.49e-03  +1.89  1.89e-03  +2.57
# =================================================================
