"""
Solve a static linear elasticity problem with the weakly
symmetric mixed method using the GG stress element
on incompressible material when Poisson ratio =0.5.

(A(sig), tau) + (u, div tau) + (skew(q), tau) +(cc,tr(tau)) = 0.
(div sig, v)                                = (f,w)
(sig, skew(q))                              = 0.0
(tr(sig), cx)                                 = 0.0,

where A is the inverse of the elasticity tensor C
(in the equation sigma = C e(u)) given by

C e(u) = lambda* tr(e(u)) I + 2*mu * e(u).

When Poisson’s ratio    nu = 1/2,
lam = 2*mu*nu/(1 -2*nu ) is infinity.
We perform a convergence study with this exact solution:

 u(x) = [  x^2*(1-x)^2*y*(1-y)*(1-2y)*z*(1-z) ]
        [ -y^2*(1-y)^2*x*(1-x)*(1-2x)*z*(1-z) ]
        [                 0.0                 ]
"""
import ngsolve as ng
from ngsolve import CoefficientFunction, SymbolicBFI, log
from ngsolve import sqrt, Integrate, FESpace, InnerProduct
from ngsolve import div, x, y, z
from netgen.csg import unit_cube
from ctypes import CDLL

mylngs = CDLL("../libelas.so")
ng.ngsglobals.msg_level = 1

# parameters:
mu = 0.5
nu = 0.5

# Making an exact solution:


def exact(mu):
    # displacement u
    u_1 = x * x * (1 - x) * (1 - x) * y * (1 - y) * (1 - 2 * y) * z * (1 - z)
    u_2 = -y * y * (1 - y) * (1 - y) * x * (1 - x) * (1 - 2 * x) * z * (1 - z)
    u_3 = 0.0

    ex_u = CoefficientFunction((u_1, u_2, u_3))

    # Derivatives to compute sigma, skew(q), and f.
    dxu_1 = 2 * x * (1 - x) * (1 -
                               2 * x) * y * (1 - y) * (1 - 2 * y) * z * (1 - z)
    dyu_1 = x * x * (1 - x) * (1 - x) * (6 * y * y - 6 * y + 1) * z * (1 - z)
    dzu_1 = x * x * (1 - x) * (1 - x) * y * (1 - y) * (1 - 2 * y) * (1 - 2 * z)

    dxxu_1 = 2 * (6 * x * x -
                  6 * x + 1) * y * (1 - y) * (1 - 2 * y) * z * (1 - z)
    dyxu_1 = 2 * x * (1 - x) * (1 -
                                2 * x) * (6 * y * y - 6 * y + 1) * z * (1 - z)
    dzxu_1 = 2 * x * (1 - x) * (1 -
                                2 * x) * y * (1 -
                                              y) * (1 - 2 * y) * (1 - 2 * z)

    dyyu_1 = x * x * (1 - x) * (1 - x) * (12 * y - 6) * z * (1 - z)
    dzzu_1 = -2 * x * x * (1 - x) * (1 - x) * y * (1 - y) * (1 - 2 * y)

    dxu_2 = -y * y * (1 - y) * (1 - y) * (6 * x * x - 6 * x + 1) * z * (1 - z)
    dyu_2 = -2 * (y * (1 - y) * (1 - 2 * y) *
                  x * (1 - x)) * (1 - 2 * x) * z * (1 - z)
    dzu_2 = -y * y * (1 - y) * (1 -
                                y) * x * (1 - x) * (1 - 2 * x) * (1 - 2 * z)

    dxxu_2 = -y * y * (1 - y) * (1 - y) * (12 * x - 6) * z * (1 - z)

    dyyu_2 = -2 * (6 * y * y - 6 * y +
                   1) * x * (1 - x) * (1 - 2 * x) * z * (1 - z)
    dyxu_2 = -2 * y * (1 - y) * (1 - 2 * y) * (6 * x * x -
                                               6 * x + 1) * z * (1 - z)
    dyzu_2 = -2 * y * ((1 - y) * (1 - 2 * y) *
                       x * (1 - x)) * (1 - 2 * x) * (1 - 2 * z)

    dzzu_2 = 2 * y * y * (1 - y) * (1 - y) * x * (1 - x) * (1 - 2 * x)

    # skew(q) = (0, -q3, q2, q3, 0, -q1, q2, q1, 0)
    q1 = -0.5 * dzu_2
    q2 = 0.5 * dzu_1
    q3 = 0.5 * (dxu_2 - dyu_1)
    ex_q = CoefficientFunction((q1, q2, q3))
    # Exact right hand side:
    F0 = 2 * mu * dxxu_1 + mu * (dyyu_1 + dyxu_2) + mu * dzzu_1
    F1 = mu * (dyxu_1 + dxxu_2) + 2 * mu * dyyu_2 + mu * dzzu_2
    F2 = mu * (dzxu_1) + mu * (dyzu_2)

    F = CoefficientFunction((F0, F1, F2))

    # e(u)
    ee = (dxu_1, 0.5 * (dyu_1 + dxu_2), 0.5 * (dzu_1),
          0.5 * (dyu_1 + dxu_2), dyu_2, 0.5 * (dzu_2),
          0.5 * dzu_1, 0.5 * dzu_2, 0.0)

    #   C e(u) = lambda* tr(e(u)) I + 2*mu * e(u)

    def C(ee):
        C0 = 2 * mu * ee[0]  # + lambda* tr(e(u))
        C4 = 2 * mu * ee[4]  # + lambda* tr(e(u))
        C8 = 2 * mu * ee[8]  # + lambda* tr(e(u))
        #
        C1 = 2 * mu * ee[1]
        C2 = 2 * mu * ee[2]
        C3 = 2 * mu * ee[3]
        C5 = 2 * mu * ee[5]
        C6 = 2 * mu * ee[6]
        C7 = 2 * mu * ee[7]

        return CoefficientFunction((C0, C1, C2,
                                    C3, C4, C5,
                                    C6, C7, C8), dims=(3, 3))

    ex_s = C(ee)
    return ex_s, ex_u, ex_q, F


def AA(ss, mu):
    """ The inverse of C is given by
    AA sigma = 0.5/ mu * sigma - lam/(2mu(3*lam + 2 mu)) tr(sigma)I_3  """

    a1 = 0.5 / mu                       # Compliance tensor for isotropic mat
    a2 = 0.5 / (mu * 3)

    As0 = a1 * ss[0] - a2 * (ss[0] + ss[4] + ss[8])
    As4 = a1 * ss[4] - a2 * (ss[0] + ss[4] + ss[8])
    As8 = a1 * ss[8] - a2 * (ss[0] + ss[4] + ss[8])

    As1 = a1 * ss[1]
    As2 = a1 * ss[2]
    As3 = a1 * ss[3]
    As5 = a1 * ss[5]
    As6 = a1 * ss[6]
    As7 = a1 * ss[7]

    return CoefficientFunction((As0, As1, As2,
                                As3, As4, As5,
                                As6, As7, As8), dims=(3, 3))


def Skw(x):
    return CoefficientFunction((0, -x[2], x[1],
                                x[2], 0, -x[0],
                                -x[1], x[0], 0), dims=(3, 3))


def weaksym_incomp3d(k, h):

    mesh = ng.Mesh(unit_cube.GenerateMesh(maxh=h))

    # Stress space S:
    S = FESpace("ggfes3d", mesh, order=k, flags={"nobubbles": False})
    # Displacement space U is two copies of
    U = ng.VectorL2(mesh, order=k - 1)
    # Antisymmetric matrix space for rotations A:
    A = ng.VectorL2(mesh, order=k)
    C = FESpace("number", mesh)

    X = FESpace([S, U, A, C])

    sig, u, r, cc = X.TrialFunction()
    tau, w, q, cx = X.TestFunction()

    Asig = AA(sig, mu)

    ex_s, ex_u, ex_q, F = exact(mu)

    a = ng.BilinearForm(X, symmetric=True)
    a += SymbolicBFI(InnerProduct(Asig, tau))
    a += SymbolicBFI(u * div(tau))
    a += SymbolicBFI(InnerProduct(Skw(r), tau))
    a += SymbolicBFI(cc * (tau[0] + tau[4] + tau[8]))

    a += SymbolicBFI(div(sig) * w)
    a += SymbolicBFI(InnerProduct(sig, Skw(q)))
    a += SymbolicBFI(cx * (sig[0] + sig[4] + sig[8]))

    f = ng.LinearForm(X)
    f += ng.SymbolicLFI(F * w)

    c = ng.Preconditioner(a, type="direct", flags={"inverse": "umfpack"})
    a.Assemble(heapsize=int(1e9))
    f.Assemble(heapsize=int(1e9))
    u = ng.GridFunction(X)
    ng.BVP(bf=a, lf=f, gf=u, pre=c, maxsteps=1000).Do()

    norm_s = sqrt(Integrate(InnerProduct(u.components[0] - ex_s,
                                         u.components[0] - ex_s), mesh))

    norm_u = sqrt(Integrate((u.components[1] - ex_u) *
                            (u.components[1] - ex_u), mesh))

    norm_r = sqrt(Integrate((u.components[2] - ex_q) *
                            (u.components[2] - ex_q), mesh))

    return norm_s, norm_u, norm_r


def hconvergencetable(e_1, e_2, e_3, maxr):
    print("============================================================")
    print(" Mesh   Errors_s  Order    Error_u   Order   Error_r   Order")
    print("------------------------------------------------------------")
    rate1 = []
    rate2 = []
    rate3 = []
    for i in range(maxr):
        rate1.append('  *  ')
        rate2.append('  *  ')
        rate3.append('  *  ')
    for i in range(1, maxr):
        if abs(e_1[i]) > 1.e-15 and abs(e_2[i]) > 1.e-15:
            rate1[i] = format(log(e_1[i - 1] / e_1[i]) / log(2), '+5.2f')
            rate2[i] = format(log(e_2[i - 1] / e_2[i]) / log(2), '+5.2f')
            rate3[i] = format(log(e_3[i - 1] / e_3[i]) / log(2), '+5.2f')
    for i in range(maxr):
        print(" h/%-4d %8.2e   %s   %8.2e   %s  %8.2e  %s" %
              (pow(2, i + 1), e_1[i], rate1[i],
               e_2[i], rate2[i], e_3[i], rate3[i]))

    print("============================================================")


def collecterrors(k, maxr):
    l2e_s = []
    l2e_u = []
    l2e_r = []
    for l in range(0, maxr):
        hl = 2**(-l) / 2
        er_1, er_2, er_3 = weaksym_incomp3d(k, hl)
        l2e_s.append(er_1)
        l2e_u.append(er_2)
        l2e_r.append(er_3)
    print("============================================================")
    print("  Error :")
    return l2e_s, l2e_u, l2e_r


maxlevels = 3

er_s, er_u, er_r = collecterrors(1, maxlevels)
hconvergencetable(er_s, er_u, er_r, maxlevels)


# ============================================================
# p =1 ( precision )
# ============================================================
# ============================================================
#   Error :
# ============================================================
#  Mesh   Errors_s  Order  Error_u     Order   Error_r   Order
# ------------------------------------------------------------
# h/2    1.83e-03     *     4.18e-04     *    1.08e-03    *
# h/4    5.12e-04   +1.84   2.36e-04   +0.82  3.46e-04  +1.65
# h/8    1.44e-04   +1.83   1.27e-04   +0.89  9.81e-05  +1.81
# ============================================================
# ===========================================================
# p =2 (precision)
# =====================================================
# =============================================================
#  Error :
#  ============================================================
#   Mesh   Errors_s  Order  Error_u     Order   Error_r   Order
#  ------------------------------------------------------------
#  h/2    4.75e-04     *     1.64e-04     *    3.49e-04    *
#  h/4    6.71e-05   +2.82   5.29e-05   +1.63  4.58e-05  +2.93
# h/8    9.36e-06   +2.84   1.53e-05   +1.79  6.59e-06  +2.80
# ============================================================#

# ============================================================
#  Error : p=3
#  ============================================================
#  Mesh   Errors_s  Order  Error_u     Order   Error_r   Order
# ------------------------------------------------------------
# h/2    1.38e-04     *     3.93e-05     *    8.92e-05    *
# h/4    9.20e-06   +3.90   4.20e-06   +3.22  5.71e-06  +3.97
# h/8    5.91e-07   +3.96   5.92e-07   +2.83  3.81e-07  +3.90
# ============================================================

# ============================================================
#  Error :
# ============================================================
# Mesh   Errors_s  Order  Error_u     Order   Error_r   Order
# ------------------------------------------------------------
# h/2    3.12e-05     *     1.20e-05     *    2.21e-05    *
# h/4    8.00e-07   +5.28   8.54e-07   +3.81  4.67e-07  +5.56
# h/8    2.58e-08   +4.95   5.99e-08   +3.83  1.52e-08  +4.94
