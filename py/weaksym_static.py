""" Solve a static linear elasticity problem with the weakly
 symmetric mixed method using the GG stress element

  (sigma, tau) + (u, div tau) + (skew(l), tau)  = 0.0
  (div sigma, v)                                = (f,w)
  (sigma, skew(q))				 = 0.0

 and perform a convergence study with this exact solution:

 u(x) = [ sin(pi*x)*sin(pi*y) ]
        [ x*(1-x)* y*(1-y)    ]

 The corresponding source is

  F_1 = ( -3*pi*pi/2)*sin(pi*x)*sin(pi*y) +(2*x-1)*(2*y)/2

  F_2 = (-y*(1-y)-2*x*(1-x)+pi*pi*cos(pi*x)*cos(pi*y)).

"""

import ngsolve as ng
from ngsolve import CoefficientFunction, SymbolicBFI, SymbolicLFI
from ngsolve import sin, cos, div, sqrt, Integrate, InnerProduct
from netgen.geom2d import unit_square
from numpy import pi
from ctypes import CDLL

mylngs = CDLL("../libelas.so")
ng.ngsglobals.msg_level = 1

x = ng.x
y = ng.y


def weaksym(k, h):

    mesh = ng.Mesh(unit_square.GenerateMesh(maxh=h))

    # Stress space S:
    S = ng.FESpace("ggfes", mesh, order=k)
    # Displacement space U is two copies of
    U = ng.VectorL2(mesh, order=k - 1)
    # Antisymmetric matrix space for rotations A:
    A = ng.L2(mesh, order=k)
    XY = ng.FESpace([S, U, A])

    sigma, u, mu = XY.TrialFunction()
    tau, v, q = XY.TestFunction()

    F = CoefficientFunction((-1.5 * pi * pi * sin(pi * x) * sin(pi * y) +
                             0.5 * (1 - 2 * x) * (1 - 2 * y),
                             -y * (1 - y) - 2 * x * (1 - x) +
                             0.5 * pi * pi * cos(pi * x) * cos(pi * y)))

    a = ng.BilinearForm(XY, symmetric=True)
    a += SymbolicBFI(InnerProduct(sigma, tau))         # (sigma, tau)
    a += SymbolicBFI(u * div(tau))                     # (u, div tau)
    a += SymbolicBFI(mu * (tau[0, 1] - tau[1, 0]))     # (mu, skw tau)
    a += SymbolicBFI(div(sigma) * v)                   # (div sigma, v)
    a += SymbolicBFI((sigma[0, 1] - sigma[1, 0]) * q)  # (q, skw sigma)

    f = ng.LinearForm(XY)
    f += SymbolicLFI(F * v)

    c = ng.Preconditioner(a, type="direct")
    a.Assemble()
    f.Assemble()

    sua = ng.GridFunction(XY)
    ng.BVP(bf=a, lf=f, gf=sua, pre=c, maxsteps=10).Do()

    exactu = CoefficientFunction((sin(pi * x) * sin(pi * y),
                                  x * (1 - x) * y * (1 - y)))
    s00 = pi * cos(pi * x) * sin(pi * y)
    s01 = 0.5 * (pi * sin(pi * x) * cos(pi * y) + (1 - 2 * x) * y * (1 - y))
    s10 = s01
    s11 = (1 - 2 * y) * x * (1 - x)
    exacts = CoefficientFunction((s00, s01,
                                  s10, s11), dims=(2, 2))
    exactq = CoefficientFunction(0.5 * (pi * sin(pi * x) * cos(pi * y) -
                                        (1 - 2 * x) * y * (1 - y)))

    norm_u = sqrt(Integrate((sua.components[1] - exactu) *
                            (sua.components[1] - exactu), mesh))
    norm_r = sqrt(Integrate((sua.components[2] - exactq) *
                            (sua.components[2] - exactq), mesh))
    norm_s = sqrt(Integrate(InnerProduct(sua.components[0] - exacts,
                                         sua.components[0] - exacts), mesh))

    return norm_s, norm_u, norm_r


def hconvergencetable(e_1, e_2, e_3, maxr):
    print("============================================================")
    print(" Mesh   Errors_s  Order    Error_u   Order   Error_r   Order")
    print("------------------------------------------------------------")
    rate1 = []
    rate2 = []
    rate3 = []
    for i in range(maxr):
        rate1.append('  *  ')
        rate2.append('  *  ')
        rate3.append('  *  ')
    for i in range(1, maxr):
        if abs(e_1[i]) > 1.e-15 and abs(e_2[i]) > 1.e-15:
            rate1[i] = format(ng.log(e_1[i - 1] / e_1[i]) / ng.log(2), '+5.2f')
            rate2[i] = format(ng.log(e_2[i - 1] / e_2[i]) / ng.log(2), '+5.2f')
            rate3[i] = format(ng.log(e_3[i - 1] / e_3[i]) / ng.log(2), '+5.2f')
    for i in range(maxr):
        print(" h/%-4d %8.2e   %s   %8.2e   %s  %8.2e  %s" %
              (pow(2, i + 1), e_1[i], rate1[i], e_2[i],
               rate2[i], e_3[i], rate3[i]))

    print("============================================================")


def collecterrors(k, maxr):
    l2e_s = []
    l2e_u = []
    l2e_r = []
    for l in range(0, maxr):
        hl = 2 ** (-l) / 2
        er_1, er_2, er_3 = weaksym(k, hl)
        l2e_s.append(er_1)
        l2e_u.append(er_2)
        l2e_r.append(er_3)
    return l2e_s, l2e_u, l2e_r


# ============= MAIN DRIVER ==============================

maxlevels = 5
er_s, er_u, er_r = collecterrors(2, maxlevels)
hconvergencetable(er_s, er_u, er_r, maxlevels)
