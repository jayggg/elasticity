"""
Solve a static linear elasticity problem in 3 dimension with the weakly
symmetric mixed method using the GG stress element
  (sig, tau) + (u, div tau) + (skew(r), tau)  + <l,tau.n> = 0
  (div sig, v)                                            = (f,w)
  (sig, skew(q))				                          = 0
  <sig.n, m>                                              = 0

 and perform a convergence study with this exact solution:

 u(x) = [ sin(pi*x)*sin(pi*y)*sin(pi*z) ]
        [ x*(1-x)*y*(1-y)*z*(1-z)       ]
        [         0                     ]
"""
import ngsolve as ng
from ngsolve import CoefficientFunction, SymbolicBFI, SymbolicLFI, sin, cos
from ngsolve import InnerProduct, div, x, y, z
from netgen.csg import unit_cube
from numpy import pi
from ctypes import CDLL

mylngs = CDLL("../libelas.so")
ng.ngsglobals.msg_level = 5

ng.SetHeapSize(int(1e8))


def weaksym_hybrid3d(k, h):

    mesh = ng.Mesh(unit_cube.GenerateMesh(maxh=h))

    # Stress space S:
    S = ng.FESpace("ggfes3d", mesh, order=k, flags={'discontinuous': True})
    # Displacement space U is two copies of
    U = ng.VectorL2(mesh, order=k - 1)
    # Antisymmetric matrix space for rotations A:
    A = ng.VectorL2(mesh, order=k)
    # Lagrange multiplier space
    L = ng.FESpace("facet", mesh, order=k, dirichlet=[1, 2, 3, 4, 5, 6])

    # Full product FE space
    X = ng.FESpace([S, U, A, L, L, L])

    sig, u, r, l1, l2, l3 = X.TrialFunction()
    tau, w, q, m1, m2, m3 = X.TestFunction()

    def Skw(r):
        return CoefficientFunction((0, -r[2], r[1],
                                    r[2], 0, -r[0],
                                    -r[1], r[0], 0), dims=(3,3))
    
    
    n = ng.specialcf.normal(mesh.dim)
    l = CoefficientFunction((l1, l2, l3))
    m = CoefficientFunction((m1, m2, m3))

    F0 = CoefficientFunction(-2 * pi * pi * sin(pi * x) *
                             sin(pi * y) * sin(pi * z) +
                             0.5 * (1 - 2 * x) * (1 - 2 * y) *
                             z * (1 - z))
    F1 = CoefficientFunction(-y * (1 - y) * z * (1 - z) -
                             2 * x * (1 - x) * z * (1 - z) -
                             x * (1 - x) * y * (1 - y) +
                             0.5 * pi * pi * cos(pi * x) *
                             cos(pi * y) * sin(pi * z))
    F2 = CoefficientFunction(0.5 * pi * pi * cos(pi * x) *
                             sin(pi * y) * cos(pi * z) +
                             0.5 * (1 - 2 * z) * (1 - 2 * y) *
                             x * (1 - x))

    a = ng.BilinearForm(X, symmetric=True, eliminate_internal=True)
    a += SymbolicBFI(InnerProduct(sig, tau))                     # (sigma, tau)
    a += SymbolicBFI(u * div(tau))
    a += SymbolicBFI(InnerProduct(Skw(r),tau))
    a += SymbolicBFI(div(sig) * w)
    a += SymbolicBFI(InnerProduct(sig, Skw(q)))
    a += SymbolicBFI(l * (tau * n), element_boundary=True)
    a += SymbolicBFI(m * (sig * n), element_boundary=True)

    f = ng.LinearForm(X)
    f += SymbolicLFI(F0 * w[0] + F1 * w[1] + F2 * w[2])

    c = ng.Preconditioner(a, type="direct")
    a.Assemble()
    f.Assemble()

    u = ng.GridFunction(X)
    ng.BVP(bf=a, lf=f, gf=u, pre=c, maxsteps=10).Do()

    ex_u0 = CoefficientFunction(sin(pi * x) * sin(pi * y) * sin(pi * z))
    ex_u1 = CoefficientFunction(x * (1 - x) * y * (1 - y) * z * (1 - z))
    ex_u2 = CoefficientFunction(0)
    ex_u = CoefficientFunction((ex_u0, ex_u1, ex_u2))

    ex_s0 = CoefficientFunction(pi * cos(pi * x) * sin(pi * y) * sin(pi * z))
    ex_s1 = CoefficientFunction(0.5 * (pi * sin(pi * x) * cos(pi * y) *
                                       sin(pi * z) + (1 - 2 * x) * y *
                                       (1 - y) * z * (1 - z)))
    ex_s2 = CoefficientFunction(0.5 * pi * sin(pi * x) *
                                sin(pi * y) * cos(pi * z))
    ex_s3 = ex_s1
    ex_s4 = CoefficientFunction((1 - 2 * y) * x * (1 - x) * z * (1 - z))
    ex_s5 = CoefficientFunction(0.5 * (1 - 2 * z) * x * (1 - x) * y * (1 - y))
    ex_s6 = ex_s2
    ex_s7 = ex_s5
    ex_s8 = CoefficientFunction(0.0)

    ex_s = CoefficientFunction((ex_s0, ex_s1, ex_s2,
                                ex_s3, ex_s4, ex_s5,
                                ex_s6, ex_s7, ex_s8))

    ex_q1 = CoefficientFunction(-0.5 * (1 - 2 * z) * x *
                                (1 - x) * y * (1 - y))

    ex_q2 = CoefficientFunction(0.5 * pi * sin(pi * x) *
                                sin(pi * y) * cos(pi * z))

    ex_q3 = CoefficientFunction(-0.5 * (pi * sin(pi * x) * cos(pi * y) *
                                        sin(pi * z) - (1 - 2 * x) * y *
                                        (1 - y) * z * (1 - z)))

    ex_q = CoefficientFunction((ex_q1, ex_q2, ex_q3))
    
    norm_u = ng.sqrt(ng.Integrate((u.components[1] - ex_u) *
                                  (u.components[1] - ex_u), mesh))

    norm_s = ng.sqrt(ng.Integrate(InnerProduct(u.components[0] - ex_s,
                                               u.components[0] - ex_s), mesh))

    norm_r = ng.sqrt(ng.Integrate((u.components[2] - ex_q) *
                                  (u.components[2] - ex_q), mesh))

    return norm_s, norm_u, norm_r


def hconvergencetable(e_1, e_2, e_3, maxr):
    print("============================================================")
    print(" Mesh   Errors_s  Order    Error_u   Order   Error_r   Order")
    print("------------------------------------------------------------")
    rate1 = []
    rate2 = []
    rate3 = []
    for i in range(maxr):
        rate1.append('  *  ')
        rate2.append('  *  ')
        rate3.append('  *  ')
    for i in range(1, maxr):
        if abs(e_1[i]) > 1.e-15 and abs(e_2[i]) > 1.e-15:
            rate1[i] = format(ng.log(e_1[i - 1] / e_1[i]) / ng.log(2), '+5.2f')
            rate2[i] = format(ng.log(e_2[i - 1] / e_2[i]) / ng.log(2), '+5.2f')
            rate3[i] = format(ng.log(e_3[i - 1] / e_3[i]) / ng.log(2), '+5.2f')
    for i in range(maxr):
        print(" h/%-4d %8.2e   %s   %8.2e   %s  %8.2e  %s" %
              (pow(2, i + 1), e_1[i], rate1[i], e_2[i],
               rate2[i], e_3[i], rate3[i]))

    print("============================================================")


def collecterrors(k, maxr):
    l2e_s = []
    l2e_u = []
    l2e_r = []
    for l in range(0, maxr):
        hl = 2**(-l) / 2
        er_1, er_2, er_3 = weaksym_hybrid3d(k, hl)
        l2e_s.append(er_1)
        l2e_u.append(er_2)
        l2e_r.append(er_3)
    return l2e_s, l2e_u, l2e_r


maxlevels = 3
er_s, er_u, er_r = collecterrors(2, maxlevels)
hconvergencetable(er_s, er_u, er_r, maxlevels)

# p=1
# ============================================================
# Mesh   Errors_s  Order  Error_v     Order   Error_r   Order
# ------------------------------------------------------------
#  h/2    6.17e-01     *     2.00e-01     *    3.46e-01    *
#  h/4    1.12e-01   +2.46   8.36e-02   +1.26  6.93e-02  +2.32
#  h/8    3.19e-02   +1.81   4.40e-02   +0.93  2.03e-02  +1.77
#  h/16   9.16e-03   +1.80   2.35e-02   +0.91  5.87e-03  +1.79
# ============================================================


# p=2
# ============================================================
# Mesh   Errors_s  Order  Error_v     Order   Error_r   Order
# ------------------------------------------------------------
# h/2    5.53e-02     *     3.49e-02     *    3.13e-02    *
# h/4    7.83e-03   +2.82   1.20e-02   +1.54  5.43e-03  +2.53
# h/8    1.21e-03   +2.69   3.24e-03   +1.89  8.64e-04  +2.65
# ============================================================

# p =3

# ============================================================
# Mesh   Errors_s  Order  Error_v     Order   Error_r   Order
# ------------------------------------------------------------
# h/2    3.02e-02     *     1.45e-02     *    1.60e-02    *
# h/4    7.91e-04   +5.25   6.96e-04   +4.38  4.56e-04  +5.14
# h/8    6.24e-05   +3.66   8.04e-05   +3.11  3.63e-05  +3.65
# ============================================================
