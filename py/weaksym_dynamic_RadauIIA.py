"""
 Equations of dynamic linear elasticity for stress sigma, velocity v,
 and rotations r:

  (sigma', tau) + (v, div tau) + (Skw(r'), tau) = 0
 -(div sigma, w) + (v', w)                       = (F,w)
  (sigma', Skw(q))			          = 0

 Here  Skw(m) = [ 0 m]  and prime denotes time derivative.
                [-m 0]

 We perform spatial discretization with GG elements, implicit time
 stepping  using 2-stage RadauIIA

 Follow the Butcher tableau:

 ----------------------------
  1/3 |  5/12       -1/12
   1  |  3/4         1/4
 ----------------------------
        3/4          1/4


 and measure convergence rates
 when the exact solution is

 u(x,y,t) = [ sin(pi*x)*sin(pi*y)*sin(t) ]
            [  x(1-x)*y(1-y)*sin(t)      ].



 Note that with this displacement, the initial velocity is

 v(x,y,0) = [ sin(pi*x)*sin(pi*y) ]
            [ x(1-x)y(1-y)        ],

 which forms the data for the simulation.

 u(x,y,t) is recover by integrating using Taylor's expansion:

 u(t+∆t)= u(t)+ (∆t)v(t)+ (∆t)^2/2*v'(t+∆t/3) + o((∆t/3)^3)

"""


from ngsolve import SymbolicBFI, SymbolicLFI, CoefficientFunction, sin, cos
from ngsolve import InnerProduct, div, x, y
import ngsolve as ng
from netgen.geom2d import unit_square
from numpy import pi

from ctypes import CDLL

mylngs = CDLL("../libelas.so")
ng.ngsglobals.msg_level = 1

# ========= INITIAL DATA =============================


def initdata():

    """ Return two vectors, representing the initial values of
    div(sigma(0)) and velocity v(0). (The remainder of the required
    initial data will be computed automatically.
    """

    divsigma0 = CoefficientFunction((0, 0))
    v0 = CoefficientFunction((sin(pi * x) * sin(pi * y),
                              x * (1 - x) * y * (1 - y)))

    return divsigma0, v0


# ========== SOURCE DATA ==========================


def F(t):
    F0 = sin(t) * (-sin(pi * x) * sin(pi * y) -
                   (-1.5 * pi * pi * sin(pi * x) * sin(pi * y) +
                    0.5 * (1 - 2 * x) * (1 - 2 * y)))
    F1 = sin(t) * (-x * (1 - x) * y * (1 - y) -
                   (-y * (1 - y) - 2 * x * (1 - x) +
                    0.5 * pi * pi * cos(pi * x) * cos(pi * y)))

    return CoefficientFunction((F0, F1))


def Skw(r):
    return CoefficientFunction((r[0, 1] -r[1 ,0]))
# =========== SOLVE ===============================


def weaksym_dynamic(k, h, l, tend):

    """ PARAMETERS:

    k: determines polynomial order of stress (in P_k + bubbles),
       velocity (in P_{k-1}) and rotations (in P_k).

    h: maximal mesh size (of the mesh of unit square)

    l: sets timestep by  ∆t = h * l

    tend: final simulation time where error is measured.

    """

    mesh = ng.Mesh(unit_square.GenerateMesh(maxh=h))
    delta = l * h   # time step size ∆t

    S = ng.FESpace("ggfes", mesh, order=k)  # Stress space
    U = ng.VectorL2(mesh, order=k - 1)      # Displacement space = L2 x L2
    A = ng.L2(mesh, order=k)                # Space of rotations (skw sym mat)
    XY = ng.FESpace([S, U, A])

    # Setting initial sigma, r values by solving a static problem:
    divsigma0, v0 = initdata()

    sig, v, r = XY.TrialFunction()
    tau, w, q = XY.TestFunction()

    a = ng.BilinearForm(XY, symmetric=True)
    a += SymbolicBFI(InnerProduct(sig, tau))
    a += SymbolicBFI(v * div(tau))
    a += SymbolicBFI(r * Skw(tau))
    a += SymbolicBFI(div(sig) * w)
    a += SymbolicBFI(Skw(sig) * q)

    fin = ng.LinearForm(XY)
    fin += ng.SymbolicLFI(divsigma0 * w)
    a.Assemble()
    fin.Assemble()

    initial = ng.GridFunction(XY)
    initial.vec.data = a.mat.Inverse(XY.FreeDofs()) * fin.vec

    # u stores initial data and will contain future time iterates
    u = ng.GridFunction(XY)
    u.components[0].Set(initial.components[0])  # Set sigma(0)
    u.components[2].Set(initial.components[2])  # Set r(0)

    # Set initial velocity by projecting v0 into fe space
    u.components[1].Set(v0)

    ng.Draw(u.components[1][0], mesh, "velocity", sd=2)

    """ RadauIIA in matrix form reads as
     M u'(t) = Cu(t) + F(t) := W(t,u)

      Using the implicit formula from the Butcher tableau:
     (1) M U_{N+1} = MU_{N} + (3∆t/4)W(t+∆t/3,U_p) + (∆t/4)W(t+∆t, U_pp)
     (2) M U_p = M u_{N} + (5∆t/12)W( t +∆t/3, U_p) -(∆t/12)W(t+∆t, U_pp)
     (3) M U_pp = M u_{N} + (3∆t/4)W( t +∆t/3, U_p) + (∆t/4)W(t+∆t, U_pp)
        Note that:  U_{N+1} = U_pp from (1) and (3)

        After some steps we obtain:
     [M - ∆t/4*C|-3∆t/4* C   ][ U_{N+1}]=M U_N + 3∆t/4 F(t+∆t/3) + ∆t/4 F(t+∆t)
     [ ∆t/12*C  | M -5*∆t/12*C][ U_p   ]=M U_N+5∆t/12 F(t+∆t/3) - ∆t/12 F(t+∆t)

       with
          [    A      0      D^T   ]
      M = [    0      C       0    ]
          [    D      0       0    ]

          [   0   -B^T   0  ]
      C = [   B    0     0  ]
          [   0    0     0  ]

      Setting:
      AA =  M - ∆t/4*C
      BB = -3∆t/4* C
      CC =  ∆t/12*C
      DD = M -5*∆t/12*C

      WW=[ AA & BB ],sol=[ U_{N+1}],ff=[M U_N + 3∆t/4 F(t+∆t/3) + ∆t/4 F(t+∆t)]
         [ CC & BB ]     [ U_p    ]  [M U_N + 5∆t/12 F(t+∆t/3) - ∆t/12 F(t+∆t)]

     We solve for sol,   WW*sol = ff
    """

    XYXY = ng.FESpace([S, U, A, S, U, A])
    sig_1, u1, r_1, sig_2, u2, r_2 = XYXY.TrialFunction()
    tau_1, v1, q_1, tau_2, v2, q_2 = XYXY.TestFunction()

    WW = ng.BilinearForm(XYXY)
    # AA
    WW += SymbolicBFI(InnerProduct(sig_1, tau_1))         # A
    WW += SymbolicBFI(r_1 * Skw(tau))                     # D^T
    WW += SymbolicBFI(u1* v1)                             # C
    WW += SymbolicBFI(Skw(sig_1) * q_1)                   # D
    WW += SymbolicBFI(delta / 4 * u1 * div(tau_1))        # - ∆t/4*B^T
    WW += SymbolicBFI(-delta / 4 * v1 * div(sig_1))       # ∆t/4*B^T
    # BB
    WW += SymbolicBFI(3 * delta / 4 * u2 * div(tau_1))    # -3*∆t/4*B^T
    WW += SymbolicBFI(-3 * delta / 4 * div(sig_2) * v1)   # 3*∆t/4*B

    # CC
    WW += SymbolicBFI(-delta / 12 * u1 * div(tau_2))      # ∆t/12*B^T
    WW += SymbolicBFI(delta / 12 * div(sig_1) * v2)       # -∆t/12*B
    # DD
    WW += SymbolicBFI(InnerProduct(sig_2, tau_2))         # A
    WW += SymbolicBFI(r_2 * Skw(tau_2))                   # D^T
    WW += SymbolicBFI(u2 * v2)                            # C
    WW += SymbolicBFI(Skw(sig_2) * q_2)                   # D
    WW += SymbolicBFI(5 * delta / 12 * u2 * div(tau_2))   # -5*∆t/12*B^T
    WW += SymbolicBFI(-5 * delta / 12 * v2 * div(sig_2))  # 5*∆t/12*B

    WW.Assemble()
    inv_WW = WW.mat.Inverse(XYXY.FreeDofs())

    # Make the m = [M & 0 // 0 & M]
    MM = ng.BilinearForm(XYXY, nonassemble=True)
    MM += SymbolicBFI(InnerProduct(sig_1, tau_1))           # A
    MM += SymbolicBFI(r_1 * Skw(tau_1))                     # D^T
    MM += SymbolicBFI(u1 * v1)                              # C
    MM += SymbolicBFI(Skw(sig_1) * q_1)                     # D
    #
    MM += SymbolicBFI(InnerProduct(sig_2, tau_2))  # A
    MM += SymbolicBFI(r_2 * Skw(tau_2))            # D^T
    MM += SymbolicBFI(u2 * v2)                     # C
    MM += SymbolicBFI(Skw(sig_2) * q_2)  # D

    ex_u1 = CoefficientFunction(sin(pi * x) * sin(pi * y) * sin(0))
    ex_u2 = CoefficientFunction(x * (1 - x) * y * (1 - y) * sin(0))

    # Make the C  matrix
    C = ng.BilinearForm(XY, nonassemble=True)
    C += SymbolicBFI(-v * div(tau))  # -B^T
    C += SymbolicBFI(div(sig) * w)   # B

    # Make  M matrix
    M = ng.BilinearForm(XY, symmetric=True)
    M += SymbolicBFI(InnerProduct(sig, tau))  # A
    M += SymbolicBFI(r * Skw(tau))            # D^T
    M += SymbolicBFI(v * w)                   # C
    M += SymbolicBFI(Skw(sig) * q)            # D
    M.Assemble()
    inv_M = M.mat.Inverse(XY.FreeDofs())

    # To obtain the displacement note that:
    # v'(t+∆t/3) = M^{-1} C U_p +F(t+∆t/3)

    # Displacement is recover by:
    # u(t+ht) = u(t)+ hv(t) + h^2/2 M^{-1}(C*U_p + F(t+h/3)

    u_dis = ng.GridFunction(U)
    u_dis.vec.data += initial.components[1].vec.data   # u(0)
 
    # To store U_p
    aux = ng.GridFunction(XY)

    # Set Solution sol = [ U_{N+1}, Up]^t , and initial sigma, and r
    sol = ng.GridFunction(XYXY)
    sol.components[0].Set(initial.components[0])  # Set sigma(0)
    sol.components[3].Set(initial.components[0])  # Set sigma(0)
    sol.components[2].Set(initial.components[2])  # Set r(0)
    sol.components[5].Set(initial.components[2])  # Set r(0)

    # Set initial velocity by projecting v0 into fe space
    sol.components[1].Set(v0)
    sol.components[4].Set(v0)

    # Vector to store time dependent rhs

    f = ng.LinearForm(XYXY)
    f.Assemble()
    ff = f.vec

    f2 = ng.LinearForm(XY)
    f2.Assemble()
    f_2 = f2.vec
    t = 0

    aux2 = ng.GridFunction(XY)

    with ng.TaskManager():
        while t < tend:
            # Obtaining right hand side
            # ff = [ M U_N + 3∆t/4 F(t+∆t/3) + ∆t/4 F(t+∆t) ]
            #     [ M U_N + 5∆t/12 F(t+∆t/3) - ∆t/12 F(t+∆t) ]

            MM.Apply(sol.vec, ff)
            li = ng.LinearForm(XYXY)
            Ft3 = F(t + delta / 3)
            Ftt = F(t + delta)
            li += SymbolicLFI(3 / 4 * delta * (Ft3 * v1) +
                              delta / 4 * (Ftt * v1))
            li += SymbolicLFI(5 / 12 * delta *
                              (Ft3 * v2) +
                              -delta / 12 * (Ftt * v2))
            li.Assemble()
            ff.data += li.vec

            # Recovering the displacement: u_dis += (∆t)v(t)
            u_dis.vec.data += delta * u.components[1].vec.data

            sol.vec.data = inv_WW * ff

            u.components[0].vec.data = sol.components[0].vec.data  # u(t+∆t)
            u.components[1].vec.data = sol.components[1].vec.data
            u.components[2].vec.data = sol.components[2].vec.data

            # Stores the U1
            aux.components[0].vec.data = sol.components[3].vec.data
            aux.components[1].vec.data = sol.components[4].vec.data
            aux.components[2].vec.data = sol.components[5].vec.data

            # Recovering the displacement:
            # u_dis += (∆t)^2 M^{-1}(C*U_p + F(t+∆t/3)
            C.Apply(aux.vec, f_2)
            li2 = ng.LinearForm(XY)
            li2 += SymbolicLFI(Ft3 * w)
            li2.Assemble()
            f_2.data += li2.vec

            aux2.vec.data = 0.5 * delta * delta * inv_M * f_2
            u_dis.vec.data += aux2.components[1].vec.data

            # updating solution:
            sol.components[3].vec.data = sol.components[0].vec.data
            sol.components[4].vec.data = sol.components[1].vec.data
            sol.components[5].vec.data = sol.components[2].vec.data

            t += delta
            print('t=%g' % t)
            ng.Redraw(blocking=True)

    # Compare with exact solution
    ex_v1 = CoefficientFunction(sin(pi * x) * sin(pi * y) * cos(t))
    ex_v2 = CoefficientFunction(x * (1 - x) * y * (1 - y) * cos(t))
    ex_v = CoefficientFunction((ex_v1, ex_v2))

    ex_s0 = pi * cos(pi * x) * sin(pi * y) * sin(t)
    ex_s1 = 0.5 * (pi * sin(pi * x) *
                   cos(pi * y) + (1 - 2 * x) * y * (1 - y)) * sin(t)
    ex_s2 = 0.5 * (pi * sin(pi * x) * cos(pi * y) +
                   (1 - 2 * x) * y * (1 - y)) * sin(t)
    ex_s3 = (1 - 2 * y) * x * (1 - x) * sin(t)

    ex_s = CoefficientFunction((ex_s0, ex_s1,
                                ex_s2, ex_s3), dims=(2, 2))

    ex_r = 0.5 * (pi * sin(pi * x) * cos(pi * y) -
                  (1 - 2 * x) * y * (1 - y)) * sin(t)

    ex_u1 = CoefficientFunction(sin(pi * x) * sin(pi * y) * sin(t))
    ex_u2 = CoefficientFunction(x * (1 - x) * y * (1 - y) * sin(t))

    ex_u = CoefficientFunction((ex_u1, ex_u2))

    norm_s = ng.sqrt(ng.Integrate(InnerProduct(u.components[0] - ex_s,
                                               u.components[0] - ex_s), mesh))
    norm_v = ng.sqrt(ng.Integrate((u.components[1] - ex_v) *
                                  (u.components[1] - ex_v), mesh))
    norm_u = ng.sqrt(ng.Integrate((u_dis - ex_u) * (u_dis - ex_u), mesh))
    norm_r = ng.sqrt(ng.Integrate((u.components[2] - ex_r) *
                                  (u.components[2] - ex_r), mesh))
    return (norm_s, norm_v, norm_u, norm_r)


def hconvergencetable(e_1, e_2, e_3, e_4, maxr):
    print("===========================================" +
          "==================================")
    print(" Mesh  Errors_s   Order    Error_v    Order ",
          "Error_u   Order  Error_r   Order")
    print("------------------------------------------" +
          "----------------------------------")
    rate1 = []
    rate2 = []
    rate3 = []
    rate4 = []
    for i in range(maxr):
        rate1.append('  *  ')
        rate2.append('  *  ')
        rate3.append('  *  ')
        rate4.append('  *  ')
    for i in range(1, maxr):
        if abs(e_1[i]) > 1.e-15 and abs(e_2[i]) > 1.e-15:
            rate1[i] = format(ng.log(e_1[i - 1] / e_1[i]) / ng.log(2), '+5.2f')
            rate2[i] = format(ng.log(e_2[i - 1] / e_2[i]) / ng.log(2), '+5.2f')
            rate3[i] = format(ng.log(e_3[i - 1] / e_3[i]) / ng.log(2), '+5.2f')
            rate4[i] = format(ng.log(e_4[i - 1] / e_4[i]) / ng.log(2), '+5.2f')
    for i in range(maxr):
        print("h/ % -4d %8.2e   %s   %8.2e   %s  %8.2e  %s  %8.2e  %s" %
              (pow(2, i + 1), e_1[i], rate1[i], e_2[i], rate2[i],
               e_3[i], rate3[i], e_4[i], rate4[i]))

    print("======================================" +
          "=======================================")


def collecterrors(k, maxr, lam, tend):
    l2e_s = []
    l2e_v = []
    l2e_u = []
    l2e_r = []

    for l in range(0, maxr):
        hl = 2 ** (-l) / 2
        er_1, er_2, er_3, er_4 = weaksym_dynamic(k, hl, lam, tend)
        l2e_s.append(er_1)
        l2e_v.append(er_2)
        l2e_u.append(er_3)
        l2e_r.append(er_4)
    return l2e_s, l2e_v, l2e_u, l2e_r


# ============= MAIN DRIVER ==============================

maxlevels = 5
tend = 1
l = 1.0
p = 3
er_s, er_v, er_u, er_r = collecterrors(p, maxlevels, l, tend)
hconvergencetable(er_s, er_v, er_u, er_r, maxlevels)

#============================================================================
#Mesh  Errors_s   Order   Error_v    Order  Error_u   Order  Error_r   Order
#----------------------------------------------------------------------------
#h/2   1.04e-02     *    4.24e-03     *    5.12e-03    *    5.08e-03    *  
#h/4   8.46e-04   +3.62  1.05e-03   +2.01  1.27e-03  +2.01  4.38e-04  +3.54
#h/8   9.72e-05   +3.12  1.19e-04   +3.14  1.36e-04  +3.23  4.80e-05  +3.19
#h/16  1.23e-05   +2.98  1.36e-05   +3.13  1.49e-05  +3.19  5.56e-06  +3.11
#h/32  1.56e-06   +2.98  1.66e-06   +3.04  1.81e-06  +3.04  6.94e-07  +3.00
#============================================================================
