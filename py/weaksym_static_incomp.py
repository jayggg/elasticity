"""
Solve a static linear elasticity problem with the weakly
symmetric mixed method using the GG stress element in the
incompressible material case where Poisson ratio is 0.5.

(A sig, tau) + (u, div tau) + (skew(l), tau) + (cc,tr(tau)) = 0
(div sigma, v)                                = (f,w)
(sigma, skew(q))                              = 0.0
(tr(sig), cx)                                 = 0.0,

where A is the inverse of the elasticity tensor C
(in the equation sigma = C e(u)) given by
C e(u) = lambda* tr(e(u)) I + 2*mu * e(u).

When Poisson’s ratio    nu = 1/2,
lam = 2*mu*nu/(1 -2*nu ) is infinity.
We perform a convergence study with this exact solution:

u(x) = [  x^2*(1-x)^2*y*(1-y)*(1-2y) ]
       [ -y^2*(1-y)^2*x*(1-x)*(1-2x) ].

"""

from ngsolve import CoefficientFunction, SymbolicBFI, SymbolicLFI, log
from ngsolve import InnerProduct, div, x, y
import ngsolve as ng
from netgen.geom2d import unit_square
from ctypes import CDLL

mylngs = CDLL("../libelas.so")
ng.ngsglobals.msg_level = 1
ng.SetHeapSize(int(1e8))

# Parameters:
mu = 0.5
nu = 0.5  # Poisson ratio

# Making an exact solution:

def exact(mu):
    # Exact displacement
    u_1 = x * x * (1 - x) * (1 - x) * y * (1 - y) * (1 - 2 * y)
    u_2 = -y * y * (1 - y) * (1 - y) * x * (1 - x) * (1 - 2 * x)

    ex_u = CoefficientFunction((u_1, u_2))

    # Derivatives to compute sigma, skew(q) and f.
    dxu_1 = 2 * x * (1 - x) * (1 - 2 * x) * y * (1 - y) * (1 - 2 * y)
    dyu_1 = x * x * (1 - x) * (1 - x) * (6 * y * y - 6 * y + 1)

    dxxu_1 = 2 * (6 * x * x - 6 * x + 1) * y * (1 - y) * (1 - 2 * y)
    dyxu_1 = 2 * x * (1 - x) * (1 - 2 * x) * (6 * y * y - 6 * y + 1)
    dyyu_1 = x * x * (1 - x) * (1 - x) * (12 * y - 6)

    dxu_2 = -y * y * (1 - y) * (1 - y) * (6 * x * x - 6 * x + 1)
    dyu_2 = -2 * y * (1 - y) * (1 - 2 * y) * x * (1 - x) * (1 - 2 * x)

    dxxu_2 = -y * y * (1 - y) * (1 - y) * (12 * x - 6)
    dyyu_2 = -2 * (6 * y * y - 6 * y + 1) * x * (1 - x) * (1 - 2 * x)
    dyxu_2 = -2 * y * (1 - y) * (1 - 2 * y) * (6 * x * x - 6 * x + 1)

    # skew(q) = (0,-q3,q2,q3,0,-q1,q2,q1,0)
    q = 0.5 * (dyu_1 - dxu_2)
    ex_q = CoefficientFunction(q)

    # Exact right hand side f
    F0 = 2 * mu * dxxu_1 + mu * (dyyu_1 + dyxu_2)
    F1 = mu * (dyxu_1 + dxxu_2) + 2 * mu * dyyu_2

    F = CoefficientFunction((F0, F1))

    # e(u)
    ee = (dxu_1, 0.5 * (dyu_1 + dxu_2),
          0.5 * (dyu_1 + dxu_2), dyu_2)

    #   C e(u) = lambda* tr(e(u)) I + 2*mu * e(u)
    def C(ee):
        C0 = 2 * mu * ee[0]
        C3 = 2 * mu * ee[3]
        C1 = 2 * mu * ee[1]
        C2 = 2 * mu * ee[2]
        return CoefficientFunction((C0, C1, C2, C3), dims=(2, 2))

    ex_s = C(ee)
    return ex_s, ex_u, ex_q, F


def weaksym_incomp(k, h):

    mesh = ng.Mesh(unit_square.GenerateMesh(maxh=h))
    # Stress space S:
    S = ng.FESpace("ggfes", mesh, order=k)
    # Displacement space U is two copies of
    U = ng.VectorL2(mesh, order=k - 1)
    # Antisymmetric matrix space for rotations A:
    A = ng.L2(mesh, order=k)
    C = ng.FESpace("number", mesh)

    X = ng.FESpace([S, U, A, C])

    sig, u, r, cc = X.TrialFunction()
    tau, w, q, cx = X.TestFunction()

    divtau = tau.Deriv()
    divsig = sig.Deriv()

    a1 = 0.5 / mu                # Compliance tensor for isotropic mat
    a2 = 0.5 / (mu * 2)
    trace = sig[0,0] + sig[1,1]

    Asig =  CoefficientFunction((a1 * sig[0] - a2 * trace,
                                 a1 * sig[1],
                                 a1 * sig[2],
                                 a1 * sig[3] - a2 * trace), dims=(2, 2))

    ex_s, ex_u, ex_q, F = exact(mu)

    a = ng.BilinearForm(X, symmetric=True)
    a += SymbolicBFI(InnerProduct(Asig, tau))
    a += SymbolicBFI(u * div(tau))
    a += SymbolicBFI(u * div(tau))
    a += SymbolicBFI(r * (tau[0, 1] -  tau[0, 1]))
    a += SymbolicBFI(cc * (tau[0, 0] + tau[1, 1]))

    a += SymbolicBFI(div(sig) * w)
    a += SymbolicBFI((sig[0, 1] -sig[1, 0]) * q)
    a += SymbolicBFI(cx * (sig[0, 0] + sig[1, 1]))

    f = ng.LinearForm(X)
    f += SymbolicLFI(F * w)

    c = ng.Preconditioner(a, type="direct")
    ng.SetHeapSize(int(1e8))
    a.Assemble()
    f.Assemble()

    u = ng.GridFunction(X)
    bvp = ng.BVP(bf=a, lf=f, gf=u, pre=c, maxsteps=10).Do()

    norm_s = ng.sqrt(ng.Integrate(InnerProduct(u.components[0] - ex_s,
                                               u.components[0] - ex_s), mesh))

    norm_u = ng.sqrt(ng.Integrate((u.components[1] - ex_u) *
                                  (u.components[1] - ex_u), mesh))

    norm_r = ng.sqrt(ng.Integrate((u.components[2] - ex_q) *
                                  (u.components[2] - ex_q), mesh))

    return norm_s, norm_u, norm_r


def hconvergencetable(e_1, e_2, e_3, maxr):
    print("============================================================")
    print(" Mesh   Errors_s  Order    Error_u   Order   Error_r   Order")
    print("------------------------------------------------------------")
    rate1 = []
    rate2 = []
    rate3 = []
    for i in range(maxr):
        rate1.append('  *  ')
        rate2.append('  *  ')
        rate3.append('  *  ')
    for i in range(1, maxr):
        if abs(e_1[i]) > 1.e-15 and abs(e_2[i]) > 1.e-15:
            rate1[i] = format(log(e_1[i - 1] / e_1[i]) / log(2), '+5.2f')
            rate2[i] = format(log(e_2[i - 1] / e_2[i]) / log(2), '+5.2f')
            rate3[i] = format(log(e_3[i - 1] / e_3[i]) / log(2), '+5.2f')
    for i in range(maxr):
        print(" h/%-4d %8.2e   %s   %8.2e   %s  %8.2e  %s" %
              (pow(2, i + 1), e_1[i], rate1[i],
               e_2[i], rate2[i], e_3[i], rate3[i]))
    print("========================================================")


def collecterrors(k, maxr):
    l2e_s = []
    l2e_u = []
    l2e_r = []
    for l in range(0, maxr):
        hl = 2**(-l) / 2
        er_1, er_2, er_3 = weaksym_incomp(k, hl)
        l2e_s.append(er_1)
        l2e_u.append(er_2)
        l2e_r.append(er_3)
    print("============================================================")
    print("  Error : k =", k)
    return l2e_s, l2e_u, l2e_r


maxlevels = 3

er_s, er_u, er_r = collecterrors(4, maxlevels)
hconvergencetable(er_s, er_u, er_r, maxlevels)

# ============================================================
#  Error : k = 1
# ============================================================
# Mesh   Errors_s  Order  Error_u     Order   Error_r   Order
# ------------------------------------------------------------
# h/2    1.70e-02     *     3.37e-03     *    1.19e-02    *
# h/4    3.45e-03   +2.30   1.42e-03   +1.24  2.16e-03  +2.46
# h/8    7.84e-04   +2.14   7.31e-04   +0.96  4.92e-04  +2.13
# ============================================================

# ============================================================
#  Error : k = 2
# ============================================================
# Mesh   Errors_s  Order  Error_u     Order   Error_r   Order
# ------------------------------------------------------------
# h/2    2.86e-03     *     1.45e-03     *    1.63e-03    *
# h/4    4.71e-04   +2.60   3.72e-04   +1.96  3.43e-04  +2.25
# h/8    5.45e-05   +3.11   8.75e-05   +2.09  4.46e-05  +2.94
# ============================================================

# ============================================================
#  Error : k = 3
# ============================================================
# Mesh   Errors_s  Order  Error_u     Order   Error_r   Order
# ------------------------------------------------------------
# h/2    1.55e-03     *     1.11e-03     *    7.94e-04    *
# h/4    4.73e-05   +5.04   8.25e-05   +3.75  2.30e-05  +5.11
# h/8    2.32e-06   +4.35   8.03e-06   +3.36  1.33e-06  +4.11
# ============================================================

# ============================================================
#   Error : k = 4
# ============================================================
# Mesh   Errors_s  Order  Error_u     Order   Error_r   Order
# ------------------------------------------------------------
# h/2    1.23e-04     *     8.41e-05     *    5.62e-05    *
# h/4    3.79e-06   +5.03   3.66e-06   +4.52  2.17e-06  +4.70
# h/8    9.18e-08   +5.37   2.09e-07   +4.13  5.68e-08  +5.25
# ============================================================
