"""
Equations of dynamic linear elasticity for stress sigma, velocity v,
and rotations r in 3D:

  (sigma', tau) + (v, div tau) + (Skw(r'), tau) = 0
  (div sigma, w) - (v', w)                      = (-F,w)
  (sigma', Skw(q))	          	        = 0

Here  Skw(m) = [ 0 -m3 m2]  and prime denotes time derivative.
               [ m3 0 -m1]
               [-m2 m1  0]

We perform spatial discretization with GG elements, implicit time
stepping  using Crank-Nicolson and measure convergence rates
when the exact solution is

 u(x,y,t) = [sin(pi * x) * sin(pi * y) * sin(pi * z) * sin(t)]
            [x * (1 - x) * y * (1 - y) * z * (1 - z) * sin(t)]
            [0.0]

Note that with this displacement, the intial velocity is

 v(x,y,0) = [ sin(pi*x)*sin(pi*x)*sin(pi*z) ]
            [ x(1-x)y(1-y)z(1-z)            ],
            [       0                       ]

which forms the data for the simulation.

u(x,y,t) is recovered by integrating in time using trapezoidal rule

"""


import ngsolve as ng
from ngsolve import sin, cos, sqrt, x, y, z
from ngsolve import InnerProduct, div
from netgen.csg import unit_cube
from numpy import pi

from ctypes import CDLL

mylngs = CDLL("../libelas.so")
ng.ngsglobals.msg_level = 1

# ===== INITIAL DATA ====


def initdata():

    """ Return two vectors, representing the initial values of
    div(sig(0)) and velocity v(0). (The remainder of the required
    initial data will be computed automatically.
    """

    divsig0 = ng.CoefficientFunction((0, 0, 0))
    v0 = ng.CoefficientFunction((sin(pi * x) * sin(pi * y) * sin(pi * z),
                                 x * (1 - x) * y * (1 - y) * z * (1 - z),
                                 0.0))

    return divsig0, v0

# ============ SOURCE  DATA ======================

def Skw(r):
    return ng.CoefficientFunction((0, -r[2], r[1],
                                   r[2], 0, -r[0],
                                   -r[1], r[0], 0), dims=(3, 3))

def F(t):  # v_t -div(sig)
    F0 = -sin(pi * x) * sin(pi * y) * sin(pi * z) * sin(t)
    F0 += -sin(t) * (-2 * pi * pi * sin(pi * x) * sin(pi * y) * sin(pi * z) +
                     0.5 * (1 - 2 * x) * (1 - 2 * y) * z * (1 - z))
    F1 = -x * (1 - x) * y * (1 - y) * z * (1 - z) * sin(t)
    F1 += -sin(t) * (-y * (1 - y) * z * (1 - z) +
                     -2 * x * (1 - x) * z * (1 - z) +
                     -x * (1 - x) * y * (1 - y) +
                     0.5 * pi * pi * cos(pi * x) * cos(pi * y) * sin(pi * z))

    F2 = -sin(t) * (0.5 * pi * pi * cos(pi * x) * sin(pi * y) * cos(pi * z) +
                    0.5 * (1 - 2 * z) * (1 - 2 * y) * x * (1 - x))
    return ng.CoefficientFunction((F0, F1, F2))

# ============ SOLVE ===========


def weaksym_dynamic3d(k, h, l, tend):

    """ PARAMETERS:

    k: determines polynomial order of stress (in P_k + bubbles),
       velocity (in P_{k-1}) and rotations (in P_k).

    h: maximal mesh size (of the mesh of unit square)

    l: sets timestep by  ∆t = h * l

    tend: final simulation time where error is measured.

    """

    mesh = ng.Mesh(unit_cube.GenerateMesh(maxh=h))
    delta = l * h   # time step size ∆t

    S = ng.FESpace("ggfes3d", mesh, order=k)  # Stress space
    U = ng.VectorL2(mesh, order=k - 1)        # Displacement space = L2 x L2 x L2
    A = ng.VectorL2(mesh, order=k)            # Space of rotations (skw sym mat)
    XY = ng.FESpace([S, U, A])

    # Setting initial sig, r values by solving a static problem:
    divsig0, v0 = initdata()

    sig, v, r = XY.TrialFunction()
    tau, w, q = XY.TestFunction()

    a = ng.BilinearForm(XY, symmetric=True)
    a += ng.SymbolicBFI(InnerProduct(sig, tau))       # (sig, tau)
    a += ng.SymbolicBFI(v * div(tau))
    a += ng.SymbolicBFI(InnerProduct(Skw(r), tau))
    a += ng.SymbolicBFI(div(sig) * w)
    a += ng.SymbolicBFI(InnerProduct(Skw(q), sig))

    fin = ng.LinearForm(XY)
    fin += ng.SymbolicLFI(divsig0 * w)

    a.Assemble(int(1e9))
    fin.Assemble(int(1e9))

    initial = ng.GridFunction(XY)
    initial.vec.data = a.mat.Inverse(XY.FreeDofs()) * fin.vec

    # u stores initial data and will contain future time iterates
    u = ng.GridFunction(XY)
    u.components[0].Set(initial.components[0])  # Set sig(0)
    u.components[2].Set(ng.CoefficientFunction((initial.components[2][0],
                                                initial.components[2][0],
                                                initial.components[2][0])))
    # Set initial velocity by projecting v0 into fe space
    u.components[1].Set(v0)

    ng.Draw(u.components[1][1], mesh, "velocity", sd=2)

    """ Crank-Nicolson in matrix form reads as

     M * u(t+∆t) = S * u(t) - FF(t)

          [    A    (∆t/2)B^T     D^T  ]
      M = [  (∆t/2)B    -C        0    ]
          [    D         0        0    ]

          [    A    -(∆t/2)B^T    D^T  ]
      S = [ -(∆t/2)B    - C      0     ]
          [    D         0       0     ]

      FF(t) = <(∆t/2)(F0(t)+F0(t+∆t)), v_1>
           + <(∆t/2)(F1(t)+F1(t+∆t)), v_2>
    """

    # Make the M matrix
    m = ng.BilinearForm(XY, symmetric=True)
    m += ng.SymbolicBFI(InnerProduct(sig, tau))                        # A
    m += ng.SymbolicBFI(0.5 * delta * v * div(tau))  # (∆t/2)B^T
    m += ng.SymbolicBFI(InnerProduct(Skw(r), tau))  # D^T
    m += ng.SymbolicBFI(0.5 * delta * w * div(sig))  # (∆t/2)B^T
    m += ng.SymbolicBFI(-v * w)       # -C
    m += ng.SymbolicBFI(InnerProduct(Skw(q), sig))   # D
    m.Assemble()

    inv_M = m.mat.Inverse(XY.FreeDofs())

    # Make the S matrix
    s = ng.BilinearForm(XY, nonassemble=True, symmetric=True)
    s += ng.SymbolicBFI(InnerProduct(sig, tau))                        # A
    s += ng.SymbolicBFI(-0.5 * delta * v * div(tau))   # -(∆t/2)B^T
    s += ng.SymbolicBFI(InnerProduct(Skw(r), tau))   # D^T
    s += ng.SymbolicBFI(-v * w)     # -C
    s += ng.SymbolicBFI(-0.5 * delta * w * div(sig))  # -(∆t/2)B^T
    s += ng.SymbolicBFI(InnerProduct(Skw(q), sig))    # D

    # Displacement Space:
    # for the trapezoidal rule

    u_dis = ng.GridFunction(U)

    u_dis.vec.data += initial.components[1].vec.data

    # Vector to store time dependent rhs  S * u + F in Crank Nicolson.
    f = ng.LinearForm(XY)
    f.Assemble()
    ff = f.vec
    t = 0

    with ng.TaskManager():
        while t < tend:

            s.Apply(u.vec, ff)   # write S*u into ff

            # assemble time dependent source using F(t)
            li = ng.LinearForm(XY)
            Ft = F(t)
            Ftt = F(t + delta)
            li += ng.SymbolicLFI(-0.5 * delta * (Ft * w + Ftt * w))
            li.Assemble()

            ff.data += li.vec    # assembled  S * u(t) - FF(t)

            u_dis.vec.data += 0.5 * delta* u.components[1].vec.data

            u.vec.data = inv_M * ff  # u(t+∆t) = M^{-1} * (S*u(t) - FF(t))

            u_dis.vec.data += 0.5 * delta * u.components[1].vec.data

            t += delta
            print('t=%g' % t)
            ng.Redraw(blocking=True)

    # Compare with exact solution
    ex_v1 = ng.CoefficientFunction(sin(pi * x) * sin(pi * y) *
                                   sin(pi * z) * cos(t))
    ex_v2 = ng.CoefficientFunction(x * (1 - x) * y * (1 - y) *
                                   z * (1 - z) * cos(t))
    ex_v3 = ng.CoefficientFunction(0)


    ex_v = ng.CoefficientFunction((ex_v1, ex_v2, ex_v3))

    ex_s0 = (pi * cos(pi * x) * sin(pi * y) * sin(pi * z)) * sin(t)
    ex_s1 = (0.5 * (pi * sin(pi * x) * cos(pi * y) * sin(pi * z) +
                    (1 - 2 * x) * y * (1 - y) * z * (1 - z))) * sin(t)
    ex_s2 = (0.5 * pi * sin(pi * x) * sin(pi * y) * cos(pi * z)) * sin(t)
    ex_s3 = ex_s1
    ex_s4 = (1 - 2 * y) * x * (1 - x) * z * (1 - z) * sin(t)
    ex_s5 = (0.5 * (1 - 2 * z) * x * (1 - x) * y * (1 - y)) * sin(t)
    ex_s6 = ex_s2
    ex_s7 = ex_s5
    ex_s8 = 0

    ex_s = ng.CoefficientFunction((ex_s0, ex_s1, ex_s2,
                                   ex_s3, ex_s4, ex_s5,
                                   ex_s6, ex_s7, ex_s8), dims=(3, 3))

    ex_q1 = -0.5 * (1 - 2 * z) * x * (1 - x) * y * (1 - y) * sin(t)
    ex_q2 = 0.5 * pi * sin(pi * x) * sin(pi * y) * cos(pi * z) * sin(t)
    ex_q3 = -0.5 * (pi * sin(pi * x) * cos(pi * y) * sin(pi * z) +
                    -(1 - 2 * x) * y * (1 - y) * z * (1 - z)) * sin(t)

    ex_q = ng.CoefficientFunction((ex_q1, ex_q2, ex_q3))
    ex_u1 = ng.CoefficientFunction(sin(pi * x) * sin(pi * y) *
                                   sin(pi * z) * sin(t))
    ex_u2 = ng.CoefficientFunction(x * (1 - x) * y * (1 - y) *
                                   z * (1 - z) * sin(t))
    ex_u3 = ng.CoefficientFunction((0.0))

    ex_u = ng.CoefficientFunction((ex_u1, ex_u2, ex_u3))

    norm_v = sqrt(ng.Integrate((u.components[1] - ex_v) *
                               (u.components[1] - ex_v), mesh))

    norm_s = sqrt(ng.Integrate(InnerProduct(u.components[0] - ex_s,
                                            u.components[0] - ex_s), mesh))

    norm_u = sqrt(ng.Integrate((u_dis - ex_u) *
                               (u_dis - ex_u), mesh))

    norm_r = sqrt(ng.Integrate((u.components[2] - ex_q) *
                               (u.components[2] - ex_q), mesh))

    return (norm_s, norm_v, norm_u, norm_r)


def hconvergencetable(e_1, e_2, e_3, e_4, maxr):
    print("===========================================" +
          "==================================")
    print(" Mesh  Errors_s   Order    Error_v    Order ",
          "Error_u   Order  Error_r   Order")
    print("------------------------------------------" +
          "----------------------------------")
    rate1 = []
    rate2 = []
    rate3 = []
    rate4 = []
    for i in range(maxr):
        rate1.append('  *  ')
        rate2.append('  *  ')
        rate3.append('  *  ')
        rate4.append('  *  ')
    for i in range(1, maxr):
        if abs(e_1[i]) > 1.e-15 and abs(e_2[i]) > 1.e-15:
            rate1[i] = format(ng.log(e_1[i - 1] / e_1[i]) / ng.log(2), '+5.2f')
            rate2[i] = format(ng.log(e_2[i - 1] / e_2[i]) / ng.log(2), '+5.2f')
            rate3[i] = format(ng.log(e_3[i - 1] / e_3[i]) / ng.log(2), '+5.2f')
            rate4[i] = format(ng.log(e_4[i - 1] / e_4[i]) / ng.log(2), '+5.2f')
    for i in range(maxr):
        print("h/ % -4d %8.2e   %s   %8.2e   %s  %8.2e  %s  %8.2e  %s" %
              (pow(2, i + 1), e_1[i], rate1[i], e_2[i], rate2[i],
               e_3[i], rate3[i], e_4[i], rate4[i]))

    print("======================================" +
          "=======================================")


def collecterrors(k, maxr, lam, tend):
    l2e_s = []
    l2e_v = []
    l2e_u = []
    l2e_r = []

    for l in range(0, maxr):
        hl = 2 ** (-l) / 2
        er_1, er_2, er_3, er_4 = weaksym_dynamic3d(k, hl, lam, tend)
        l2e_s.append(er_1)
        l2e_v.append(er_2)
        l2e_u.append(er_3)
        l2e_r.append(er_4)
    return l2e_s, l2e_v, l2e_u, l2e_r

# ================= MAIN DRIVER ===============================


maxlevels = 3
tend = 1
l = 1.0
p = 1
er_s, er_v, er_u, er_r = collecterrors(p, maxlevels, l, tend)
hconvergencetable(er_s, er_v, er_u, er_r, maxlevels)
