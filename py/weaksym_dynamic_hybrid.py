"""
 Solve a hybrid linear elasticity problem with the weakly
 symmetric mixed method in 2D using the GG stress element

  (dt sig, tau) + (u, div tau) + (skew(dt r), tau) + <l,tau.n> = 0
  (div sig, w) - (dt v, w)                                    = -(f,w)
  (dt sig, skew(q))			         	      = 0
  <sig.n, m>                                                  = 0

 time step using Crank Nicolson and perform a convergence study
 with this exact solution:

 u(x) = [ sin(pi*x)*sin(pi*y)*sin(t) ]
        [ x*(1-x)* y*(1-y)*sin(t)    ]

 """
import ngsolve as ng
from ngsolve import sqrt, FESpace, L2, CoefficientFunction, sin, cos
from ngsolve import BilinearForm, SymbolicBFI, GridFunction
from ngsolve import InnerProduct, div, x, y
from netgen.geom2d import unit_square
from numpy import pi
from ctypes import CDLL

mylngs = CDLL("../libelas.so")
ng.ngsglobals.msg_level = 1

# ===========  INITIAL DATA =================================


def initdata():

    """ Return two vectors, representing the initial values of
    div(sigma(0)) and velocity v(0). (The remainder of the required
    initial data will be computed automatically.
    """

    divsigma0 = CoefficientFunction((0, 0))
    v0 = CoefficientFunction((sin(pi * x) * sin(pi * y),
                              x * (1 - x) * y * (1 - y)))

    return divsigma0, v0

# ============ SOURCE  DATA ==============================


def F(t):
    F0 = -sin(pi * x) * sin(pi * y) * sin(t) + \
         -sin(t) * (-1.5 * pi * pi * sin(pi * x) * sin(pi * y) +
                    0.5 * (1 - 2 * x) * (1 - 2 * y))

    F1 = -x * (1 - x) * y * (1 - y) * sin(t) + \
         -sin(t) * (-y * (1 - y) - 2 * x * (1 - x) +
                    0.5 * pi * pi * cos(pi * x) * cos(pi * y))

    return CoefficientFunction((F0, F1))

# =============== SOLVE ===================================


def weaksym_dynamic_hybrid(k, h, l, tend):

    """ PARAMETERS:

   k: determines polynomial order of stress (in P_k + bubbles),
      velocity (in P_{k-1}) and rotations (in P_k).

   h: maximal mesh size (of the mesh of unit square)

   l: sets timestep by  ∆t = h * l

    tend: final simulation time where error is measured.

   """

    mesh = ng.Mesh(unit_square.GenerateMesh(maxh=h))
    delta = l * h   # time step size ∆t

    # Stress space S:
    S = FESpace("ggfes", mesh, order=k, flags={'discontinuous': True})
    # Velocity space U is two copies of
    U = ng.VectorL2(mesh, order=k - 1)
    # Antisymmetric matrix space for rotations A:
    A = L2(mesh, order=k)
    # Lagrange multiplier space
    L = ng.FacetFESpace(mesh, order=k, dirichlet="bottom|right|left|top")

    # Full product FE space
    XY = FESpace([S, U, A, L, L])
    
    sig, v, r, l1, l2 = XY.TrialFunction()
    tau, w, q, m1, m2 = XY.TestFunction()
   
    n = ng.specialcf.normal(mesh.dim)
    l = ng.CoefficientFunction((l1, l2))
    m = ng.CoefficientFunction((m1, m2))

    # Setting initial sigma, r values by solving a static problem:
    divsigma0, v0 = initdata()

    a = BilinearForm(XY, symmetric=True, eliminate_internal=True)
    a += SymbolicBFI(InnerProduct(sig, tau))         # (sig, tau)
    a += SymbolicBFI(v * div(tau))                   # (u, div tau)
    a += SymbolicBFI(r * (tau[0, 1] - tau[1, 0]))     # (r, skw tau)
    a += SymbolicBFI(div(sig) * w)                   # (div sig, v)
    a += SymbolicBFI((sig[0,1] - sig[1,0]) * q)       # (q, skw sig)
    a += SymbolicBFI(l * (tau * n), element_boundary=True)
    a += SymbolicBFI(m * (sig * n), element_boundary=True)

    fin = ng.LinearForm(XY)
    fin += ng.SymbolicLFI(divsigma0 * w)
    a.Assemble()
    fin.Assemble()

    initial = GridFunction(XY)

    c = ng.Preconditioner(a, type="direct")
    a.Assemble()
    ng.BVP(bf=a, lf=fin, gf=initial, pre=c, maxsteps=10).Do()

    # u stores initial data and will contain future time iterates
    u = GridFunction(XY)
    u.components[0].Set(initial.components[0])  # Set sigma(0)
    u.components[2].Set(initial.components[2])  # Set r(0)

    # Set initial velocity by projecting v0 into fe space
    u.components[1].Set(v0)

    ng.Draw(u.components[1][1], mesh, "velocity", sd=2)

    """ Crank-Nicolson in matrix form reads as

           M * u(t+∆t) = S * u(t) + F(t)
     where

          [    A    (∆t/2)B^T     D^T  (∆t/2) E^T  ]
      M = [ (∆t/2)B     -C        0       0        ]
          [    D         0        0       0        ]
          [ (∆t/2)E      0        0       0        ]

          [    A    -(∆t/2)B^T   D^T  - (∆t/2) E^T ]
      S = [ -(∆t/2)B    -C       0        0        ]
          [    D         0       0        0        ]
          [ -(∆t/2)E     0       0        0        ]

      F(t) = ( -(∆t/2) (F(t)+F(t+∆t)), v )
    """
    # Make the M matrix
    mm = BilinearForm(XY, symmetric=True, eliminate_internal=True)
    mm += SymbolicBFI(InnerProduct(sig, tau))            # A
    mm += SymbolicBFI(0.5 * delta * v * div(tau))        # (∆t/2)B^T
    mm += SymbolicBFI(r * (tau[0, 1] - tau[1, 0]))       # D^T
    mm += SymbolicBFI(0.5 * delta * w * div(sig))        # (∆t/2)B
    mm += SymbolicBFI(-v * w)                            # -C
    mm += SymbolicBFI((sig[0, 1] - sig[1,0]) * q)        # D
    mm += SymbolicBFI(0.5 * delta *
                     l * (tau * n),  element_boundary=True)  # (∆t/2)E^T
    mm += SymbolicBFI(0.5 * delta * m * (sig * n), element_boundary=True)  # (∆t/2)E^T

    cc = ng.Preconditioner(mm, type="direct")
    mm.Assemble()

    # Make the S matrix
    s = BilinearForm(XY, nonassemble = True)
    s += SymbolicBFI(InnerProduct(sig, tau))              # A
    s += SymbolicBFI(-0.5 * delta * v * div(tau))       # -(∆t/2)B^T
    s += SymbolicBFI(r * (tau[0, 1] - tau[1, 0]))             # D^T
    s += SymbolicBFI(-v * w)                  # C
    s += SymbolicBFI(-0.5 * delta * w * div(sig))       # (∆t/2)B^T
    s += SymbolicBFI((sig[0, 1] - sig[1, 0]) * q)             # D
    s += SymbolicBFI(-0.5 * delta *
                     l * (tau * n), element_boundary=True)   # -(∆t/2)E^T
    s += SymbolicBFI(-0.5 * delta *
                     m * (sig * n), element_boundary=True)  # (∆t/2)E^T

    # Integrating velocity to get  displacement:
    u_dis = GridFunction(U)
    u_dis.vec.data += initial.components[1].vec.data
    # Vector to store time dependent rhs  S * u + F in Crank Nicolson.
    f = ng.LinearForm(XY)
    f.Assemble()
    t = 0

    with ng.TaskManager():
        while t < tend:

            s.Apply(u.vec, f.vec)   # write S*u into ff

            # assemble time dependent source using F(t)
            li = ng.LinearForm(XY)
            Ft = F(t)
            Ftt = F(t + delta)
            li += ng.SymbolicLFI(-0.5 * delta * (Ft * w + Ftt * w))
            li.Assemble()

            f.vec.data += li.vec.data    # assembled  S * u(t) + F(t)

            # Use half of trapezoidal rule to get displacement
            u_dis.vec.data += 0.5 * delta * u.components[1].vec.data

            # Solving U_{N+1}
            ng.BVP(bf=mm, lf=f, gf=u, pre=cc, maxsteps=100).Do()

            # Add the remaining half of the trapezoidal rule at new time
            u_dis.vec.data += 0.5 * delta * u.components[1].vec.data
           
            t += delta
            print('t=%g' % t)
            ng.Redraw(blocking=True)

    # Compare with exact solution
    ex_v1 = CoefficientFunction(sin(pi * x) * sin(pi * y) * cos(t))
    ex_v2 = CoefficientFunction(x * (1 - x) * y * (1 - y) * cos(t))

    ex_v = CoefficientFunction((ex_v1, ex_v2))

    ex_s0 = pi * cos(pi * x) * sin(pi * y) * sin(t)
    ex_s1 = 0.5 * (pi * sin(pi * x) * cos(pi * y) +
                   (1 - 2 * x) * y * (1 - y)) * sin(t)
    ex_s2 = 0.5 * (pi * sin(pi * x) * cos(pi * y) +
                   (1 - 2 * x) * y * (1 - y)) * sin(t)
    ex_s3 = (1 - 2 * y) * x * (1 - x) * sin(t)

    ex_s = CoefficientFunction((ex_s0, ex_s1,
                                ex_s2, ex_s3))

    ex_r = 0.5 * (pi * sin(pi * x) * cos(pi * y) -
                  (1 - 2 * x) * y * (1 - y)) * sin(t)

    ex_u1 = CoefficientFunction(sin(pi * x) * sin(pi * y) * sin(t))
    ex_u2 = CoefficientFunction(x * (1 - x) * y * (1 - y) * sin(t))

    ex_u = CoefficientFunction((ex_u1, ex_u2))
    norm_v = sqrt(ng.Integrate((u.components[1] - ex_v) *
                               (u.components[1] - ex_v), mesh))
    
    norm_s = sqrt(ng.Integrate(InnerProduct(u.components[0] - ex_s,
                                            u.components[0] - ex_s), mesh))
    norm_u = sqrt(ng.Integrate((u_dis - ex_u) *
                               (u_dis - ex_u), mesh))

    norm_r = sqrt(ng.Integrate((u.components[2] - ex_r) *
                               (u.components[2] - ex_r), mesh))

    return (norm_s, norm_v, norm_u, norm_r)


def hconvergencetable(e_1, e_2, e_3, e_4, maxr):
    print("===========================================" +
          "==================================")
    print(" Mesh  Errors_s   Order    Error_v    Order ",
          "Error_u   Order  Error_r   Order")
    print("------------------------------------------" +
          "----------------------------------")
    rate1 = []
    rate2 = []
    rate3 = []
    rate4 = []
    for i in range(maxr):
        rate1.append('  *  ')
        rate2.append('  *  ')
        rate3.append('  *  ')
        rate4.append('  *  ')
    for i in range(1, maxr):
        if abs(e_1[i]) > 1.e-15 and abs(e_2[i]) > 1.e-15:
            rate1[i] = format(ng.log(e_1[i - 1] / e_1[i]) / ng.log(2), '+5.2f')
            rate2[i] = format(ng.log(e_2[i - 1] / e_2[i]) / ng.log(2), '+5.2f')
            rate3[i] = format(ng.log(e_3[i - 1] / e_3[i]) / ng.log(2), '+5.2f')
            rate4[i] = format(ng.log(e_4[i - 1] / e_4[i]) / ng.log(2), '+5.2f')
    for i in range(maxr):
        print("h/ % -4d %8.2e   %s   %8.2e   %s  %8.2e  %s  %8.2e  %s" %
              (pow(2, i + 1), e_1[i], rate1[i], e_2[i], rate2[i],
               e_3[i], rate3[i], e_4[i], rate4[i]))

    print("======================================" +
          "=======================================")


def collecterrors(k, maxr, lam, tend):
    l2e_s = []
    l2e_v = []
    l2e_u = []
    l2e_r = []

    for l in range(0, maxr):
        hl = 2 ** (-l) / 2
        er_1, er_2, er_3, er_4 = weaksym_dynamic_hybrid(k, hl, lam, tend)
        l2e_s.append(er_1)
        l2e_v.append(er_2)
        l2e_u.append(er_3)
        l2e_r.append(er_4)
    return l2e_s, l2e_v, l2e_u, l2e_r

# ================= MAIN DRIVER ===============================


maxlevels = 4
tend = 1
l = 1.0
p = 2
er_s, er_v, er_u, er_r = collecterrors(p, maxlevels, l, tend)
hconvergencetable(er_s, er_v, er_u, er_r, maxlevels)
