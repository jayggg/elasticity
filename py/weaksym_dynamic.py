"""
Equations of dynamic linear elasticity for stress sigma, velocity v,
and rotations r in 2D:

  (sigma', tau) + (v, div tau) + (Skw(r'), tau) = 0
 -(div sigma, w) + (v', w)                      = (F, w)
  (sigma', Skw(q))			        = 0

Here  Skw(m) = [ 0 m]  and prime denotes time derivative.
               [-m 0]

We perform spatial discretization with GG elements, implicit time
stepping  using Crank-Nicolson and measure convergence rates
when the exact solution is

 u(x,y,t) = [sin(pi * x) * sin(pi * y) * sin(t)]
            [x * (1 - x) * y (1 - y) * sin(t)].

Note that with this displacement, the intial velocity is

 v(x,y,0) = [sin(pi * x) * sin(pi * y)]
            [x * (1 - x) * y * (1 - y)],

which forms the data for the simulation.

u(x,y,t) is recovered by integrating in time using trapezoidal rule

"""
import ngsolve as ng
from ngsolve import sin, cos, sqrt, InnerProduct, div
from netgen.geom2d import unit_square
from numpy import pi

from ctypes import CDLL

mylngs = CDLL("../libelas.so")
ng.ngsglobals.msg_level = 1

# ========== INITIAL DATA ===============================

x = ng.x
y = ng.y


def initdata():

    """ Return two vectors, representing the initial values of
    div(sig(0)) and velocity v(0). (The remainder of the required
    initial data will be computed automatically.
    """

    divsig0 = ng.CoefficientFunction((0, 0))
    v0 = ng.CoefficientFunction((sin(pi * x) * sin(pi * y),
                                 x * (1 - x) * y * (1 - y)))

    return (divsig0, v0)

# ========= SOURCE  DATA ===================================


def F(t):
    F0 = -sin(pi * x) * sin(pi * y) * sin(t)
    F0 += - sin(t) * (-1.5 * pi * pi * sin(pi * x) * sin(pi * y) +
                      0.5 * (1 - 2 * x) * (1 - 2 * y))
    F1 = -x * (1 - x) * y * (1 - y) * sin(t)
    F1 += -sin(t) * (-y * (1 - y) - 2 * x * (1 - x) +
                     0.5 * pi * pi * cos(pi * x) * cos(pi * y))

    return ng.CoefficientFunction((F0, F1))

# ========= SOLVE  ====================================


def weaksym_dynamic(k, h, l, tend):

    """ PARAMETERS:

    k: determines polynomial order of stress (in P_k + bubbles),
       velocity (in P_{k-1}) and rotations (in P_k).

    h: maximal mesh size (of the mesh of unit square)

    l: sets timestep by  ∆t = h * l

    tend: final simulation time where error is measured.

    """

    mesh = ng.Mesh(unit_square.GenerateMesh(maxh=h))
    delta = l * h   # time step size ∆t

    S = ng.FESpace("ggfes", mesh, order=k)  # Stress space
    U = ng.VectorL2(mesh, order=k - 1)      # Displacement space = L2 x L2
    A = ng.L2(mesh, order=k)                # Space of rotations (skw sym mat)
    XY = ng.FESpace([S, U, A])

    # Setting initial sig, r values by solving a static problem:
    divsig0, v0 = initdata()

    sig, v, mu = XY.TrialFunction()
    tau, w, q = XY.TestFunction()

    a = ng.BilinearForm(XY, symmetric=True)
    a += ng.SymbolicBFI(InnerProduct(sig, tau))
    a += ng.SymbolicBFI(v * div(tau))
    a += ng.SymbolicBFI(mu * (tau[0, 1] - tau[1, 0]))
    a += ng.SymbolicBFI(div(sig) * w)
    a += ng.SymbolicBFI((sig[0,1] - sig[1,0]) *  q)

    fin = ng.LinearForm(XY)
    fin += ng.SymbolicLFI(divsig0 * w)

    a.Assemble()
    fin.Assemble()

    initial = ng.GridFunction(XY)
    initial.vec.data = a.mat.Inverse(XY.FreeDofs()) * fin.vec

    # u stores initial data and will contain future time iterates
    u = ng.GridFunction(XY)
    u.components[0].Set(initial.components[0])  # Set sig(0)
    u.components[2].Set(initial.components[2])  # Set r(0)

    # Set initial velocity by projecting v0 into fe space
    u.components[1].Set(v0)

    ng.Draw(u.components[1][1], mesh, "velocity", sd=2)

    """ Crank-Nicolson in matrix form reads as

     M * u(t+∆t) = S * u(t) + F(t)

          [    A    (∆t/2)B^T     D^T ]
      M = [ -(∆t/2)B     C       0    ]
          [    D         0       0    ]

          [    A    -(∆t/2)B^T     D^T  ]
      S = [  (∆t/2)B     C       0    ]
          [    D         0       0    ]
      F(t) = <(∆t/2)(F0(t)+F0(t+∆t)), v_1>
           + <(∆t/2)(F1(t)+F1(t+∆t)), v_2>
    """

    # Make the M matrix
    m = ng.BilinearForm(XY)
    m += ng.SymbolicBFI(InnerProduct(sig, tau))                        # A
    m += ng.SymbolicBFI(0.5 * delta * (v * div(tau)))  # (∆t/2)B^T
    m += ng.SymbolicBFI(mu * (tau[0,1] - tau[1,0]))          # D^T
    m += ng.SymbolicBFI(-0.5 * delta * (w * div(sig)))  # -(∆t/2)B^T
    m += ng.SymbolicBFI(v * w)                    # C
    m += ng.SymbolicBFI((sig[0, 1] - sig[1, 0]) * q)           # D
    m.Assemble()
    inv_M = m.mat.Inverse(XY.FreeDofs())

    # Make the S matrix
    s = ng.BilinearForm(XY, nonassemble = True)
    s += ng.SymbolicBFI(InnerProduct(sig, tau))      # A
    s += ng.SymbolicBFI(-0.5 * delta * v * div(tau))  # -(∆t/2)B^T
    s += ng.SymbolicBFI(mu * (tau[0, 1] - tau[1, 0]))            # D^T
    s += ng.SymbolicBFI(v * w)                    # C
    s += ng.SymbolicBFI(0.5 * delta * w * div(sig))  # (∆t/2)B^T
    s += ng.SymbolicBFI((sig[0, 1] - sig[1, 0]) * q)          # D

    # Displacement Space:
    # for the trapezoidal rule
    u_dis = ng.GridFunction(U)
    u_dis.vec.data += initial.components[1].vec.data

    # Vector to store time dependent rhs  S * u + F in Crank Nicolson.
    f = ng.LinearForm(XY)
    f.Assemble()
    ff = f.vec
    t = 0

    with ng.TaskManager():
        while t < tend:

            s.Apply(u.vec, ff)   # write S*u into ff

            # assemble time dependent source using F(t)
            li = ng.LinearForm(XY)
            Ft = F(t)
            Ftt = F(t + delta)
            li += ng.SymbolicLFI(0.5 * delta * (Ft * w + Ftt * w))
            li.Assemble()

            ff.data += li.vec    # assembled  S * u(t) + F(t)

            u_dis.vec.data += 0.5 * delta * u.components[1].vec.data
            u.vec.data = inv_M * ff  # u(t+∆t) = M^{-1}*(S*u(t)+F(t))
            u_dis.vec.data += 0.5 * delta * u.components[1].vec.data

            t += delta
            print('t=%g' % t)
            ng.Redraw(blocking=True)

    # Compare with exact solution
    ex_v1 = ng.CoefficientFunction(sin(pi * x) * sin(pi * y) * cos(t))
    ex_v2 = ng.CoefficientFunction(x * (1 - x) * y * (1 - y) * cos(t))

    ex_v = ng.CoefficientFunction((ex_v1, ex_v2))

    ex_s0 = pi * cos(pi * x) * sin(pi * y) * sin(t)
    ex_s1 = 0.5 * (pi * sin(pi * x) * cos(pi * y) +
                   (1 - 2 * x) * y * (1 - y)) * sin(t)
    ex_s2 = 0.5 * (pi * sin(pi * x) * cos(pi * y) +
                   (1 - 2 * x) * y * (1 - y)) * sin(t)
    ex_s3 = (1 - 2 * y) * x * (1 - x) * sin(t)

    ex_s = ng.CoefficientFunction((ex_s0, ex_s1,
                                   ex_s2, ex_s3))

    ex_r = 0.5 * (pi * sin(pi * x) * cos(pi * y) -
                  (1 - 2 * x) * y * (1 - y)) * sin(t)

    ex_u1 = ng.CoefficientFunction(sin(pi * x) * sin(pi * y) * sin(t))
    ex_u2 = ng.CoefficientFunction(x * (1 - x) * y * (1 - y) * sin(t))

    ex_u = ng.CoefficientFunction((ex_u1, ex_u2))
    norm_v = sqrt(ng.Integrate((u.components[1] - ex_v) *
                               (u.components[1] - ex_v), mesh))

    norm_s = sqrt(ng.Integrate(InnerProduct(u.components[0] - ex_s,
                                            u.components[0] - ex_s), mesh))
    norm_u = sqrt(ng.Integrate((u_dis - ex_u) *
                               (u_dis - ex_u), mesh))

    norm_r = sqrt(ng.Integrate((u.components[2] - ex_r) *
                               (u.components[2] - ex_r), mesh))

    return (norm_s, norm_v, norm_u, norm_r)


def hconvergencetable(e_1, e_2, e_3, e_4, maxr):
    print("===========================================" +
          "==================================")
    print(" Mesh  Errors_s   Order    Error_v    Order ",
          "Error_u   Order  Error_r   Order")
    print("------------------------------------------" +
          "----------------------------------")
    rate1 = []
    rate2 = []
    rate3 = []
    rate4 = []
    for i in range(maxr):
        rate1.append('  *  ')
        rate2.append('  *  ')
        rate3.append('  *  ')
        rate4.append('  *  ')
    for i in range(1, maxr):
        if abs(e_1[i]) > 1.e-15 and abs(e_2[i]) > 1.e-15:
            rate1[i] = format(ng.log(e_1[i - 1] / e_1[i]) / ng.log(2), '+5.2f')
            rate2[i] = format(ng.log(e_2[i - 1] / e_2[i]) / ng.log(2), '+5.2f')
            rate3[i] = format(ng.log(e_3[i - 1] / e_3[i]) / ng.log(2), '+5.2f')
            rate4[i] = format(ng.log(e_4[i - 1] / e_4[i]) / ng.log(2), '+5.2f')
    for i in range(maxr):
        print("h/ % -4d %8.2e   %s   %8.2e   %s  %8.2e  %s  %8.2e  %s" %
              (pow(2, i + 1), e_1[i], rate1[i], e_2[i], rate2[i],
               e_3[i], rate3[i], e_4[i], rate4[i]))

    print("======================================" +
          "=======================================")


def collecterrors(k, maxr, lam, tend):
    l2e_s = []
    l2e_v = []
    l2e_u = []
    l2e_r = []

    for l in range(0, maxr):
        hl = 2 ** (-l) / 2
        er_1, er_2, er_3, er_4 = weaksym_dynamic(k, hl, lam, tend)
        l2e_s.append(er_1)
        l2e_v.append(er_2)
        l2e_u.append(er_3)
        l2e_r.append(er_4)
    return l2e_s, l2e_v, l2e_u, l2e_r


# ============= MAIN DRIVER ==============================

maxlevels = 4
tend = 1
l = 1.0
p = 2
er_s, er_v, er_u, er_r = collecterrors(p, maxlevels, l, tend)
hconvergencetable(er_s, er_v, er_u, er_r, maxlevels)
