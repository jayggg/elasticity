"""
Solve a static linear elasticity problem with the weakly
symmetric mixed method in 3D using the GG stress element

  (sigma, tau) + (u, div tau) + (skew(l), tau)  = 0.0
  (div sigma, v)                                = (f,w)
  (sigma, skew(q))    			        = 0.0

 and perform a convergence study with this exact solution:

 u(x) = [sin(pi * x) * sin(pi * y) * sin(pi * z)]
        [x * (1 - x) * y * (1 - y) * z * (1 - z)]
        [0.0]

"""

from ngsolve import CoefficientFunction, SymbolicBFI, Integrate, log, sin
from ngsolve import cos, sqrt, InnerProduct, div, x, y, z
import ngsolve as ng
from netgen.csg import unit_cube
from numpy import pi
from ctypes import CDLL

mylngs = CDLL("../libelas.so")
ng.ngsglobals.msg_level = 1


def weaksym3d(k, h):
    mesh = ng.Mesh(unit_cube.GenerateMesh(maxh=h))

    # Stress space S:
    S = ng.FESpace("ggfes3d", mesh, order=k)
    # Displacement space U is two copies of
    U = ng.VectorL2(mesh, order=k - 1)
    # Antisymmetric matrix space for rotations A:
    A = ng.VectorL2(mesh, order=k)

    XY = ng.FESpace([S, U, A])

    sig, u, r = XY.TrialFunction()
    tau, w, q = XY.TestFunction()

    def Skw(r):
        return CoefficientFunction((0, -r[2], r[1],
                                    r[2], 0, -r[0],
                                    -r[1], r[0], 0), dims=(3,3))

    F0 = CoefficientFunction(-2 * pi * pi * sin(pi * x) *
                             sin(pi * y) * sin(pi * z) +
                             0.5 * (1 - 2 * x) * (1 - 2 * y) * z * (1 - z))
    F1 = CoefficientFunction(-y * (1 - y) * z * (1 - z) -
                             2 * x * (1 - x) * z * (1 - z) -
                             x * (1 - x) * y * (1 - y) +
                             0.5 * pi * pi * cos(pi * x) *
                             cos(pi * y) * sin(pi * z))
    F2 = CoefficientFunction(0.5 * pi * pi * cos(pi * x) * sin(pi * y) *
                             cos(pi * z) + 0.5 * (1 - 2 * z) * (1 - 2 * y) *
                             x * (1 - x))
    F = CoefficientFunction((F0,F1,F2))

    a = ng.BilinearForm(XY, symmetric=True)
    a += SymbolicBFI(InnerProduct(sig, tau))               # (sigma, tau)
    a += SymbolicBFI(u * div(tau) + div(sig) * w)
    a += SymbolicBFI(InnerProduct(Skw(r), tau) + InnerProduct(sig, Skw(q)))
                     
    f = ng.LinearForm(XY)
    f += ng.SymbolicLFI(F*w)

    c = ng.Preconditioner(a, type="direct")
    a.Assemble(heapsize=int(1e8))
    f.Assemble(heapsize=int(1e8))

    u = ng.GridFunction(XY)
    ng.BVP(bf=a, lf=f, gf=u, pre=c, maxsteps=10).Do()

    u0 = CoefficientFunction(sin(pi * x) * sin(pi * y) * sin(pi * z))
    u1 = CoefficientFunction(x * (1 - x) * y * (1 - y) * z * (1 - z))
    u2 = CoefficientFunction(0)

    ex_u = CoefficientFunction((u0, u1, u2))

    ex_s0 = CoefficientFunction(pi * cos(pi * x) * sin(pi * y) * sin(pi * z))
    ex_s1 = CoefficientFunction(0.5 * (pi * sin(pi * x) * cos(pi * y) *
                                       sin(pi * z) + (1 - 2 * x) *
                                       y * (1 - y) * z * (1 - z)))
    ex_s2 = CoefficientFunction(0.5 * pi * sin(pi * x) *
                                sin(pi * y) * cos(pi * z))
    ex_s3 = ex_s1
    ex_s4 = CoefficientFunction((1 - 2 * y) * x * (1 - x) * z * (1 - z))
    ex_s5 = CoefficientFunction(0.5 * (1 - 2 * z) * x * (1 - x) * y * (1 - y))
    ex_s6 = ex_s2
    ex_s7 = ex_s5
    ex_s8 = CoefficientFunction(0)

    ex_s = CoefficientFunction((ex_s0, ex_s1, ex_s2,
                                ex_s3, ex_s4, ex_s5,
                                ex_s6, ex_s7, ex_s8), dims=(3,3))

    ex_q1 = CoefficientFunction(-0.5 * (1 - 2 * z) * x * (1 - x) * y * (1 - y))

    ex_q2 = CoefficientFunction(0.5 * pi * sin(pi * x) *
                                sin(pi * y) * cos(pi * z))

    ex_q3 = CoefficientFunction(-0.5 * (pi * sin(pi * x) *
                                        cos(pi * y) * sin(pi * z) -
                                        (1 - 2 * x) * y * (1 - y) *
                                        z * (1 - z)))

    ex_q = CoefficientFunction((ex_q1, ex_q2, ex_q3))
    norm_u = sqrt(Integrate((u.components[1] - ex_u) *
                            (u.components[1] - ex_u), mesh))

    norm_s = sqrt(Integrate(InnerProduct(u.components[0] - ex_s,
                                         u.components[0] - ex_s), mesh))

    norm_r = sqrt(Integrate((u.components[2] - ex_q) *
                            (u.components[2] - ex_q), mesh))

    return norm_s, norm_u, norm_r


def hconvergencetable(e_1, e_2, e_3, maxr):
    print("============================================================")
    print(" Mesh   Errors_s  Order    Error_u   Order   Error_r   Order")
    print("------------------------------------------------------------")
    rate1 = []
    rate2 = []
    rate3 = []
    for i in range(maxr):
        rate1.append('  *  ')
        rate2.append('  *  ')
        rate3.append('  *  ')
    for i in range(1, maxr):
        if abs(e_1[i]) > 1.e-15 and abs(e_2[i]) > 1.e-15:
            rate1[i] = format(log(e_1[i - 1] / e_1[i]) / log(2), '+5.2f')
            rate2[i] = format(log(e_2[i - 1] / e_2[i]) / log(2), '+5.2f')
            rate3[i] = format(log(e_3[i - 1] / e_3[i]) / log(2), '+5.2f')

    for i in range(maxr):
        print(" h/%-4d %8.2e   %s   %8.2e   %s  %8.2e  %s" %
              (pow(2, i + 1), e_1[i], rate1[i],
               e_2[i], rate2[i], e_3[i], rate3[i]))

    print("============================================================")


def collecterrors(k, maxr):
    l2e_s = []
    l2e_u = []
    l2e_r = []
    for l in range(0, maxr):
        hl = 2**(-l) / 2
        er_1, er_2, er_3 = weaksym3d(k, hl)
        l2e_s.append(er_1)
        l2e_u.append(er_2)
        l2e_r.append(er_3)
    return l2e_s, l2e_u, l2e_r


maxlevels = 3
er_s, er_u, er_r = collecterrors(2, maxlevels)
hconvergencetable(er_s, er_u, er_r, maxlevels)
