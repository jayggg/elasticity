import ngsolve as ngs
from ngsolve import InnerProduct, SymbolicBFI, CoefficientFunction
from ngsolve import GridFunction, div, sqrt
import netgen.csg as ng
from ctypes import CDLL

mylib = CDLL("../libelas.so")
ngs.ngsglobals.msg_level = 1
ngs.SetHeapSize(int(1e8))
    
x = ngs.x
y = ngs.y
z = ngs.z

L = 5  # Length of the beam in the y-direction

brick = ng.OrthoBrick(ng.Pnt(0, 0, 0), ng.Pnt(L, 1, 1)).bc('free')
wall = ng.Plane(ng.Pnt(0, 0, 0), ng.Vec(-1, 0, 0)).bc('clamped')
cantilever = wall * brick
beam = ng.CSGeometry()
beam.Add(cantilever)
mesh = ngs.Mesh(beam.GenerateMesh(maxh=0.5))

k = 2   # Polynomial order

# Physical parameters
lam = 4.0
mu = 0.5
rho = 1

load = CoefficientFunction((0, -z * 0.004, y * 0.004))

S = ngs.FESpace('ggfes3d', mesh, order=k, dirichlet='free')
U = ngs.VectorL2(mesh, order=k - 1)
A = ngs.VectorL2(mesh, order=k)
X = ngs.FESpace([S, U, A])

sig, u, r = X.TrialFunction()
tau, w, q = X.TestFunction()

def Skw(r):
    return CoefficientFunction((0, -r[2], r[1],
                                r[2], 0, -r[0],
                                -r[1], r[0], 0), dims=(3, 3))


a1 = 0.5 / mu                 # Compliance tensor for isotropic mat
a2 = lam / (2.0 * mu * (3 * lam + 2 * mu))
trace = sig[0,0] + sig[1,1] + sig[2,2]

Asig = CoefficientFunction((a1 * sig[0] - a2 * trace,
                            a1 * sig[1], a1 * sig[2],
                            a1 * sig[3],
                            a1 * sig[4] - a2 * trace,
                            a1 * sig[5],
                            a1 * sig[6], a1 * sig[7],
                            a1 * sig[8] - a2 * trace))

a = ngs.BilinearForm(X, symmetric = True)
a += SymbolicBFI(InnerProduct(Asig, tau))
a += SymbolicBFI(u * div(tau) + div(sig) * w)
a += SymbolicBFI(InnerProduct(Skw(r), tau) + InnerProduct(sig, Skw(q)))

f = ngs.LinearForm(X)
f += ngs.SymbolicLFI(-load * w)

c = ngs.Preconditioner(a, type="direct", flags={"inverse": "umfpack"})
a.Assemble()
f.Assemble()

sur = ngs.GridFunction(X)
bvp = ngs.BVP(bf=a, lf=f, gf=sur, pre=c, maxsteps=10).Do()

VH = ngs.VectorH1(mesh, order=k)
dis = GridFunction(VH, 'displacement')
dis.Set(sur.components[1])

H = ngs.H1(mesh, order=k)
absrot = GridFunction(H, '|rotation|^2')
absrot.Set(sur.components[2] * sur.components[2])

ngs.Draw(absrot)
ngs.Draw(dis)
ngs.SetVisualization(deformation=True)
