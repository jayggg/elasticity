""" Solve a static linear elasticity problem with the
  weakly symmetric mixed method using the GG stress element

     (sig, tau) + (u, div tau) + (skew(r), tau)  + <l,tau.n> = 0
     (div sig, v)                                            = (f,w)
     (sig, skew(q))				             = 0
      <sig.n, m>                                             = 0

    and perform a convergence study with this exact solution:

  u(x) = [ sin(pi*x)*sin(pi*y) ]
         [ x*(1-x)* y*(1-y)    ]

  The corresponding source is

  F_1 = ( -3*pi*pi/2)*sin(pi*x)*sin(pi*y) +(2*x-1)*(2*y)/2

  F_2 = (-y*(1-y)-2*x*(1-x)+pi*pi*cos(pi*x)*cos(pi*y)).

"""

import ngsolve as ng
from ngsolve import CoefficientFunction, SymbolicBFI, SymbolicLFI, sin, cos
from ngsolve import InnerProduct, div
from netgen.geom2d import unit_square
from numpy import pi
from ctypes import CDLL

mylngs = CDLL("../libelas.so")
ng.ngsglobals.msg_level = 1

x = ng.x
y = ng.y


def weaksym_hybrid(k, h):

    mesh = ng.Mesh(unit_square.GenerateMesh(maxh=h))

    # Stress space S:
    S = ng.FESpace("ggfes", mesh, order=k, flags={'discontinuous': True})
    # Displacement space U is two copies of
    U = ng.VectorL2(mesh, order=k - 1)
    # Antisymmetric matrix space for rotations A:
    A = ng.L2(mesh, order=k)
    # Lagrange multiplier space
    L = ng.FacetFESpace(mesh, order=k, dirichlet="bottom|right|left|top")

    # Full product FE space
    X = ng.FESpace([S, U, A, L, L])

    sig, u, r, l1, l2 = X.TrialFunction()
    tau, v, q, m1, m2 = X.TestFunction()
    n = ng.specialcf.normal(mesh.dim)

    F1 = CoefficientFunction(-1.5 * pi * pi * sin(pi * x) * sin(pi * y) +
                             0.5 * (1 - 2 * x) * (1 - 2 * y))
    F2 = CoefficientFunction(-y * (1 - y) - 2 * x * (1 - x) +
                             0.5 * pi * pi * cos(pi * x) * cos(pi * y))

    a = ng.BilinearForm(X, symmetric=True, eliminate_internal=True)
    a += SymbolicBFI(InnerProduct(sig, tau))                        # (sig, tau)
    a += SymbolicBFI(u * div(tau))
    a += SymbolicBFI(r * (tau[0, 1] - tau[1, 0]))            # (r, skw tau)
    a += SymbolicBFI(div(sig) * v)              
    a += SymbolicBFI((sig[0,1] - sig[1, 0]) * q)# (q, skw sig)
    a += SymbolicBFI(l1 * (tau[0] * n[0] + tau[1] * n[1]) +  # <l,tau.n>
                     l2 * (tau[2] * n[0] + tau[3] * n[1]),
                     element_boundary=True)
    a += SymbolicBFI(m1 * (sig[0] * n[0] + sig[1] * n[1]) +  # <sig.n, m>
                     m2 * (sig[2] * n[0] + sig[3] * n[1]),
                     element_boundary=True)

    f = ng.LinearForm(X)
    f += SymbolicLFI(F1 * v[0] + F2 * v[1])

    c = ng.Preconditioner(a, type="direct")
    a.Assemble()
    f.Assemble()

    u = ng.GridFunction(X)
    ng.BVP(bf=a, lf=f, gf=u, pre=c, maxsteps=10).Do()

    exactu1 = CoefficientFunction(sin(pi * x) * sin(pi * y))
    exactu2 = CoefficientFunction(x * (1 - x) * y * (1 - y))
    ex_u = CoefficientFunction((exactu1, exactu2))

                     
    exs1 = CoefficientFunction(pi * cos(pi * x) * sin(pi * y))
    exs2 = CoefficientFunction(0.5 * (pi * sin(pi * x) *
                                      cos(pi * y) + (1 - 2 * x) * y * (1 - y)))
    exs3 = CoefficientFunction(0.5 * (pi * sin(pi * x) * cos(pi * y) +
                                      (1 - 2 * x) * y * (1 - y)))
    exs4 = CoefficientFunction((1 - 2 * y) * x * (1 - x))

    ex_s = CoefficientFunction((exs1, exs2,
                                exs3, exs4))
    exactq = CoefficientFunction(0.5 * (pi * sin(pi * x) * cos(pi * y) -
                                        (1 - 2 * x) * y * (1 - y)))

    norm_u = ng.sqrt(ng.Integrate((u.components[1] - ex_u) *
                                  (u.components[1] - ex_u), mesh))
    norm_r = ng.sqrt(ng.Integrate((u.components[2] - exactq) *
                                  (u.components[2] - exactq), mesh))
    norm_s = ng.sqrt(ng.Integrate(InnerProduct(u.components[0] - ex_s,
                                               u.components[0] - ex_s), mesh))

    return norm_s, norm_u, norm_r


def hconvergencetable(e_1, e_2, e_3, maxr):
    print("============================================================")
    print(" Mesh   Errors_s  Order    Error_u   Order   Error_r   Order")
    print("------------------------------------------------------------")
    rate1 = []
    rate2 = []
    rate3 = []
    for i in range(maxr):
        rate1.append('  *  ')
        rate2.append('  *  ')
        rate3.append('  *  ')
    for i in range(1, maxr):
        if abs(e_1[i]) > 1.e-15 and abs(e_2[i]) > 1.e-15:
            rate1[i] = format(ng.log(e_1[i - 1] / e_1[i]) / ng.log(2), '+5.2f')
            rate2[i] = format(ng.log(e_2[i - 1] / e_2[i]) / ng.log(2), '+5.2f')
            rate3[i] = format(ng.log(e_3[i - 1] / e_3[i]) / ng.log(2), '+5.2f')

    for i in range(maxr):
        print(" h/%-4d %8.2e   %s   %8.2e   %s  %8.2e  %s" %
              (pow(2, i + 1), e_1[i], rate1[i],
               e_2[i], rate2[i], e_3[i], rate3[i]))

    print("============================================================")


def collecterrors(k, maxr):
    l2e_s = []
    l2e_u = []
    l2e_r = []
    for l in range(0, maxr):
        hl = 2**(-l) / 2
        er_1, er_2, er_3 = weaksym_hybrid(k, hl)
        l2e_s.append(er_1)
        l2e_u.append(er_2)
        l2e_r.append(er_3)
    return l2e_s, l2e_u, l2e_r


maxlevels = 5
er_s, er_u, er_r = collecterrors(2, maxlevels)
hconvergencetable(er_s, er_u, er_r, maxlevels)
