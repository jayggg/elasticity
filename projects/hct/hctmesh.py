"""
Mesh refinement generator:

Given any geomtry and initial mesh, it adds the 
barycenter of each simplex as additional mesh points,
and then connects the vertices of the simplex to the barycenter.

Example when d = 2, each 2-simplex is refined into 3-subsimplices.
          *
         /|\
        / | \
       /  |  \
      /   |   \
     /  - * -  \
    / -       - \ 
   *-------------* 

"""
import ngsolve as ngs 
from netgen.meshing import Element1D, Element2D,Element3D
from netgen.meshing import MeshPoint, FaceDescriptor
from netgen.csg import unit_cube, Plane, Pnt
from netgen.geom2d import unit_square
import netgen.meshing as ngm
import numpy as np
ngs.ngsglobals.msg_level = 1


# Input: Example 3D case:
#geo = unit_cube
#mesh = ngs.Mesh(unit_cube.GenerateMesh(maxh=0.25))

# Input: Example 2D case:
geo = unit_square
mesh = ngs.Mesh(unit_square.GenerateMesh(maxh=0.25))


def HctMesh(geo,mesh):
    """ One Refinement a given mesh:
    Each d-simplex is divided in d+1-subsimplices, by adding the
    barycentric, and connecting the vertices to this point.
    """
    
    bmesh = ngm.Mesh()
    bmesh.SetGeometry(geo)
    dim = mesh.dim
    bmesh.dim = dim
    
    for p in mesh.ngmesh.Points():
        x,y,z =p.p
        bmesh.Add(MeshPoint(Pnt(x,y,z)))
        
    bmesh.Add (FaceDescriptor(surfnr=1,domin=1,bc=1))

    if (dim ==3):
        elem = mesh.ngmesh.Elements3D
    else:
        elem = mesh.ngmesh.Elements2D
        
    for el in elem():
        a0 = el.vertices[0]
        a1 = el.vertices[1]
        a2 = el.vertices[2]
        if (dim ==3):
            a3 = el.vertices[3]
        
        a0x, a0y, a0z = mesh.ngmesh.Points()[a0]
        a1x, a1y, a1z = mesh.ngmesh.Points()[a1]
        a2x, a2y, a2z = mesh.ngmesh.Points()[a2]
        if (dim ==3):
            a3x, a3y, a3z = mesh.ngmesh.Points()[a3]

        #Barycenter:
        bx = (1 / (dim + 1)) * (a0x + a1x + a2x)
        by = (1 / (dim + 1)) * (a0y + a1y + a2y)
        bz = 0.0
        if (dim ==3):
            bx += (1 / (dim + 1)) * a3x
            by += (1 / (dim + 1)) * a3y
            bz = (1/(dim + 1)) * (a0z + a1z + a2z + a3z)    
        aX = bmesh.Add(MeshPoint(Pnt(bx,by,bz)))

        
        if (dim ==3):
            # New 1st element:
            p0 = [a0.nr, a1.nr, a2.nr, aX.nr]
            bmesh.Add(Element3D(1, p0))   

            # Face of first element
            bmesh.Add(Element2D(1,[p0[0], p0[3], p0[2]]))#P0-0
            bmesh.Add(Element2D(1,[p0[0], p0[1], p0[3]]))#P0-1
            bmesh.Add(Element2D(1,[p0[1], p0[2], p0[3]]))#P0-2
            bmesh.Add(Element2D(1,[p0[0], p0[2], p0[1]])) #Global

            # New 2nd element:
            p1 = [aX.nr, a0.nr, a1.nr, a3.nr]
            bmesh.Add(Element3D(1,p1))        
            # Edges of second element
            bmesh.Add(Element2D(1,[p1[0], p1[3], p1[2]])) #P1-0
            bmesh.Add(Element2D(1,[p1[0], p1[1], p1[3]])) #P1-1
            bmesh.Add(Element2D(1,[p1[1], p1[2], p1[3]])) #Global
            
            # New 3rd element:
            p2 = [aX.nr, a2.nr, a0.nr, a3.nr ] 
            bmesh.Add(Element3D(1,p2))
            
            # Edges of third element
            bmesh.Add(Element2D(1,[p2[3], p2[1], p2[2]]))#G
            bmesh.Add(Element2D(1,[p2[3], p2[0], p2[1]])) #P2-2
        
            # New 4th element:
            p3 = [a3.nr, aX.nr,  a2.nr, a1.nr ] 
            # Edges of third element
            bmesh.Add(Element3D(1,p3))
            bmesh.Add(Element2D(1,[p3[3], p3[2], p3[0]])) #G
        else:
             # New 1st element:
            bmesh.Add(Element2D(1,[a0.nr, a1.nr,aX.nr]))
            # Edges of first element  
            bmesh.Add(Element1D([a0.nr, a1.nr],index=1)) #Global
            bmesh.Add(Element1D([a1.nr, aX.nr],index=1)) #p0-0
            bmesh.Add(Element1D([aX.nr, a2.nr],index=1)) #p0-1
            # New 2nd element:
            bmesh.Add(Element2D(1,[aX.nr, a1.nr, a2.nr]))
            # Edges of second element
            bmesh.Add(Element1D([aX.nr, a1.nr],index=1)) #p1-0
            bmesh.Add(Element1D([a1.nr, a2.nr],index=1)) #G
            # New 3rd element:
            bmesh.Add(Element2D(1,[aX.nr, a2.nr, a0.nr]))
            # Edges of third element
            bmesh.Add(Element1D([a2.nr, a0.nr],index=1)) #G
            bmesh.Add(Element1D([a0.nr, aX.nr],index=1)) #p2-0
            
    bmesh.Save("mesh"+str(dim)+"D.vol")
    return ngs.Mesh(bmesh)

meshb = HctMesh(geo,mesh)

ngs.Draw(meshb)

