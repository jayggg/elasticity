"""
Test to check symmetry of stress tensor when using 
HCT(Hseih-Clough-Toscher) mesh. 
Solve a static linear elasticity problem with the weakly
symmetric mixed method in 2D/3D using the BDM stress element:

  (sigma, tau) + (u, div tau) + (skew(l), tau)  = 0.0
  (div sigma, v)                                = (f,w)
  (sigma, skew(q))    			        = 0.0

 and perform a convergence study with this exact solution:

case 2D:

 u(x) = [ sin(pi*x)*sin(pi*y) ]
        [ x*(1-x)* y*(1-y)    ]

case 3D:

 u(x) = [sin(pi * x) * sin(pi * y) * sin(pi * z)]
        [x * (1 - x) * y * (1 - y) * z * (1 - z)]
        [0.0]

"""

from ngsolve import CoefficientFunction, SymbolicBFI, Integrate, log, sin
from ngsolve import cos, sqrt, InnerProduct, div, x, y, z
import ngsolve as ng
from netgen.csg import unit_cube
from netgen.geom2d import unit_square
from numpy import pi
from hctmesh import HctMesh 
from ctypes import CDLL

mylngs = CDLL("../../libelas.so")
ng.ngsglobals.msg_level = 1
ng.SetHeapSize(int(1e9))


# Input: (Switch between geometry 2D/3D)

#geo = unit_square
geo = unit_cube

initialmesh = ng.Mesh(geo.GenerateMesh(maxh=0.5))
k = 2 #order


# Generate HctMesh
mesh = HctMesh(geo, initialmesh)
dim = mesh.dim

#Space
if (dim ==2):
    # Stress space S
    ggfes = "ggfes"
    # Antisymmetric matrix space for rotations A
    A = ng.L2(mesh, order=k)
else:
    # Stress space S
    ggfes = "ggfes3d"
    # Antisymmetric matrix space for rotations A
    A = ng.VectorL2(mesh, order=k)

S = ng.FESpace(ggfes, mesh, order=k, flags={"nobubbles":True})
# Displacement space U is two copies of
U = ng.VectorL2(mesh, order=k - 1)


XY = ng.FESpace([S, U, A])

sig, u, r = XY.TrialFunction()
tau, w, q = XY.TestFunction()


def Skw(r):
    if (dim ==2):
        skw = CoefficientFunction((0,r, -r, 0))
    else:
        skw = CoefficientFunction((0, -r[2], r[1],
                                   r[2], 0, -r[0],
                                   -r[1], r[0], 0), dims=(3,3))
    return skw
    

if (dim ==3):
# Source term F 
    F0 = CoefficientFunction(-2 * pi * pi * sin(pi * x) *
                             sin(pi * y) * sin(pi * z) +
                            0.5 * (1 - 2 * x) * (1 - 2 * y) * z * (1 - z))
    F1 = CoefficientFunction(-y * (1 - y) * z * (1 - z) -
                             2 * x * (1 - x) * z * (1 - z) -
                             x * (1 - x) * y * (1 - y) +
                             0.5 * pi * pi * cos(pi * x) *
                             cos(pi * y) * sin(pi * z))
    F2 = CoefficientFunction(0.5 * pi * pi * cos(pi * x) * sin(pi * y) *
                             cos(pi * z) + 0.5 * (1 - 2 * z) * (1 - 2 * y) *
                             x * (1 - x))
    F = CoefficientFunction((F0,F1,F2))
    
    
else:
    # Source term F
    F = CoefficientFunction((-1.5 * pi * pi * sin(pi * x) * sin(pi * y) +
                             0.5 * (1 - 2 * x) * (1 - 2 * y),
                             -y * (1 - y) - 2 * x * (1 - x) +
                             0.5 * pi * pi * cos(pi * x) * cos(pi * y)))
    
def normsym(s):
    if (dim==2):
        norm = sqrt(Integrate((s[1]-s[2])*
                              (s[1]-s[2]), mesh))
    else:
        norm = sqrt(Integrate((s[1] - s[3]) * (s[1] - s[3])+
                              (s[2] - s[6]) * (s[2] - s[6])+
                            (s[5] - s[7]) * (s[5] - s[7]), mesh))
    return norm


a = ng.BilinearForm(XY, symmetric=True)
a += SymbolicBFI(InnerProduct(sig, tau))               # (sigma, tau)
a += SymbolicBFI(u * div(tau) + div(sig) * w)
a += SymbolicBFI(InnerProduct(Skw(r), tau) + InnerProduct(sig, Skw(q)))

f = ng.LinearForm(XY)
f += ng.SymbolicLFI(F*w)

c = ng.Preconditioner(a, type="direct")
a.Assemble()
f.Assemble()

u = ng.GridFunction(XY)
ng.BVP(bf=a, lf=f, gf=u, pre=c, maxsteps=10).Do()

err = normsym(u.components[0])

print("error" , err)

def test_hctstatic():
    success = (err < 1.e-11)
    msg = 'Symmetry error in '+str(dim)+ 'D  = %g != 0' % err
    assert success, msg
