import ngsolve as ngs
from ngsolve import cos, sin, tan, sqrt, exp
from ngsolve import InnerProduct, x, y, div
from netgen.geom2d import SplineGeometry
from ctypes import CDLL
from math import pi
mylngs = CDLL("../../libelas.so")
ngs.ngsglobals.msg_level = 1


h = 0.7        # mesh size
k = 3          # polynomial degree

# Make and mesh the domain
theta = -31.0 * pi / 180  # Angle of inclination


def rotate(X, Y):
    """
    From David George's coordinates: X, Y
    to   Netgen coordinates: x, y

    """
    xx = cos(theta) * X - sin(theta) * Y
    yy = sin(theta) * X + cos(theta) * Y
    return (xx, yy)


def invrot(xx, yy):
    """
    From  Netgen coordinates: x, y
    to    David George's coordinates: X, Y

    """
    X = cos(theta) * xx + sin(theta) * yy
    Y = -sin(theta) * xx + cos(theta) * yy
    return (X, Y)


""" Flume geometry in Netgen's x, y coordinates:

             i
  a +---------
    |	      \-
    |	        \-
    |	           *  h
    |		     \-
    |		       \--
    |		          \-
    |		            \-
    |		              \--
    |			         * g
    |				   \-
    |				      *-*-----------------+ d
    |				     f 	e		  |
    |							  |
    |							  |
    |							  |
    |							  |
  b +-----------------------------------------------------+ c

"""


def Domain(geom, h):

    fh = (0, 0)    # Origin
    fi = rotate(-5.5, 0)
    fa = (fi[0] - 3, fi[1])
    fb = (fa[0], -7)
    fd = rotate(7.8 + 2.7 + cos(14.5 * pi / 180) *
                sin(29 * pi / 180) / sin(75.5 * pi / 180),
                2.7 * tan(28 * pi / 180) +
                sin(14.5 * pi / 180) *
                sin(29 * pi / 180) / sin(75.5 * pi / 180))
    fe = rotate(7.8 + cos(14.5 * pi / 180) *
                sin(29 * pi / 180) / sin(75.5 * pi / 180),
                sin(14.5 * pi / 180) *
                sin(29 * pi / 180) / sin(75.5 * pi / 180))
    ff = rotate(7.8 + cos(10 * pi / 180) *
                sin(20 * pi / 180) / sin(80 * pi / 180),
                sin(10 * pi / 180) *
                sin(20 * pi / 180) / sin(80 * pi / 180))
    fg = rotate(7.8, 0)

    fc = (fd[0], -7)

    pnts = [fa, fb, fc, fd,
            fe, ff, fg, fh, fi]
    dom = [geom.AppendPoint(*p) for p in pnts]
    lines0 = [(dom[0], dom[1], "ab", 1, 0),
              (dom[1], dom[2], "bc", 1, 0),
              (dom[2], dom[3], "topright", 1, 0)]
    lines1 = [(dom[3], dom[4], "ed", 1, 0)]
    curves = [(dom[4], dom[5], dom[6], "gfe", 1, 0)]
    lines2 = [(dom[6], dom[7], "hg", 1, 0)]
    lines3 = [(dom[7], dom[8], "ih", 1, 0),
              (dom[8], dom[0], "ai", 1, 0)]
    for p0, p1, bn, ml, mr in lines0:
        geom.Append(["line", p0, p1], bc=bn, leftdomain=ml, rightdomain=mr)

    for p0, p1, bn, ml, mr in lines1:
        geom.Append(["line", p0, p1], bc=bn,
                    leftdomain=ml, rightdomain=mr, maxh=h / 4)

    for p0, p1, p2, bc, left, right in curves:
        geom.Append(["spline3", p0, p1, p2],
                    bc=bc, leftdomain=left, rightdomain=right, maxh=h / 4)

    for p0, p1, bn, ml, mr in lines2:
        geom.Append(["line", p0, p1], bc=bn,
                    leftdomain=ml, rightdomain=mr, maxh=h / 4)

    for p0, p1, bn, ml, mr in lines3:
        geom.Append(["line", p0, p1], bc=bn, leftdomain=ml, rightdomain=mr)
    return (geom, dom)


def MakeMesh(h):
    geometry = SplineGeometry()
    geometry, geom = Domain(geometry, h)
    return ngs.Mesh(geometry.GenerateMesh(maxh=h))


mesh = MakeMesh(h)

# ---------------------------------------------------------------
# Problem parameters

rho = 1.0   # Density
lam = 4.0   # Lame coefficients lambda and mu:
mu = 0.5

# P and S wave speeds:
cp = sqrt((lam + 2 * mu) / rho)
cs = sqrt(mu / rho)

# Conservative time step size ∆t
delta = 0.9 * h / (k * k * max(cp, cs))

n = ngs.specialcf.normal(mesh.dim)

# Absorbing boundary conditions:


def B(ss):

    """Return the action the matrix B (in the 1st order absorbing b.c.)
    on a stress matrix ss.  The b.c. is written e.g. in Bamberger et al
    as

    sigma n + c v = 0

    where c is the matrix c = rho * N * diag(cp,cs) * N and
    N = [ nx ny; ny -nx]. We invert c to get B.
    """

    # Compute  c = rho * N * diag(cp,cs) * N
    c = [[rho * (n[0] * n[0] * cp + n[1] * n[1] * cs),
          rho * n[0] * n[1] * (cp - cs)],
         [rho * n[0] * n[1] * (cp - cs),
          rho * (n[1] * n[1] * cp + n[0] * n[0] * cs)]]
    cdet = c[0][0] * c[1][1] - c[0][1] * c[1][0]

    # Compute B =  inv(c)
    B = [[c[1][1] / cdet, -c[0][1] / cdet],
         [-c[1][0] / cdet, c[0][0] / cdet]]
    Bs = (B[0][0] * ss[0] + B[0][1] * ss[2],
          B[0][0] * ss[1] + B[0][1] * ss[3],
          B[1][0] * ss[0] + B[1][1] * ss[2],
          B[1][0] * ss[1] + B[1][1] * ss[3])

    return ngs.CoefficientFunction(Bs, dims=(2, 2))


# Forcing through boundary conditions:
aa = 30
bb = 0.2


def G(t):
    (X, Y) = invrot(x, y)
    U = 10 * exp(-t * t) * exp(-aa * (X - bb - cp * t) * (X - bb - cp * t))
    sbdry = ngs.CoefficientFunction((0, 0, 0, -U), dims=(2, 2))
    return sbdry


# Initial conditions:
sinit = ngs.CoefficientFunction((0, 0, 0, 0), dims=(2, 2))
vinit = ngs.CoefficientFunction((0, 0))
rinit = ngs.CoefficientFunction(0)

# Compute:

S = ngs.FESpace("ggfes", mesh, order=k, dirichlet='ih|hg|gfe|ed')
V = ngs.VectorL2(mesh, order=k - 1)      # Velocity space = L2 x L2
A = ngs.L2(mesh, order=k)                # Space of rotations (skw sym mat)
X = ngs.FESpace([S, V, A])

sig, v, r = X.TrialFunction()
tau, w, q = X.TestFunction()


u = ngs.GridFunction(X)
u.components[1].Set(vinit)
u.components[2].Set(rinit)


ngs.Draw(u.components[0][1], mesh, "s12")
ngs.Draw(u.components[0][3], mesh, "s22")
ngs.Draw(u.components[0][0], mesh, "s11")
ngs.Draw(u.components[0][0] + u.components[0][3], mesh, "trace(stress)",
         autoscale=False, min=-5, max=1)


"""
Formulation:  Weakly symmetric mixed method:

 (sigma',tau) + (v, div tau) + <B sigma.n,tau.n> + (Skw r',tau) = 0
 (div sigma, w) - (v', w)   = -(F,w)
 (sigma', Skw q)   = 0

Discretization:   Backward Euler in matrix form:

 M * u(t+∆t) = S * u(t) + RHS(t)

      [    A      ∆t B^T    D^T   ]
  M = [  ∆t B       -C       0    ]
      [    D         0       0    ]

      [    A         0       D^T  ]
  S = [    0        -C       0    ]
      [    D         0       0    ]

  RHS(t) = ∆t <G(t+∆t)n, tau n> -  ∆t (F(t+∆t), w)

"""

a1 = 0.5 / mu
a2 = lam / (4.0 * mu * (lam + mu))
Asig = ngs.CoefficientFunction((a1 * sig[0] - a2 * (sig[0] + sig[3]),
                                a1 * sig[1], a1 * sig[2],
                                a1 * sig[3] - a2 * (sig[0] + sig[3])),
                               dims=(2, 2))

# Make the M matrix
m = ngs.BilinearForm(X)
m += ngs.SymbolicBFI(delta * InnerProduct(B(sig.Trace()), tau.Trace()),
                     ngs.BND, definedon=mesh.Boundaries('ab|ai|bc|cd'))
m += ngs.SymbolicBFI(InnerProduct(Asig,tau))
m += ngs.SymbolicBFI(delta * v * div(tau))             # ∆t B^T
m += ngs.SymbolicBFI(r * (tau[0, 1] -  tau[1, 0]))  # D^T
m += ngs.SymbolicBFI(delta * div(sig) * w)             # ∆t B
m += ngs.SymbolicBFI(-rho * (v * w))                   # -C
m += ngs.SymbolicBFI((sig[0, 1] - sig[1, 0]) * q)      # D
m.Assemble()
inv_M = m.mat.Inverse(X.FreeDofs())

# Make the S matrix
s = ngs.BilinearForm(X, nonassemble=True)
s += ngs.SymbolicBFI(InnerProduct(Asig, tau))       # A, only vol part has d_t
s += ngs.SymbolicBFI(r * (tau[0, 1] - tau[1, 0]))   # D^T
s += ngs.SymbolicBFI(-rho * (v * w))                # -C
s += ngs.SymbolicBFI((sig[0, 1] - sig[1, 0]) * q)   # D

f = ngs.LinearForm(X)
f.Assemble()
ff = f.vec
t = 0
tend = 3.5
itcnt = 0

with ngs.TaskManager():
    while t < tend:
        s.Apply(u.vec, ff)  # write S*u into ff
        u1 = ngs.GridFunction(X)
        u1.components[0].Set(G(t + delta),
                             ngs.BND,
                             definedon=mesh.Boundaries('ih|hg|gfe|ed'))
        ff.data -= m.mat * u1.vec
        u.vec.data = inv_M * ff
        u.vec.data += u1.vec

        if itcnt % 50 == 0:
            # solfilename = 'solutions/s_vx_vy_a_t%5.3f't+'.sol'
            solfilename = 'stress' + str(t)
            # VTKOutput object
            vtk = ngs.VTKOutput(ma=mesh, coefs=[u.components[1]],
                                names=["sol"], filename=solfilename,
                                subdivision=3)

            # Exporting the results:
            vtk.Do()

            #  u.Save(solfilename)

        itcnt += 1
        t += delta
        print('t=%g' % t)
        ngs.Redraw(blocking=True)
