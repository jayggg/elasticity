\documentclass[12pt]{report}
\usepackage[margin=1in,footskip=0.25in]{geometry}
\usepackage{setspace}
\usepackage{amsmath}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{eucal,accents}

\usepackage{amssymb}
\usepackage[english]{babel}


\usepackage[table]{xcolor}

% Text Colors:
\usepackage{color} 
\definecolor{light-gray}{gray}{0.95}
\definecolor{rodi}{rgb}{0,1,0}
\definecolor{orange}{rgb}{1,0.5,0}
\definecolor{LightCyan}{rgb}{0.88,1,1}
\definecolor{morado}{rgb}{0.44, 0.16, 0.39}
\newcommand\x{\times}
\newcommand\y{\cellcolor{green!10}}
\newcommand{\red}[1]{\textcolor{red}{#1}}
\newcommand{\blue}[1]{\textcolor{blue}{#1}}
\newcommand{\green}[1]{\textcolor{green!60!black}{#1}}
% Shortcuts  for operator A 
\newcommand{\dt}{\partial_t } 
\newcommand{\dx}{\partial_x } 
\newcommand{\RR}[1]{\mathbb{#1}}
\newcommand{\forAll}{\text{ for all }}
\newcommand{\dense}{\displaystyle{\mathop{\underset{\mathrm{dense}}{\subseteq}}}}
\newcommand{\dd}{\partial} 

\newcommand{\Wo}{\ring{W}}
\newcommand{\wo}{\ring{w}}
\newcommand{\vx}{\vec x}
\newcommand{\vp}{\vec p}
\newcommand{\vn}{\vec n}
\newcommand{\vu}{\vec u}

\newcommand{\hn}{\hat n}
\newcommand{\hw}{\hat w}
\newcommand{\hW}{\hat W}
\newcommand{\hA}{\hat A}
\newcommand{\hx}{{\hat x}}
\newcommand{\htt}{{\hat t}}
\newcommand{\hK}{\hat K}
\newcommand{\hu}{\hat u}
\newcommand{\hz}{\hat z}
\newcommand{\hr}{\hat r}
\newcommand{\grad}{\nabla}
\newcommand{\dom}{ \partial\Omega}
\newcommand{\bb}[1]{ \begin{bmatrix}#1\end{bmatrix}}


% Brackets
\newcommand{\ip}[1]{\langle{#1}\rangle}
\newcommand{\anK}[1]{\langle{#1}\rangle_{W(K)}}
\newcommand{\anh}[1]{\langle{#1}\rangle_{W(\Omega_h)}}
\newcommand{\om}{\Omega}

% Brackets
\newcommand{\an}[1]{\langle{#1}\rangle_W}

% Modify the sections style
\usepackage{sectsty}
\sectionfont{\fontsize{13}{15}\selectfont}
\subsectionfont{\fontsize{11}{15}\selectfont}
%
\usepackage{tikz}
\usetikzlibrary{decorations.text}
\usetikzlibrary{calc}
\usepackage[T1]{fontenc}
\usepackage[font=small,labelfont=bf,tableposition=top]{caption}

\DeclareCaptionLabelFormat{andtable}{#1~#2  \&  \tablename~\thetable}
%
\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhf{}
\lhead{\textbf{\textcolor{green!30!black}{Elastodynamics simulation report for David George}} \\
{Paulina Sep\'{u}lveda \hfill  Advisor: Jay Gopalakrishnan}}
\rfoot{Page \thepage}


\begin{document}


\vspace{1cm}

\section*{Model problem } 

Let $\om$ be the domain of simulation representing the flume geometry
and let $\Gamma_0$ be the boundary part subjected to landslide-type
loading. The remainder of the boundary is denoted by $\Gamma_1$. We
want to numerically solve the following boundary value problem.

Find the stress
$\sigma : \om \times (0,t_{end}) \to \mathbb{R}^{2 \times 2}$ and the
velocity $v : \om \times (0,T) \to \mathbb{R}^2$ solving the plane
strain equations
\begin{align}
\mathcal{A}\,\partial_t \sigma - \varepsilon(v) & =0 \quad\text{ on } \om \label{Hook}
\\
 \rho \, \partial_t v  - \mathrm{div} \, \sigma & = f \quad \text{ on } \om \label{motion}
\end{align}
with initial conditions
\begin{align*}
\sigma (x, 0 ) & = 0\qquad x \in \om \\
v (x, 0 ) & = 0 \qquad   x \in \om
\end{align*}
and boundary conditions
\begin{align}
\label{eq:bc1}
\sigma  n & = g \qquad \text{ at } \Gamma_0 \times (0,t_{end}) 
 \\
\label{eq:bc2}
\sigma n + Cv & = 0 \qquad \text{ at } \Gamma_1 \times (0,t_{end}).
\end{align}

\vspace{-5cm}
\hfill
\begin{tikzpicture}[scale=0.2]
%\coordinate (O) at  (0,0);
%\coordinate (H) at (-1.5,0);
%\coordinate(B) at (7.8,0);

\def \atheta {-31}
\def \aalpha{-3}
\begin{scope}[rotate=\atheta]

\def \dpa {0.2cm}; % Soil             : 0-2m
\def \dpb {0.6cm}; % Weathered bedrock: 2-6m
\def \dpc {0.9cm}; % Bedrock 1        : 6-9m
\def \dpd {1.3cm}; % aux layer       : 9-(dp4)m
\def \ldist {5.5cm}; %left auxiliar distance 
\def \dflume {7.8cm}; %distance of the flume
\def \rdist  {2.7cm}; %right auxiliar distance
\draw[fill] (0,0) circle (2pt) coordinate (fh);   % origin
% left dom 0
\draw[fill] (-\ldist,0) circle (1pt) coordinate (fa);
\draw[fill] ({- \ldist+ \dpa*tan (-\atheta)} , - 0.2) circle (1pt) coordinate (fb);
\draw[red] (fh) -- (fa);
\draw[blue] (fa) -- (fb);
% curv
\draw[fill] (\dflume,0) circle (1pt) coordinate (fg);
\draw[thick,red]
 ([shift=(-90:1cm)]\dflume,1) arc (-90:-90-\atheta+\aalpha:1cm) coordinate (fe);
%right dom 0

\draw[fill] ( $(fe) + (\rdist,{ \rdist*tan(-\atheta+\aalpha)})$) circle (1pt) coordinate (fd);
\draw[fill] ($(fd|-fh) +tan(-\atheta)*(fe-|fh)+ ({\rdist*tan(-\atheta+\aalpha)+ \dpa)*tan(-\atheta)}, -\dpa)$) circle (1pt) coordinate (fc);


\draw[blue] (fb)--(fc);
\draw[blue] (fc)--(fd);
\draw[red] (fd)--(fe);
\draw[red] (fg) --(fh);
\draw [red,  thick] (1.3,2) node [ below] {\fontsize{10}{15}\selectfont $\Gamma_0$};

%  left dom 1 
\draw[fill]({- \ldist+ \dpb*tan (-\atheta)} , - \dpb)  circle (1pt) coordinate (fl1);
\draw[fill] ($(fd|-fh) +tan(-\atheta)*(fe-|fh)+ ({\rdist*tan(-\atheta+\aalpha)+ \dpb)*tan(-\atheta)},{-\dpb})$ circle (1pt) coordinate (fr1);
\draw[blue] (fb)--(fl1);
\draw[blue] (fl1)--(fr1);
\draw[blue] (fr1)--(fc);

% left dom 2
\draw[fill] ({- \ldist+ \dpc*tan (-\atheta)} , - \dpc) circle (1pt) coordinate (fl2);
\draw[fill] ($(fd|-fh) +tan(-\atheta)*(fe-|fh)+ ({\rdist*tan(-\atheta+\aalpha)+ \dpc)*tan(-\atheta)},{-\dpc})$ circle (1pt) coordinate (fr2);
\draw[blue] (fl1)--(fl2);
\draw[blue] (fl2)--(fr2);
\draw[blue] (fr2)--(fr1);

% left dom 3

\draw[fill] ($(fd|-fh) +tan(-\atheta)*(fe-|fh)+ ({\rdist*tan(-\atheta+\aalpha)+ \dpd)*tan(-\atheta)},{-\dpd})$) circle (1pt) coordinate (fr3);
\draw[fill,rotate=-\atheta] (fa|-fr3) circle (1pt) coordinate (fl3);

\draw[blue] (fl2)--(fl3);
\draw[blue] (fl3)--(fr3);
\draw[blue] (fr3)--(fr2);

%%\draw[blue,rotate=31] (H1) -- (-4.5cm,-8);
 \draw [blue,  thick] (3.3,-7) node [ below] {\fontsize{10}{15}\selectfont $\Gamma_1$};

\end{scope}
\end{tikzpicture} \\

\vspace{2cm}
\noindent Here $n$ denotes the outward unit normal vector, so the
matrix-vector product $\sigma n$ represents the force applied at the
boundary due to the landslide. This is given as an input vector
function $g : \Gamma_0 \to \mathbb{R}^2$. The other inputs are
material constants in
\begin{equation}
  \label{eq:A}
 \mathcal{A}\tau = \frac{1}{2\mu} \tau - \frac{\lambda}{4\mu(  
 \lambda + \mu ) } \mathrm{tr}(\tau) I_{2\times 2}.
\end{equation}
The matrix $C$ is the well-known matrix used to impose the first order
absorbing boundary conditions [Bamberger et al] on $\Gamma_1$, namely 
\begin{align*}
C  = \rho \bb{ n_x & n_y \\ n_y & -n_x}  \bb{ c_p & 0 \\ 0 & c_s}\bb{ n_x & n_y \\ n_y & -n_x
},
\qquad  \mu =\rho c_s ^2, \qquad \lambda = \rho c_p ^2-2\mu.
\end{align*}

\section*{Parameter values} 

{\em Material parameters}

\begin{center}
\begin{tabular}{|c|>{\columncolor{blue!1}}c|>{\columncolor{blue!5}}c|>{\columncolor{blue!15}}c|>{\columncolor{blue!40}}cc|}\hline
 & Soil & Weathered bedrock & Bedrock 1 & Bedrock 2 \\ \hline
 depth & 0-2m & 2-6m & 6-9m & 9m -- \\ \hline
 $ c_s$ &  150m/s & 600m/s & 600m/s & 1200m/s \\ \hline
 $ c_p$ &  220m/s & 900m/s & 1700m/s& 1700m//s \\ \hline
 $\rho$ & 2000 kg/m$^3$ &  2000 kg/m$^3$ & 2000 kg/m$^3$ & 2000 kg/m$^3$\\ \hline
\end{tabular}
\end{center}

\begin{tikzpicture}[scale=1,rotate=0]

\draw[help lines, color=gray!30, dashed] (-5,-9) grid (11,4);
\def \atheta {-31}
\def \aalpha{-3}



\begin{scope}[rotate=\atheta]

\def \dpa {0.2cm}; % Soil             : 0-2m
\def \dpb {0.6cm}; % Weathered bedrock: 2-6m
\def \dpc {0.9cm}; % Bedrock 1        : 6-9m
\def \dpd {1.3cm}; % aux layer       : 9-(dp4)m
\def \ldist {5.5cm}; %left auxiliar distance 
\def \dflume {7.8cm}; %distance of the flume
\def \rdist  {2.7cm}; %right auxiliar distance
\draw[fill] (0,0) circle (2pt) coordinate (fh) node [above] {\fontsize{10}{15}\selectfont $h$};   % origin
% left dom 0
\draw[fill] (-\ldist,0) circle (1pt) coordinate (fa)  node [above] {\fontsize{7}{15}\selectfont $a$};
\draw[fill] ({- \ldist+ \dpa*tan (-\atheta)} , - 0.2) circle (1pt) coordinate (fb) node [left] {\fontsize{7}{15}\selectfont $b$};
\draw[red] (fh) -- node [above,rotate=\atheta] {\fontsize{7}{15}\selectfont $ltop$} (fa) ;
\draw[line width=0.5mm,  blue] (fa) --node [left,blue] {\fontsize{7}{15}\selectfont $lft0$---}(fb);

% curv
\draw[fill] (\dflume,0) circle (1pt) coordinate (fg);
\draw[thick,red]
 ([shift=(-90:1cm)]\dflume,1) arc (-90:-90-\atheta+\aalpha:1cm) coordinate (fe) node [above,black] {\fontsize{8}{15}\selectfont $\begin{matrix}\textcolor{red}{curve}\\ gfe\end{matrix}$};
\draw[fill] (fe)  circle (1pt);

%right dom 0
\draw[fill] ( $(fe) + (\rdist,{ \rdist*tan(-\atheta+\aalpha)})$) circle (1pt) coordinate (fd) node [right,black] {\fontsize{10}{15}\selectfont $d$};
\draw[fill] ($(fd|-fh) +tan(-\atheta)*(fe-|fh)+ ({\rdist*tan(-\atheta+\aalpha)+ \dpa)*tan(-\atheta)}, -\dpa)$) circle (1pt) coordinate (fc)node [right] {\fontsize{10}{15}\selectfont $c$};


\draw[blue] (fb)-- (fc);
\draw[line width=0.5mm,  blue] (fc)--node [right,blue] {\fontsize{8}{15}\selectfont ---$rgt0$} (fd);
\draw[red] (fd)-- node [above,rotate=\aalpha] {\fontsize{10}{15}\selectfont $rtop$} (fe);
\draw[red] (fg)-- node [above,rotate=\atheta] {\fontsize{10}{15}\selectfont $flume$} (fh);

\fill[fill=blue!2,fill opacity=0.7] (fa)--(fb)--(fc) --(fd)--(fe)--(fg)--(fh)--(fa);

%  left dom 1 
\draw[fill]({- \ldist+ \dpb*tan (-\atheta)} , - \dpb)  circle (1pt) coordinate (fl1) node [left] {\fontsize{8}{15}\selectfont $l1$};
\draw[fill] ($(fd|-fh) +tan(-\atheta)*(fe-|fh)+ ({\rdist*tan(-\atheta+\aalpha)+ \dpb)*tan(-\atheta)},{-\dpb})$ circle (1pt) coordinate (fr1) node [right] {\fontsize{8}{15}\selectfont $r1$};
\draw[line width=0.5mm,  blue] (fb)--node [left,blue] {\fontsize{8}{15}\selectfont $lft1$---}(fl1);
\draw[blue] (fl1)--(fr1);
\draw[line width=0.5mm,  blue] (fr1)--node [right,blue] {\fontsize{8}{15}\selectfont ---$rgt1$} (fc);

\fill[fill=blue!10] (fb)--(fl1)--(fr1) --(fc)--(fb);

% left dom 2
\draw[fill] ({- \ldist+ \dpc*tan (-\atheta)} , - \dpc) circle (1pt) coordinate (fl2) node [left] {\fontsize{8}{15}\selectfont $l2$};
\draw[fill] ($(fd|-fh) +tan(-\atheta)*(fe-|fh)+ ({\rdist*tan(-\atheta+\aalpha)+ \dpc)*tan(-\atheta)},{-\dpc})$ circle (1pt) coordinate (fr2) node [right] {\fontsize{8}{15}\selectfont $r2$};
\draw[line width=0.5mm,  blue] (fl1)--node [left,blue] {\fontsize{8}{15}\selectfont $lft2$---}(fl2);
\draw[line width=0.1mm,  blue] (fl2)--(fr2);
\draw[line width=0.5mm,  blue] (fr2)--node [right,blue] {\fontsize{8}{15}\selectfont ---$rgt2$}(fr1);
\fill[fill=blue!30] (fl1)--(fl2)--(fr2) --(fr1)--(fl1);

% left dom 3

\draw[fill] ($(fd|-fh) +tan(-\atheta)*(fe-|fh)+ ({\rdist*tan(-\atheta+\aalpha)+ \dpd)*tan(-\atheta)},{-\dpd})$) circle (1pt) coordinate (fr3) node [right] {\fontsize{8}{15}\selectfont $r3$};
\draw[fill,rotate=-\atheta] (fa|-fr3) circle (1pt) coordinate (fl3) node [left] {\fontsize{8}{15}\selectfont $l3$};;

\draw[line width=0.5mm,  blue] (fl2)--node [left,blue] {\fontsize{8}{15}\selectfont $lft3$---}(fl3);
\draw[line width=0.5mm,  blue] (fl3)--node [below,blue] {\fontsize{8}{15}\selectfont $bot3$}(fr3);
\draw[line width=0.5mm,  blue] (fr3)--node [right,blue] {\fontsize{8}{15}\selectfont ---$rgt3$}(fr2);
\fill[fill=blue!40] (fl2)--(fl3)--(fr3) --(fr2)--(fl2);

%%\draw[blue,rotate=31] (H1) -- (-4.5cm,-8);
 \draw [black,  thick,] (10,0.3) node [ below,rotate=-31] {\fontsize{10}{15}\selectfont Mat 0};
 \draw [black,  thick,] (1.3,-0.13) node [ below,rotate=-31] {\fontsize{10}{15}\selectfont Mat 1};
  \draw [black,  thick] (3,-0.45) node [ below,rotate=-31] {\fontsize{10}{15}\selectfont Mat 2};
 \draw [black,  thick] (1.3,-3) node [ below] {\fontsize{10}{15}\selectfont Mat 3};

\end{scope}

\draw[<-, thick] (-2,0)--(0,0) ;
\draw[->, thick] (0,0)--({-2*cos(-31)},{2*sin(31)}) ;
\draw (-0.8,0.5) arc (149:180:1); 
\node[] at (162:0.5)  {\fontsize{8}{15}\selectfont $\theta$};


\draw[->, thick] (7.2,-4.15)--(10,-4.15) ;
\draw (9.2,-4.26) arc (-3:0:2.4); 

\node[] at (-25.6:9.8)  {\fontsize{8}{15}\selectfont $\alpha$};
\end{tikzpicture} 

{\em Parameters of the geometry:}

\begin{itemize}
\item $\theta = 31 ^\circ$ define the slope of the flume and slope of
  the trailing end is $\alpha = -3^\circ$.

\item The origin of the system is $h = (0,0)$. The above figure shows
  the coordinate system used in the computations.

\item \textbf{$\Gamma_1$} (blue) is the boundary part where first
  order absorbing boundary conditions are imposed.
  \textbf{$\Gamma_0$} (red) is the boundary part where the given force
  data is prescribed as an essential boundary condition on the stress
  $\sigma$.
\end{itemize}


\section*{Scaling}

In order to work with unit order material coefficients (so as not to
lose too many digits in the computations) we perform dimensional
scaling of the model before computing. Since the elastic waves
propagate in a short period of time and the physical forces are
expected to be large, we rescale time $t$ and stress $\sigma$ by
\[
\hat t = T t \quad \text{ and } \quad \hat \sigma = M^{-1} \sigma 
\]
where $T$ is the characteristic time and $M$ is the characteristic
mass.  We use the characteristic mass~$M$ to scale $\sigma$ since the
units for the stress $\sigma$ are
$\mathrm{Kg}\, \mathrm{m}^{-2}\mathrm{s}^{-2}$. 


Then~\eqref{Hook} and~\eqref{motion} become 
\begin{align*}
  (MT \mathcal A)    \partial_{\hat t} \hat\sigma - \varepsilon (v) & = 0
  &\implies  
\hat{\mathcal A}    \partial_{\hat t} \hat\sigma - \varepsilon (v) & = 0
  \\
  (\rho T)  \partial_{\hat t} v - M\mathrm{div} \hat \sigma & = f
  & \implies 
    \hat \rho  \partial_{\hat t} v - \mathrm{div} \hat \sigma & = \hat f
\end{align*}
where the rescaled material parameters are defined by 
\[
  \hat {\mathcal A} = MT \mathcal A, \qquad \hat \rho = \frac{\rho T }{M}, \qquad 
  \hat f = \frac{1}{M} f.
\]
Note that the rescaled compliance tensor $\hat {\mathcal A}$ can
be equivalently be expressed as in~\eqref{eq:A} by substituting for
$\lambda$ and $\mu$ the rescaled Lame parameters
$\hat \lambda =\lambda/(MT)$ and $\hat \mu = \mu/(MT)$.
In  our computations we use $\hat t$ and $\hat \sigma$ with 
\[
T = 10^3 \quad\text{and}\quad M = 10^6.
\]
This gives unit sized material parameters $\hat\lambda$ and $\hat \mu$
for the computation.

In the computations, we must also replace the boundary
conditions. Equation~\eqref{eq:bc1} is replaced by
\[
\hat \sigma n  = \hat g( x, \hat t), 
\qquad \text{ on } \Gamma_0 \times (0,T t_{end}) 
\]
where
\[
\hat g( x, \hat t\,) = \frac{1}{M} g( x, T^{-1} \hat t \,).
\] 
To replace the boundary condition~\eqref{eq:bc2}, we need to replace
$C$ by $\hat C$ which is appropriate for the rescaled parameters,
namely
\begin{align*}
\hat C = 
\hat \rho  \bb{n_x & n_y \\ n_y & -n_x}  \bb{\hat c_p  & 0 \\ 0 & \hat c_s}  \bb{n_x & n_y \\ n_y & -n_x} 
\end{align*}
where $\hat c_s = \sqrt{\hat \mu/\hat \rho} = c_s / T$ and
$\hat c_p = \sqrt{ (\hat \lambda + 2\hat \mu )/ \hat \rho} = c_p/T$. Hence 
$
\hat C = M^{-1} C
$
and boundary condition~\eqref{eq:bc2} is replaced by 
\[
\hat \sigma n + \hat C v  = 0  \qquad \text{ on } \Gamma_1 \times (0,Tt_{end}). 
\]


\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
