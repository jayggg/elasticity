""" Elastodynamic waves in a flume:

The geometry parameters are given to us in (X,Y) coordinates,
where the flume surface is alined with the X-axis.

We construct and solve the problem in th
physical (x,t) real coordinate system.

1. Setting:
1.a. Material parameters: density and lame parameters
1.b. Scaling factors :    time and mass
1.c. Geometry parameters: mesh size, depths and angles in X, Y

2. Scaling the paramets of the problem.

3. Set boundary sources, (The ficticial source is
   given in the scaled parameters)

4. Solving the mixed finite element formulation, by imposing weak
symmetry of the stress tensor and using Backward-Euler.

"""

import ngsolve as ng
from ngsolve import sin, cos, tan, x, y
from ngsolve import InnerProduct, div
from netgen.geom2d import SplineGeometry
from math import pi
from ctypes import CDLL


mylngs = CDLL("../../libelas.so")
ng.ngsglobals.msg_level = 1


# ===== Setting problem parameters =========

# Physical Material parameters:
p_rho = 2000  # Density in  kg/m^3
#      [mat0, mat1, mat2, mat3]
p_cp = [220, 900, 1700, 1700]  # P-wave velocity in m/s
p_cs = [150, 600, 600, 1200]   # S-wave velocity in m/s


# Scaling factors:
T = 1000  # scaling time
M = 10e6  # scaling mass


# Parameters of the geometry in David George's coordinate system:

h = 7    # mesh size

# Depth by layer: (Y-direction)
dp0 = 2    # Soil             : 0-2m
dp1 = 6    # Weathered bedrock: 2-6m
dp2 = 9    # Bedrock 1        : 6-9m
dp3 = 15   # aux layer        : 9-(dp3)m

# Length of the flume: (X-direction)
dist = 55    # Auxiliar length of left extension : -55m < X < 0m
dflume = 78  # Length of the physical flume      :   0m < X < 78m
dist2 = 27   # Auxiliar length of right extension:  78m < X < 78 + (dist2)m

theta = -31.0 * pi / 180  # Angle of inclination
alpha = -3.0 * pi / 180  # Angle to the right


k = 3    # polynomial order

# ========= Defining the domain and change of coordinate system ===============


def rotate(X, Y):
    """
    From David George's coordinates: X, Y
    to   Netgen coordinates: x, y
    """
    xx = cos(theta) * X - sin(theta) * Y
    yy = sin(theta) * X + cos(theta) * Y
    return (xx, yy)


def invrot(xx, yy):
    """
    From  Netgen coordinates: x, y
    to    David George's coordinates: X, Y

    """
    X = cos(theta) * xx + sin(theta) * yy
    Y = -sin(theta) * xx + cos(theta) * yy
    return (X, Y)


def Domain(geom, h):
    #  Domain 0:
    fh = (0, 0)    # Origin of the system

    # Left_bdry_dom0  [fa, fb]
    fa = rotate(-dist, 0)  # topleft_dom1
    fb = rotate(-dist + dp0 * tan(-theta), -dp0)  # botleft_dom0

    # Points in the curve [fg, ff, fe]
    fg = rotate(dflume, 0)
    # ff point
    ff_hyp = 10 * sin((-theta + alpha) / 2) / sin(pi / 2 + (theta - alpha) / 4)
    # law of sines
    ff_x = cos((-theta + alpha) / 4) * ff_hyp
    ff_y = sin((-theta + alpha) / 4) * ff_hyp
    ff = rotate(dflume + ff_x, ff_y)
    # fe point
    fe_hyp = 10 * sin(-theta + alpha) / sin(pi / 2 + (theta - alpha) / 2)
    # law of sines
    fe_x = cos((-theta + alpha) / 2) * fe_hyp
    fe_y = sin((-theta + alpha) / 2) * fe_hyp
    fe = rotate(dflume + fe_x, fe_y)

    # Right_bdry_dom0 [fd,fc]
    fd_x = dflume + dist2 + fe_x
    fd_y = dist2 * tan(-theta + alpha) + fe_y

    fc = rotate(fd_x + tan(-theta) * (dist2 *
                                      tan(-theta + alpha) + fe_y +
                                      dp0), -dp0)  # botright_dom0
    fd = rotate(fd_x, fd_y)  # topright_dom0

    # Domain 1:
    fr1 = rotate(fd_x + tan(-theta) *
                 (dist2 * tan(-theta + alpha) +
                  fe_y + dp1), -dp1)  # botright_dom_1
    fl1 = rotate(-dist +
                 dp1 * tan(-theta), -dp1)  # botleft_dom1

    # Domain 2:
    fr2 = rotate(fd_x + tan(-theta) *
                 (dist2 * tan(-theta + alpha) +
                  fe_y + dp2), -dp2)  # botright_dom_2
    fl2 = rotate(-dist + dp2 * tan(-theta), -dp2)  # botleft_dom2

    # Domain 3:
    fr3 = rotate(fd_x + tan(-theta) *
                 (dist2 * tan(-theta + alpha) +
                  fe_y + dp3), -dp3)  # botright_dom_3
    fl3 = (fa[0], fr3[1])  # botleft_dom3

    # Appending points:
    pnts1 = [fa, fb, fc, fd,  # topleft-botleft-botright-topright
             fe, ff, fg,      # top bdry (curve)
             fh]              # origin

    dm0 = [geom.AppendPoint(*p) for p in pnts1]
    dm1 = [geom.AppendPoint(*fl1), geom.AppendPoint(*fr1)]
    dm2 = [geom.AppendPoint(*fl2), geom.AppendPoint(*fr2)]
    dm3 = [geom.AppendPoint(*fl3), geom.AppendPoint(*fr3)]

    mat0a = [(dm0[0], dm0[1], "lft0", 1, 0, h),
             (dm0[1], dm0[2], "bot0", 1, 2, h),
             (dm0[2], dm0[3], "rgt0", 1, 0, h),
             (dm0[3], dm0[4], "rtop", 1, 0, h / 4),
             (dm0[6], dm0[7], "flume", 1, 0, h / 8),
             (dm0[7], dm0[0], "ltop", 1, 0, h / 8)]
    geom.SetDomainMaxH(1, h / 4)
    # mat0b correspond to curv, and it is appended below

    mat1 = [(dm0[1], dm1[0], "lft1", 2, 0),
            (dm1[0], dm1[1], "bot1", 2, 3),
            (dm1[1], dm0[2], "rgt1", 2, 0)]

    mat2 = [(dm1[0], dm2[0], "lft2", 3, 0),
            (dm2[0], dm2[1], "bot2", 3, 4),
            (dm2[1], dm1[1], "rgt2", 3, 0)]

    mat3 = [(dm2[0], dm3[0], "lft3", 4, 0),
            (dm3[0], dm3[1], "bot3", 4, 0),
            (dm3[1], dm2[1], "rgt3", 4, 0)]

    # Mat 0 : Soil
    for p0, p1, bn, ml, mr, mh in mat0a:
        geom.Append(["line", p0, p1], bc=bn,
                    leftdomain=ml, rightdomain=mr, maxh=mh)
    geom.Append(["spline3", dm0[4], dm0[5], dm0[6]],
                bc="curv", leftdomain=1,
                rightdomain=0, maxh=h / 4)  # the curve

    # Mat 1 : Weathered bedrock

    for p0, p1, bn, ml, mr in mat1:
        geom.Append(["line", p0, p1], bc=bn, leftdomain=ml, rightdomain=mr,
                    maxh=h / 4)
    # Mat 2 : Bedrock 1
    for p0, p1, bn, ml, mr in mat2:
        geom.Append(["line", p0, p1], bc=bn, leftdomain=ml, rightdomain=mr,
                    maxh=h / 4)
    # Mat 3 : Bedrock 2
    for p0, p1, bn, ml, mr in mat3:
        geom.Append(["line", p0, p1], bc=bn, leftdomain=ml, rightdomain=mr)

    return (geom)


def MakeMesh(h):
    geometry = SplineGeometry()
    geometry = Domain(geometry, h)
    return ng.Mesh(geometry.GenerateMesh(maxh=h))


mesh = MakeMesh(h)


# ---------------------------------------------------------------

def scaled_lame(p_rho, p_cp, p_cs):
    """ Returns  the scaled density and lam'e parameters
    from  the physical density rho, and wave speeds cp and cs

    hat mu = mu / (M * T)
    hat lam = lam / (M * T)
    hat rho = rho / (M / T)
    """
    # Physical mu and lambda lam'e parameters
    p_mu = [p_rho * p_cs[0] * p_cs[0],  # mat0
            p_rho * p_cs[1] * p_cs[1],  # mat1
            p_rho * p_cs[2] * p_cs[2],  # mat2
            p_rho * p_cs[3] * p_cs[3]]  # mat3

    p_lm = [p_rho * p_cp[0] * p_cp[0] - 2 * p_mu[0],  # mat0
            p_rho * p_cp[1] * p_cp[1] - 2 * p_mu[1],  # mat1
            p_rho * p_cp[2] * p_cp[2] - 2 * p_mu[2],  # mat2
            p_rho * p_cp[3] * p_cp[3] - 2 * p_mu[3]]  # mat3

    # Scaled mu and lambda, and rho parameters:
    s_mu = [(1 / (M * T)) * p_mu[0],
            (1 / (M * T)) * p_mu[1],
            (1 / (M * T)) * p_mu[2],
            (1 / (M * T)) * p_mu[3]]

    s_lm = [(1 / (M * T)) * p_lm[0],
            (1 / (M * T)) * p_lm[1],
            (1 / (M * T)) * p_lm[2],
            (1 / (M * T)) * p_lm[3]]

    s_rho = p_rho * T / M

    return s_rho, s_lm, s_mu


# ==========  Scaled parameters =====================

rho, lm, mu = scaled_lame(p_rho, p_cp, p_cs)

# Computing cp, cs :

cp = [ng.sqrt((lm[0] + 2 * mu[0]) / rho),
      ng.sqrt((lm[1] + 2 * mu[1]) / rho),
      ng.sqrt((lm[2] + 2 * mu[2]) / rho),
      ng.sqrt((lm[3] + 2 * mu[3]) / rho)]

cs = [ng.sqrt(mu[0] / rho),
      ng.sqrt(mu[1] / rho),
      ng.sqrt(mu[2] / rho),
      ng.sqrt(mu[3] / rho)]

lam = ng.CoefficientFunction([lm[0], lm[1], lm[2], lm[3]])
mu = ng.CoefficientFunction([mu[0], mu[1], mu[2], mu[3]])


# Conservative time step size ∆t
delta = 0.9 * h / (k * k *
                   max(cp[0], cs[0], cp[1], cs[1],
                       cp[2], cs[2], cp[3], cs[3]))

# ================= Defining parts of boundary ================

absorbing = 'lft0|lft1|lft2|lft3|bot3|rgt3|rgt2|rgt1|rgt0'
flume_dirichlet = 'rtop|curv|flume|ltop'


# To define values of functions in the boundary
bdrymat = {'lft0': 0, 'bot0': 0, 'rgt0': 0,
           'rtop': 0, 'curv': 0, 'flume': 0, 'ltop': 0,
           'lft1': 1, 'bot1': 1, 'rgt1': 1,
           'lft2': 2, 'bot2': 2, 'rgt2': 2,
           'lft3': 3, 'bot3': 3, 'rgt3': 3}

cpb = ng.CoefficientFunction([cp[bdrymat[b]] for b in mesh.GetBoundaries()])
csb = ng.CoefficientFunction([cs[bdrymat[b]] for b in mesh.GetBoundaries()])
lmb = ng.CoefficientFunction([lm[bdrymat[b]] for b in mesh.GetBoundaries()])

n = ng.specialcf.normal(mesh.dim)


# ================== Boundary Source Force ====================
aa = 31
bb = 20


def G(t):
    (X, Y) = invrot(x, y)  # From netgen to the x=0
    U = 10e6 * ng.exp(-50 * t *
                      t) * ng.exp(-aa *
                                  ((X - bb) - cpb * t) *
                                  ((X - bb) - cpb * t))

    sbdry = ng.CoefficientFunction((0, 0, 0, -U), dims=(2, 2))
    return sbdry

# ==============  Main Driver =================================


def Atensor(sig):
    """ Returns the compliance  A tensor :

     Atensor( sig) = (1/ 2mu) sig - lam/(4*(mu + lam)) tr(sig)*I

    """
    a1 = 0.5 / mu
    a2 = lam / (4.0 * mu * (lam + mu))
    tr_sig = sig[0] + sig[3]
    Asig = (a1 * sig[0] - a2 * (tr_sig), a1 * sig[1],
            a1 * sig[2], a1 * sig[3] - a2 * (tr_sig))
    return ng.CoefficientFunction(Asig, dims=(2, 2))


def B(ss):
    """ Return the action the matrix B (in the 1st order absorbing b.c.)
    on a stress matrix ss.  The b.c. is written e.g. in Bamberger et al
    as

    sigma n + c v = 0

    where c is the matrix c = rho * N * diag(cp,cs) * N and
    N = [ nx ny; ny -nx]. We invert c to get B.
    """
    c = [[rho * (n[0] * n[0] * cpb + n[1] * n[1] * csb),
          rho * n[0] * n[1] * (cpb - csb)],
         [rho * n[0] * n[1] * (cpb - csb),
          rho * (n[1] * n[1] * cpb + n[0] * n[0] * csb)]]

    cdet = c[0][0] * c[1][1] - c[0][1] * c[1][0]

    # Compute B =  inv(c)
    B = [[c[1][1] / cdet, -c[0][1] / cdet],
         [-c[1][0] / cdet, c[0][0] / cdet]]
    Bs = (B[0][0] * ss[0] + B[0][1] * ss[2],
          B[0][0] * ss[1] + B[0][1] * ss[3],
          B[1][0] * ss[0] + B[1][1] * ss[2],
          B[1][0] * ss[1] + B[1][1] * ss[3])

    # Scaling the inv(\hat C)

    Bs = (Bs[0], Bs[1],
          Bs[2], Bs[3])

    return ng.CoefficientFunction(Bs, dims=(2, 2))


# Initial conditions:
sinit = ng.CoefficientFunction((0.0, 0.0, 0.0, 0.0), dims=(2, 2))
vinit = ng.CoefficientFunction((0.0, 0.0))
rinit = ng.CoefficientFunction(0.0)

# ================  Solving the problem =========================

S = ng.FESpace("ggfes", mesh, order=k, dirichlet=flume_dirichlet)
V = ng.VectorL2(mesh, order=k - 1)      # Velocity space = L2 x L2
A = ng.L2(mesh, order=k)                # Space of rotations (skw sym mat)
X = ng.FESpace([S, V, A])

sig, v, r = X.TrialFunction()
tau, w, q = X.TestFunction()

u = ng.GridFunction(X)
u.components[1].Set(vinit)
u.components[2].Set(rinit)


ng.Draw(u.components[0][0] + u.components[0][3], mesh, "trace(stress)",
        autoscale=False,
        min=-5,
        max=1)


"""
Formulation:  Weakly symmetric mixed method:

 (Atensor(sig)',tau) + (v, div tau) + <B sig.n,tau.n> + (Skw r',tau) = 0
 (div sig, w) - (v', w)   = -(F,w)
 (sig', Skw q)   = 0

Discretization:   Backward Euler in matrix form:

 M * u(t+∆t) = S * u(t) + RHS(t)

      [    A      ∆t B^T    D^T   ]
  M = [  ∆t B       -C       0    ]
      [    D         0       0    ]

      [    A         0       D^T  ]
  S = [    0        -C       0    ]
      [    D         0       0    ]

  RHS(t) = ∆t <G(t+∆t)n, tau n> -  ∆t (F(t+∆t), w)

"""

divtau = tau.Deriv()
divsig = sig.Deriv()


# ==========  Make the M matrix ================
m = ng.BilinearForm(X)
m += ng.SymbolicBFI(delta * InnerProduct(B(sig.Trace()), tau.Trace()),
                    ng.BND, definedon=mesh.Boundaries(absorbing))
m += ng.SymbolicBFI(InnerProduct(Atensor(sig), tau))     # A, vol part
m += ng.SymbolicBFI(delta * v * div(tau))    # ∆t B^T
m += ng.SymbolicBFI(r * (tau[0, 1] - tau[1, 0]))  # D^T
m += ng.SymbolicBFI(delta * div(sig) * w)    # ∆t B
m += ng.SymbolicBFI(-rho * (v * w))       # -C
m += ng.SymbolicBFI((sig[0, 1] - sig[1, 0]) * q)   # D

m.Assemble()
inv_M = m.mat.Inverse(X.FreeDofs())

# ============ Make the S matrix ==================
s = ng.BilinearForm(X, nonassemble=True)
s += ng.SymbolicBFI(InnerProduct(Atensor(sig), tau))  # A
s += ng.SymbolicBFI(r * (tau[0, 1] -  tau[1, 0]))  # D^T
s += ng.SymbolicBFI(-rho * (v * w))    # -C
s += ng.SymbolicBFI((sig[0, 1] - sig[1, 0]) * q)      # D

f = ng.LinearForm(X)
f.Assemble()
ff = f.vec
t = 0
tend = 40
itcnt = 0


# =========== Solving in time ========================

with ng.TaskManager():
    while t < tend:
        s.Apply(u.vec, ff)  # write S*u into ff
        u1 = ng.GridFunction(X)
        u1.components[0].Set(G(t + delta), ng.BND,
                             definedon=mesh.Boundaries(flume_dirichlet))
        ff.data -= m.mat * u1.vec
        u.vec.data = inv_M * ff
        u.vec.data += u1.vec

        if itcnt % 50 == 0:
            solfilename = 'solutions/s_vx_vy_a_t%5.3f' % t + '.sol'
            vtk = ng.VTKOutput(ma=mesh, coefs=[u.components[1]],
                               names=["sol"], filename=solfilename,
                               subdivision=3)
            # Exporting the results:
            vtk.Do()

        itcnt += 1
        t += delta
        t_real = t / T  # Real time
        print('t=%g' % t_real)
        ng.Redraw(blocking=True)
