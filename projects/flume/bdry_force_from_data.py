import ngsolve as ngs
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation


class BoundaryForceData:

    """A class to convert force data tables into NGSpy coefficient
       functions through B-splines.
    """

    def __init__(self, normalforcefile, tangentialforcefile):
        """
        This constructor sets the following DATA MEMBERS:
        _nx = number of x values.
        _nt = number of t values.
        _xvals = array of x-coordinates in David's coord system.
        _tvals = array of time values in data set,
        _fnvals[i][j] = normal force at time _tvals[i] at point _xvals[j].
        _ftvals[i][j] = tangential force at time tvals[i] at point xvals[j].
        _timeseries_fn[j] = a spline function (of time t) that gives the
                            normal force  at the point xvals[j] for any t.
        _timeseries_ft[j] = like above, but gives tangential force.
        """

        tlist, xlist, fnlistlist = self._read_data_file(normalforcefile)
        tlist2, xlist2, ftlistlist = self._read_data_file(tangentialforcefile)

        self._nx = len(xlist)
        self._nt = len(tlist)
        self._xvals = np.array(xlist)
        self._tvals = np.array(tlist)

        xvals2 = np.array(xlist2)
        tvals2 = np.array(tlist2)
        if (max(abs(self._tvals - tvals2)) > 1.e-14) or \
           (max(abs(self._xvals - xvals2)) > 1.e-14):
            raise ValueError('Incompatible x or t values in normal ' +
                             'and tangential force data files')
        if min(self._tvals) < 0:
            raise ValueError('Need non-negative time values')

        self._fnvals = np.array(fnlistlist)
        self._ftvals = np.array(ftlistlist)
        self._timeseries_fn, self._timeseries_ft = self._time_series_list()

    def Fn(self, t):
        """
        Fn(t)(x) gives the normal force at time t and position x.
        """
        fv = [f(t) for f in self._timeseries_fn]
        xv = [xx for xx in self._xvals]
        return ngs.BSpline(2, [min(self._xvals)] + xv,  fv)

    def Ft(self, t):
        """
        Ft(t)(x) gives tangential force at time t and position x.
        """
        fv = [f(t) for f in self._timeseries_ft]
        xv = [xx for xx in self._xvals]
        return ngs.BSpline(2, [min(self._xvals)] + xv, fv)

    def plotFn(self, i):
        """
        Plot the original normal force data values together with the
        B-spline curve at time _tvals[i] (the i-th time step given in
        the data file).
        """
        t = self._tvals[i]
        plt.xlabel("x")
        plt.ylabel("f")
        plt.title('Normal force')
        xv = [xx for xx in self._xvals]
        fv = self._fnvals[i]
        plt.plot(xv, fv, '-oy', label='original values')
        xtot = max(self._xvals) - min(self._xvals)
        xs = np.linspace(min(self._xvals) - 0.1*xtot,
                         max(self._xvals) + 0.1*xtot, 400)
        bv = [self.Fn(t)(xx) for xx in xs]
        plt.plot(xs, bv, '-', label='spline values')
        plt.legend()
        plt.show()

    def plotFt(self, i):
        """
        Plot the original tangential force data values together with the
        B-spline curve at time _tvals[i] (the i-th time step given in
        the data file).
        """
        t = self._tvals[i]
        plt.xlabel("x")
        plt.ylabel("f")
        plt.title('Tangential force')
        xv = [xx for xx in self._xvals]
        fv = self._ftvals[i]
        plt.plot(xv, fv, '-oy', label='original values')
        xtot = max(self._xvals) - min(self._xvals)
        xs = np.linspace(min(self._xvals) - 0.1*xtot,
                         max(self._xvals) + 0.1*xtot, 400)
        bv = [self.Ft(t)(xx) for xx in xs]
        plt.plot(xs, bv, '-', label='spline values')
        plt.legend()
        plt.show()

    def animFn(self, nframes=300):
        """
        Animate the computed spline representing normal force.
        """
        fig, ax = plt.subplots()
        xtot = max(self._xvals) - min(self._xvals)
        xx = np.linspace(min(self._xvals) - 0.1*xtot,
                         max(self._xvals) + 0.1*xtot, 400)
        tmin = min(self._tvals)
        tmax = max(self._tvals)
        ff = [self.Fn(0)(x) for x in xx]
        line, = ax.plot(xx, ff)
        line.set_ydata(np.ma.array(xx, mask=True))

        def init():
            line.set_ydata(np.ma.array(xx, mask=True))
            return line,

        def update(frame):
            ff = [self.Fn(frame)(x) for x in xx]
            line.set_ydata(ff)
            return line,

        a = animation.FuncAnimation(fig, update,
                                    frames=np.linspace(tmin, tmax, nframes),
                                    init_func=init, blit=True)
        a.repeat_delay = 200
        plt.show()

    def animFt(self, nframes=300):
        """
        Animate the computed spline representing tangential force.
        """

        fig, ax = plt.subplots()
        xtot = max(self._xvals) - min(self._xvals)
        xx = np.linspace(min(self._xvals) - 0.1*xtot,
                         max(self._xvals) + 0.1*xtot, 400)
        tmin = min(self._tvals)
        tmax = max(self._tvals)
        ff = [self.Ft(0)(x) for x in xx]
        line, = ax.plot(xx, ff)
        line.set_ydata(np.ma.array(xx, mask=True))

        def init():
            line.set_ydata(np.ma.array(xx, mask=True))
            ax.set_ylim(np.min(self._ftvals), np.max(self._ftvals))
            return line,

        def update(frame):
            ff = [self.Ft(frame)(x) for x in xx]
            line.set_ydata(ff)
            return line,

        a = animation.FuncAnimation(fig, update,
                                    frames=np.linspace(tmin, tmax, nframes),
                                    init_func=init, blit=True)
        a.repeat_delay = 200
        plt.show()

    def _read_data_file(self, filename):
        """
        Read data file assuming the format specified by David and
        output a 2D table of force values for each x and t value.

        OUTPUTS: (tvals, xvals, fvals),  where
        fvals[i][j] gives force value at time tvals[i] at point xvals[j].
        """

        forcedatafile = open(filename, 'r')
        flines = forcedatafile.readlines()
        # Initial line is assumed to be a comment row.
        xvaluesrow = flines[1]              # 2nd line contains x-values.
        xvals = xvaluesrow.split()[1:]
        xvals = [float(xx) for xx in xvals]
        tvals = []
        fvals = []

        for line in flines[2:]:
            row = line.split()
            # Time values appear first on each line:
            tvals.append(float(row[0]))
            # Remaining values are force values:
            f = []
            for xv in row[1:]:
                f.append(float(xv))
            fvals.append(f)

        return (tvals, xvals, fvals)

    def _time_series_list(self):
        """Make a list of timeseries-splines such that timeseries[j] is a
           function of time t that gives the force at the location
           xvals[j] for all t.
        """

        timeseries_fn = []
        timeseries_ft = []
        for j in range(self._nx):
            nforce_at_xj = ngs.BSpline(2, [0] + list(self._tvals),
                                       list(self._fnvals[:, j]))
            timeseries_fn.append(nforce_at_xj)
            tforce_at_xj = ngs.BSpline(2, [0] + list(self._tvals),
                                       list(self._ftvals[:, j]))
            timeseries_ft.append(tforce_at_xj)

        return (timeseries_fn, timeseries_ft)


######################################################################

if __name__ == '__main__':

    B = BoundaryForceData('data/NormalForce.txt', 'data/TangentialForce.txt')
    # B.plotFt(100)
    B.animFt()
