// Integrators


#include <fem.hpp>

using namespace ngfem;

#include "GGtet.hpp"
#include "integrator3d.hpp"



namespace ngfem
{
  class HDivMassIntegratorNsym3D;
  class HDivRobinIntegratorNsym3D;
  class HDivDivIntegratorNsym3D;
  
  static RegisterBilinearFormIntegrator<HDivMassIntegratorNsym3D> initmass3("masshdivsigma3", 3, 1);
  static RegisterBilinearFormIntegrator<HDivDivIntegratorNsym3D> initdiv3("divmasshdivsigma3", 3, 1);
  static RegisterBilinearFormIntegrator<HDivRobinIntegratorNsym3D> initrobin3("robinhdivsym3", 3, 1);
	
}

//	/* ******************** Integrator ************************** */
//
