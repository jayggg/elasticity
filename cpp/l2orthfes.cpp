#include <comp.hpp>    
#include "l2orth.hpp"
#include "l2orthfes.hpp"

namespace ngcomp {
  
  l2orthFEspace::l2orthFEspace (shared_ptr<MeshAccess> ama,
				const Flags & flags)
    : FESpace (ama, flags)   {
    
    order = int(flags.GetNumFlag ("order", 1));
    if (order==0) {
      throw Exception("\n *Error: l2orthfes.Order() must be greater than 0 \n"); };
    
    evaluator[VOL] = make_shared<T_DifferentialOperator<DiffOpId<2>>>();
    flux_evaluator[VOL] = make_shared<T_DifferentialOperator<DiffOpGradient<2>>>();
    evaluator[BND] = make_shared<T_DifferentialOperator<DiffOpIdBoundary<2>>>();
    integrator[VOL] = GetIntegrators().CreateBFI("mass", ma->GetDimension(), make_shared<ConstantCoefficientFunction>(1));
  }
  
  
  void l2orthFEspace::Update(LocalHeap & lh)   {
    
    int n_cell = ma->GetNE();  
    int ii = 0;
    
    first_cell_dof.SetSize (n_cell+1);
    for (int i = 0; i < n_cell; i++, ii+=(order+1))
      first_cell_dof[i] = ii;
    first_cell_dof[n_cell] = ii;
    ndof = ii;
  }

    
  void l2orthFEspace::GetDofNrs (ElementId ei, Array<int> & dnums) const {
    dnums.SetSize(0);
    if (!DefinedOn (ei) || ei.VB()!=VOL) return;

    int elnr = ei.Nr();
    int first = first_cell_dof[elnr];
    int next  = first_cell_dof[elnr+1];
    for (int i = first; i < next; i++)  dnums.Append (i);   
  }

  
  // const FiniteElement & 
  // l2orthFEspace::GetFE (int elnr, LocalHeap & lh) const  {
    
  //   L2orthTrig * trig = new (lh) L2orthTrig(order);
    
  //   ArrayMem<int, 3> vvnums;	  
  //   ma->GetElVertices(elnr, vvnums); 
  //   trig->SetVertexNumbers(vvnums);         
    
  //   return *trig;
  // }
  
  // const FiniteElement &
  // l2orthFEspace::GetSFE(int selnr, LocalHeap & lh) const {

  //   throw Exception (" GetSFE not implemented in l2orthfes");
  // }

  FiniteElement &
  l2orthFEspace::GetFE (ElementId ei, Allocator & lh) const  {

      Ngs_Element ngel = ma->GetElement(ei);
      ELEMENT_TYPE eltype = ngel.GetType();

      switch (eltype) {

      case ET_TRIG:  {
	
	L2orthTrig * trig = new (lh) L2orthTrig(order);
    
	ArrayMem<int, 3> vvnums;	  
	vvnums = ma->GetElVertices(ei);
	trig->SetVertexNumbers(vvnums);         

	return *trig; 
      }
      default:
	throw Exception ("illegal element in L2OrthFE::GetFE");
      }
  }
  
  static RegisterFESpace<l2orthFEspace> initifes ("l2orthfes");
}
