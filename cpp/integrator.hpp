#ifndef FILE_INTEGRATOR2
#define FILE_INTEGRATOR2

#include <fem.hpp>
#include "GGtrg.hpp"

// T_BDBIntegrator is calling DIffopIHDivSigma
namespace ngfem
{
  
  class HDivMassIntegratorNsym
    :public  T_BDBIntegrator<DiffOpIdHDivSigma, DiagDMat<DiffOpIdHDivSigma::DIM_DMAT>, GGtrg>
  
  {
    typedef  T_BDBIntegrator<DiffOpIdHDivSigma, DiagDMat<DiffOpIdHDivSigma::DIM_DMAT>, GGtrg> BASE;
  public:
    using  T_BDBIntegrator<DiffOpIdHDivSigma, DiagDMat<DiffOpIdHDivSigma::DIM_DMAT>, GGtrg>::T_BDBIntegrator;
    
    virtual string Name () const { return "HDivSigmaMass"; }
  };
  
  
  class HDivRobinIntegratorNsym
    : public T_BDBIntegrator<DiffOpBoundIdHDiv, DiagDMat<4>, GGnrm>
  {
    typedef T_BDBIntegrator<DiffOpBoundIdHDiv, DiagDMat<4>, GGnrm> BASE;
  public:
    using T_BDBIntegrator<DiffOpBoundIdHDiv, DiagDMat<4>, GGnrm>::T_BDBIntegrator;
    
    virtual string Name () const { return "HDivRobin"; }
    virtual bool BoundaryForm () const { return 1; }
  };
  
  
  

  
  
} // namespace ngfem

#endif // FILE_INTEGRATOR2
