#ifndef FILE_GGTET_HPP
#define FILE_GGTET_HPP

#include <fem.hpp>
#include <hdivhofe.hpp>
#include <l2hofe.hpp>
#include "l2orth3d.hpp"



/*
  This is the implementation of a stress element for weakly
  symmetric mixed methods in elasticity.
  
  Definition of the GG element: 
  
  P_k + curl( curl(A_k) * B )
  
  where A_k is skew symmetric matrices whose entries are polynomials
  of degree k and are orthogonal to all poynomials of degree k-1,
  and B is the matrix bubble.
  
*/

namespace ngfem {
  
  class GGtet : public FiniteElement {
  protected:
   
    int vnums[4];
    enum { DIM_STRESS = 9};
    HDivHighOrderFE<ET_TET> hdivelt;
    int hdivdof = hdivelt.GetNDof();
    L2orthTet l2orth;
    bool nobubble;
    
   
  public:

    GGtet (int _k, bool omitbubbles=false )  :l2orth(_k) , hdivelt(_k)  {

      nobubble = omitbubbles;

      if (nobubble){
	ndof  = 3*hdivelt.GetNDof();
      }
      else{
	ndof = 3*hdivelt.GetNDof() + 3*(_k+1)*(_k+2)/2 ;
      }
      
      /* NOTE: hdivelt(0) = lowest order RT
       *       hdivelt(1) = lowest order BDM (vector linear)
       *       hdivelt(2) = BDM2 (vector quadratic)
       * 
       * WARNING: If you ask an hdivelt object for its order, you
       * don't get the order you input!
       * 
       *    hdivelt(k).Order()  returns  k+1 in 2D!
       *    hdivelt(k).Order()  returns  k   in 3D! 
       */

      order = _k+1;
    }
    
    int Order() { return order;}
    virtual ELEMENT_TYPE ElementType() const override { return ET_TET; }
    virtual ~GGtet () { ; };
    
    void SetVertexNumbers(FlatArray<int> & avnums)    {
      hdivelt.SetVertexNumbers(avnums);
      l2orth.SetVertexNumbers(avnums);
      for (int i=0; i<avnums.Size(); i++) vnums[i] = avnums[i];
    }

    
    virtual void CalcShape (const IntegrationPoint & ip,
			    BareSliceMatrix<> shape) const {

      STACK_ARRAY(double, mem, 3*hdivdof);  // get fast stack memory
      FlatMatrix<>  hdivshape(hdivdof,3,&mem[0]);
      hdivelt.CalcShape(ip, hdivshape);
      
      int k = order-1;
      for (int i= 0; i<hdivdof;i++) {
	
	shape(3*i,0) = hdivshape(i,0);
	shape(3*i,1) = hdivshape(i,1);
	shape(3*i,2) = hdivshape(i,2);
	shape(3*i,3) = 0.0;
	shape(3*i,4) = 0.0;
	shape(3*i,5) = 0.0;
	shape(3*i,6) = 0.0;
	shape(3*i,7) = 0.0;
	shape(3*i,8) = 0.0;
		
	shape(3*i+1,0) = 0.0;
	shape(3*i+1,1) = 0.0;
	shape(3*i+1,2) = 0.0;
	shape(3*i+1,3) = shape(3*i,0);  	
	shape(3*i+1,4) = shape(3*i,1);
	shape(3*i+1,5) = shape(3*i,2);
	shape(3*i+1,6) = 0.0;
	shape(3*i+1,7) = 0.0;
	shape(3*i+1,8) = 0.0;
	
	shape(3*i+2,0) = 0.0;
	shape(3*i+2,1) = 0.0;
	shape(3*i+2,2) = 0.0;
	shape(3*i+2,3) = 0.0;
	shape(3*i+2,4) = 0.0;
	shape(3*i+2,5) = 0.0;
	shape(3*i+2,6) = shape(3*i,0);
	shape(3*i+2,7) = shape(3*i,1);
	shape(3*i+2,8) = shape(3*i,2);
    
      }
      if (!nobubble){

	AutoDiffDiff<3> ax[3]; // x,y,z coordinates
	for (int i=0; i<3; i++)
	  ax[i] = AutoDiffDiff<3> (ip(i),i);

	// Calling the L2orthogonal shape functions
	Vector<AutoDiffDiff<3> > S( (k+1)*(k+2)/2 );
	
	l2orth.T_L2orthShape<AutoDiffDiff<3> > (ax,S);
	
		

	/* Make bubbles using part of L2 shape functions:

   For efficiency: We should be computing only those L2 shape
	   functions that are orthogonal to pols of degree k-1, but since
	   we do not know how to do this, we compute all L2 shapes.

	   Given (p1,p2,p3) in L2orth, we want to write
	   in the reference element
 	   
	        [ 0  -p3  p2 ]   [dy p2 + dz p3      -dx p2        -dx p3      ] 
	   curl [ p3  0  -p1 ] = [   -dy p1       dz p3 + dx p1    -dy p3      ]
	        [-p2  p1  0  ]   [   -dz p1          -dz p2      dx p1 + dy p2 ]
	
	   Creating MATRIX BUBBLE:
          
	   Bubble = [ b1 bX bX]
	            [ bX b2 bX] 
            	    [ bX bX b3]

	   Note:     [  [  0.0     0.0    0.0  ]        ]        [ 0 0 0  ]
	         curl[  [-dy p1   dx p1   0.0  ] *Bubble] = curl [ row(a) ]
	             [  [-dz p1    0.0   dx p1 ]        ]        [ row(b) ] 

	   Note:     [  [ dy p2  -dx p2   0.0  ]        ]        [-row(a) ]
	         curl[  [  0.0     0.0    0.0  ] *Bubble] = curl [ 0 0 0  ]
	             [  [  0.0   -dz p2  dy p2 ]        ]        [ row(c) ]
 
	   Note:     [  [ dz p3    0.0  -dx p3 ]        ]        [-row(b) ]
	         curl[  [  0.0    dz p3 -dy p3 ] *Bubble] = curl [-row(c) ]
	             [  [  0.0     0.0    0.0  ]        ]        [ 0 0 0  ]
		  
	*/
   
	AutoDiffDiff<3> b1 = ax[0]*ax[1]*ax[2] + ax[1]*ax[2]*(1-ax[0]-ax[1]-ax[2]);
	AutoDiffDiff<3> b2 = ax[0]*ax[1]*ax[2] + ax[0]*ax[2]*(1-ax[0]-ax[1]-ax[2]);
	AutoDiffDiff<3> b3 = ax[0]*ax[1]*ax[2] + ax[0]*ax[1]*(1-ax[0]-ax[1]-ax[2]);
	AutoDiffDiff<3> b0 = ax[0]*ax[1]*ax[2];
    
	double b1_x  =  b1.DValue(0);   double b2_x = b2.DValue(0);
	double b1_y  =  b1.DValue(1);   double b2_y = b2.DValue(1); 
	double b1_z  =  b1.DValue(2);   double b2_z = b2.DValue(2);
      
	double b3_x = b3.DValue(0);     double b0_x = b0.DValue(0);
	double b3_y = b3.DValue(1);     double b0_y = b0.DValue(1);
	double b3_z = b3.DValue(2);     double b0_z = b0.DValue(2);
	
	double B0 = b0.Value();  double B1 = b1.Value();
	double B2 = b2.Value();  double B3 = b3.Value();
	    

	//===========================================================
	/* Shape bubbles for curl(skew(p1,0,0)* bmatrix) */
	
	int kt  = 3*hdivdof;
	int kt2 = 3*hdivdof + (k+1)*(k+2)/2;
	int kt3 = 3*hdivdof + 2*(k+1)*(k+2)/2;
      
	double S_x = S(k).DValue(0); double S0 = S(k).Value();
	double S_y = S(k).DValue(1);
	double S_z = S(k).DValue(2);
    
	double S_xx = S(k).DDValue(0,0); double S_yy = S(k).DDValue(1,1);
	double S_xy = S(k).DDValue(0,1); double S_yz = S(k).DDValue(1,2); 
	double S_xz = S(k).DDValue(0,2); double S_zz = S(k).DDValue(2,2);
    
	
	// shape bubble (i) :  0 < i < k+1 
	for ( int i = 0; i<(k+1)*(k+2)/2; i++){
	  
	  S_x = S(i).DValue(0); S0 = S(i).Value();
	  S_y = S(i).DValue(1);
	  S_z = S(i).DValue(2);
    
	  S_xx = S(i).DDValue(0,0); S_yy = S(i).DDValue(1,1);
	  S_xy = S(i).DDValue(0,1); S_yz = S(i).DDValue(1,2); 
	  S_xz = S(i).DDValue(0,2); S_zz = S(i).DDValue(2,2);
	  
	  // bubble 1:
	  shape(kt + i, 0 ) = 0.0;
	  shape(kt + i, 1 ) = 0.0;
	  shape(kt + i, 2 ) = 0.0;
	  // first row = 0 
	  // 2nd row = curl( row(a) ) 
	  shape(kt+i,3) = -S_yy*B0 -S_y*b0_y+S_xy*B0 +S_x*b0_y; // dy(a3)
	  shape(kt+i,3)+=  S_yz*B0 +S_y*b0_z-S_xz*B2 -S_x*b2_z; //-dz(a2)
	  shape(kt+i,4) = -S_yz*B1 -S_y*b1_z+S_xz*B0 +S_x*b0_z; // dz(a1)
	  shape(kt+i,4)+=  S_xy*B0 +S_y*b0_x-S_xx*B0 -S_x*b0_x; //-dx(a3)
	  shape(kt+i,5) = -S_xy*B0 -S_y*b0_x+S_xx*B2 +S_x*b2_x; // dx(a2)
	  shape(kt+i,5)+=  S_yy*B1 +S_y*b1_y-S_xy*B0 -S_x*b0_y; //-dy(a1)	  
	  // 3rd row = curl( row(b) ) 
	  shape(kt+i,6) = -S_yz*B0 -S_z*b0_y+S_xy*B3 +S_x*b3_y; // dy(b3)
	  shape(kt+i,6)+=  S_zz*B0 +S_z*b0_z-S_xz*B0 -S_x*b0_z; //-dz(b2)
	  shape(kt+i,7) = -S_zz*B1 -S_z*b1_z+S_xz*B0 +S_x*b0_z; // dz(b1)
	  shape(kt+i,7)+=  S_xz*B0 +S_z*b0_x-S_xx*B3 -S_x*b3_x; //-dx(b3)
	  shape(kt+i,8) = -S_xz*B0 -S_z*b0_x+S_xx*B0 +S_x*b0_x; // dx(b2)
	  shape(kt+i,8)+=  S_yz*B1 +S_z*b1_y-S_xy*B0 -S_x*b0_y; //-dy(b1)
	  

	  // bubble 2:
	  // 1st row = curl( -row(a) ) 
	  shape(kt2+i,0) = -shape(kt+i,3);
	  shape(kt2+i,1) = -shape(kt+i,4);
	  shape(kt2+i,2) = -shape(kt+i,5);
	  // 2nd row = 0
	  shape(kt2+i,3) = 0.0;
	  shape(kt2+i,4) = 0.0;
	  shape(kt2+i,5) = 0.0;
	  // 3rd row = curl( row(c) )
	  shape(kt2+i,6) = -S_yz*B0 -S_z*b0_y+S_yy*B3 +S_y*b3_y; // dy(c3)
	  shape(kt2+i,6)+=  S_zz*B2 +S_z*b2_z-S_yz*B0 -S_y*b0_z; //-dz(c2)
	  shape(kt2+i,7) = -S_zz*B0 -S_z*b0_z+S_yz*B0 +S_y*b0_z; // dz(c1)
	  shape(kt2+i,7)+=  S_xz*B0 +S_z*b0_x-S_xy*B3 -S_y*b3_x; //-dx(c3)
	  shape(kt2+i,8) = -S_xz*B2 -S_z*b2_x+S_xy*B0 +S_y*b0_x; // dx(c2)
	  shape(kt2+i,8)+=  S_yz*B0 +S_z*b0_y-S_yy*B0 -S_y*b0_y; //-dy(c1)

	  // bubble 3:
	  // 1st row = curl( -row(b) )
	  shape(kt3+i,0) = -shape(kt+i,6);
	  shape(kt3+i,1) = -shape(kt+i,7);
	  shape(kt3+i,2) = -shape(kt+i,8);
	  // 2nd row = curl( -row(c) ) 
	  shape(kt3+i,3) = -shape(kt2+i,6);
	  shape(kt3+i,4) = -shape(kt2+i,7);
	  shape(kt3+i,5) = -shape(kt2+i,8);
	  // 3rd row = 0 
	  shape(kt3+i,6) = 0.0;
	  shape(kt3+i,7) = 0.0;
	  shape(kt3+i,8) = 0.0;
           
	}
      } 
    }
    
    void CalcDivShape (const IntegrationPoint & ip,
		       BareSliceMatrix<> divshape) const {

      for (int i=0; i<ndof; i++)
	for (int j=0; j<3; j++) 
	  divshape(i,j) = 0.0;
	

      Vector<> divhdivshape(hdivdof);
      hdivelt.CalcDivShape(ip, divhdivshape);
      for (int i= 0; i<hdivdof;i++){     
	divshape(3*i,0)   = divhdivshape(i);
	divshape(3*i,1)   = 0.0;
	divshape(3*i,2)   = 0.0;

	divshape(3*i+1,0)   = 0.0;	
	divshape(3*i+1,1) = divhdivshape(i);
	divshape(3*i+1,2)   = 0.0;	

	divshape(3*i+2,0) = 0.0;	
	divshape(3*i+2,1) = 0.0;
	divshape(3*i+2,2) = divhdivshape(i);
      }
    }
    
    template <typename MAT> void
    AssignTensor ( const Mat<3> & sigma, int & ii, MAT & shape ) const {
      
      shape ( ii, 0 ) = sigma(0,0);  
      shape ( ii, 1 ) = sigma(0,1);  
      shape ( ii, 2 ) = sigma(0,2);
      shape ( ii, 3 ) = sigma(1,0);
      shape ( ii, 4 ) = sigma(1,1);
      shape ( ii, 5 ) = sigma(1,2);
      shape ( ii, 6 ) = sigma(2,0);
      shape ( ii, 7 ) = sigma(2,1);
      shape ( ii, 8 ) = sigma(2,2);
    }
        
  };


  /// This class represents the normal trace of GG stress elements

  class GGnrm3d : public FiniteElement {
  protected:
    int vnums[3]; // 3 vertex face 
    HDivHighOrderNormalTrig<TrigExtensionMonomial> sn;
  public:
    
    GGnrm3d(int _k) : sn(_k) { order = _k; }
    
    
    virtual ELEMENT_TYPE ElementType() const { return ET_TRIG; }
    
    virtual ~GGnrm3d() {};
    
    void SetVertexNumbers (FlatArray<int> & avnums) {
      sn.SetVertexNumbers(avnums);
      for (int i = 0; i < avnums.Size(); i++)  vnums[i] = avnums[i];
    }
    
    virtual void ComputeNDof () {  ndof = 3*(order + 1)*(order +2)/2; }
    
    
    void CalcShape (const IntegrationPoint & ip,
		    BareSliceMatrix<> shape) const {
		
      STACK_ARRAY(double, mem, (order+1)*(order+2)/2); 
      FlatVector<>  hdivshape((order+1)*(order+2)/2,&mem[0]);      
      sn.CalcShape(ip, hdivshape);
      
      for (int i=0; i<(order+1)*(order+2)/2; i++) { 
	shape(3*i,0) = hdivshape(i);
	shape(3*i,1) = 0;
	shape(3*i,2) = 0;
	
	shape(3*i+1,0) = 0;
	shape(3*i+1,1) = hdivshape(i);
	shape(3*i+1,2) = 0;
	
	shape(3*i+2,0) = 0;
	shape(3*i+2,1) = 0;
	shape(3*i+2,2) = hdivshape(i);	

      }
    }
    
  };
  
  
  
  /* ==============DiffOps for evaluators & integrators: ===================== */

  // DiffOpIdHDivSigma: Calculates Piola mapped shape functions 
  //   This is used by the FE space (GGfes3d) for evaluator[VOL]
  
  class DiffOpIdHDivSigma3D : public DiffOp<DiffOpIdHDivSigma3D>  { 
    
  public:
    
    enum { DIM = 1 };
    enum { DIM_SPACE = 3 };
    enum { DIM_ELEMENT = 3 };
    enum { DIM_DMAT = 9 }; // number of entries of the matrix
    enum { DIFFORDER = 0 };


    // This makes the finite element 3 x 3 matrix-valued:
    
    static Array<int> GetDimensions() { return Array<int> ({3, 3}); }


    // This is used by FE space's evaluator[VOL]:
    
    template <typename FEL, typename SIP, typename MAT> static void
    GenerateMatrix (const FEL & bfel, const SIP & sip,
  		    MAT & mat, LocalHeap & lh) {
     
      HeapReset hr(lh);
      const GGtet & fel = dynamic_cast<const GGtet&> (bfel);
      
      int nd = fel.GetNDof();
      
      Mat<3> jac = sip.GetJacobian();
     
      double det = fabs (sip.GetJacobiDet());
      
      FlatMatrix<> shape(nd, DIM_DMAT, lh);
      fel.CalcShape (sip.IP(), shape);
      
      Mat<DIM_DMAT> trans;
      for (int i = 0; i < DIM_DMAT; i++)
  	{
  	  Mat<3> indicator, hm;
  	  indicator = 0.0;
	  
  	  switch (i)
  	    {
  	    case 0: indicator(0,0) = 1.0; break;
  	    case 1: indicator(0,1) = 1.0; break;
  	    case 2: indicator(0,2) = 1.0; break;
  	    case 3: indicator(1,0) = 1.0; break; 
  	    case 4: indicator(1,1) = 1.0; break; 
  	    case 5: indicator(1,2) = 1.0; break; 
  	    case 6: indicator(2,0) = 1.0; break; 
  	    case 7: indicator(2,1) = 1.0; break; 
  	    case 8: indicator(2,2) = 1.0; break; 
  	    }
	  
  	  hm = indicator * Trans(jac); // Piola Transform
  	  hm *= (1.0 / (det));

  	  fel.AssignTensor(hm, i, trans);
  	}
      mat = Trans(trans) * Trans (shape);
    }


    // Should tune Apply (not GenerateMatrix) for better matrix-free
    // performance, but the one below is not tuned ...
    
    template <typename FEL, typename SIP, class TVX, class TVY>  static void
    Apply (const FEL & bfel, const SIP & sip,
    	   const TVX & x, TVY & y, LocalHeap & lh)  {

      HeapReset hr(lh);
      
      typedef typename TVX::TSCAL TSCAL;
      
      Vec<DIM_DMAT,TSCAL> hx;
      
      const GGtet & fel = dynamic_cast<const GGtet&> (bfel);
      
      int nd = fel.GetNDof();
     
      Mat<3> jac = sip.GetJacobian();
      double det = fabs (sip.GetJacobiDet());
      
      FlatMatrix<> shape(nd, DIM_DMAT, lh);
      fel.CalcShape (sip.IP(), shape);
      
      Mat<DIM_DMAT> trans;
      
      for (int i = 0; i < DIM_DMAT; i++)    	{

	Mat<3> indicator, hm, sigma;
	indicator = 0.0;
	
	switch (i)
	  {
	  case 0: indicator(0,0) = 1.0; break; 
	  case 1: indicator(0,1) = 1.0; break;
	  case 2: indicator(0,2) = 1.0; break;
	  case 3: indicator(1,0) = 1.0; break; 
	  case 4: indicator(1,1) = 1.0; break;
	  case 5: indicator(1,2) = 1.0; break;
	  case 6: indicator(2,0) = 1.0; break;
	  case 7: indicator(2,1) = 1.0; break;
	  case 8: indicator(2,2) = 1.0; break;
	  }	
	sigma = indicator * Trans(jac);
	sigma *= (1.0 / (det));
	
	fel.AssignTensor(sigma, i, trans);
      }
      
      hx = Trans(shape) * x;
      y = Trans(trans) * hx;
      
    }
    

  };


  // DiffOpDivHDivSigma: Computes divergence of mapped shape functions 
  //   This is used by the FE space (GGfes3d) for flux_evaluator[VOL]
  
  class DiffOpDivHDivSigma3D : public DiffOp<DiffOpDivHDivSigma3D >
  {
    
  public:
    enum { DIM = 1 };
    enum { DIM_SPACE = 3 };
    enum { DIM_ELEMENT = 3 };
    enum { DIM_DMAT = 3 }; 
    enum { DIFFORDER = 1 };
    enum { DIM_STRESS = 9 }; 
    enum { D = 3};

    
    template <typename FEL, typename SIP, typename MAT>
    static void GenerateMatrix (const FEL & bfel, const SIP & sip,
				MAT & mat, LocalHeap & lh)
    {
      HeapReset hr(lh);
      const GGtet & fel = 
	dynamic_cast<const GGtet&> (bfel);
    
      int nd = fel.GetNDof();
      
      FlatMatrixFixWidth<D> div_shape(nd, lh);
      fel.CalcDivShape (sip.IP(), div_shape);
            
      double det = fabs (sip.GetJacobiDet());
      
      mat = (1/det)* Trans(div_shape);      
    }
    
    // template <typename FEL, typename SIP, class TVX, class TVY>  static void
    // Apply (const FEL & bfel, const SIP & sip,
    // 	   const TVX & x, TVY & y, LocalHeap & lh)
    // {
    //   HeapReset hr(lh);
    //   typedef typename TVX::TSCAL TSCAL;
    //   Vec<DIM_DMAT,TSCAL> hv;
      
    //   const GGtet & fel = dynamic_cast<const GGtet&> (bfel);
    //   int nd = fel.GetNDof();

    //   FlatMatrix<> divshape(nd, DIM_DMAT, lh);
    //   fel.CalcDivShape (sip.IP(), divshape);

    //   hv = Trans(divshape)*x;
    //   y = (1.0/sip.GetJacobiDet()) * hv;
    // }
    
    static void GenerateMatrix (const FiniteElement &,
				const MappedIntegrationPoint<D,D,Complex> & mip,
				SliceMatrix<Complex, ORDERING::ColMajor> & mat,
				LocalHeap & lh)  {
      
      throw Exception ("DiffOpDivHDivSigma3D: GenerateMatrix complex not supported");
    }

    // Allows you to write ‘div(sigma)’ rather than sigma.Deriv()
    static string Name() { return "div"; }

  };
  

  // DiffOpBoundIdHDiv3D: Calculates mapped trace element shape functions.
  //    * Its GenerateMatrix gives values of G n for each shape
  //      function G in the boundary (trace) element GGnrm3d.
  //    * This implementation is analogous to DiffOpIdVecHDivBoundary
  //      for standard Hdiv.
  //    * This is used by the space GGfes3d in its evaluator[BND].
  
  class DiffOpBoundIdHDiv3D : public DiffOp<DiffOpBoundIdHDiv3D>
  {
    
  public:
    enum { DIM = 1 };
    enum { DIM_SPACE = 3 }; 
    enum { DIM_ELEMENT = 2 };
    enum { DIM_DMAT = 9 }; 
    enum { DIFFORDER = 0 };
    enum { D = 3 };

    // This makes the boundary shape functions 3 x 3 matrix-valued:
   
    static Array<int> GetDimensions() { return Array<int> ({3, 3}); }

    
    template <typename FEL, typename SIP, typename MAT>
    static void GenerateMatrix (const FEL & bfel, const SIP & sip,
				MAT & mat, LocalHeap & lh)
    {
      HeapReset hr(lh);
      const GGnrm3d & fel = dynamic_cast<const GGnrm3d&> (bfel);
            
      int nd = fel.GetNDof();
      
      FlatMatrix<> shape(nd,D, lh);  
      fel.CalcShape (sip.IP(), shape);
      
      // scaled normal vector is given by:
      auto scaled_nv = (1.0/sip.GetJacobiDet()) *sip.GetNV();    
      
      for (int j = 0; j < nd; j++ )
	{
	  mat(0, j) =  shape(j,0) * scaled_nv(0);
	  mat(1, j) =  shape(j,0) * scaled_nv(1);
	  mat(2, j) =  shape(j,0) * scaled_nv(2);
	  mat(3, j) =  shape(j,1) * scaled_nv(0);
	  mat(4, j) =  shape(j,1) * scaled_nv(1);
	  mat(5, j) =  shape(j,1) * scaled_nv(2);
	  mat(6, j) =  shape(j,2) * scaled_nv(0);
	  mat(7, j) =  shape(j,2) * scaled_nv(1);
	  mat(8, j) =  shape(j,2) * scaled_nv(2);
	} 
    }
  };
  
/////////////////////////////////////////////////////////////////
  
  
}


#endif
