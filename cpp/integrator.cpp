// Integrators

#include <fem.hpp>

using namespace ngfem;

#include "GGtrg.hpp"
#include "integrator.hpp"


namespace ngfem
{
  class HDivMassIntegratorNsym;
  class HDivRobinIntegratorNsym;
  
  static RegisterBilinearFormIntegrator<HDivMassIntegratorNsym> initmass2("masshdivsigma", 2, 1);
  
  static RegisterBilinearFormIntegrator<HDivRobinIntegratorNsym> initrobin2("robinhdivsym", 2, 1);
	
}

