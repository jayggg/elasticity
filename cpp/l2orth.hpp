#ifndef FILE_L2ORTHTRIG_HPP 
#define FILE_L2ORTHTRIG_HPP

/*
   Space L2orth contains polynomials w in P_{k} such that
   ( w, p) = 0 for all p in P_{k-1} on each element
   here k = order is >=1.
   ----------------------   
   2D:
   n˚ dofs = (k+1)
   order = k 
   -------------------
   3D: 
   n˚ dofs = (k+1)(k+2)/2
   order = k   
*/


using namespace ngfem;

namespace ngfem { 
  
  class L2orthTrig : public ScalarFiniteElement<2>   {   
    int vnums[3];
    int order;
    L2HighOrderFE<ET_TRIG> l2trig;  
    int l2ndof = l2trig.GetNDof(); //(order+1)*(order)/2
    
  public:
                                             //(ndofs,order)
    L2orthTrig (int k): ScalarFiniteElement<2> ((k+1), k),
			l2trig(k) { order = k; }
    
    
    virtual ELEMENT_TYPE ElementType() const override { return ET_TRIG; }
    
    void SetVertexNumbers(FlatArray<int> & avnums)    {
      l2trig.SetVertexNumbers(avnums);
      for (int i=0; i<avnums.Size(); i++) vnums[i] = avnums[i];
    }
    
    
    
    virtual void CalcShape (const IntegrationPoint & ip, 
			    BareSliceVector<> shape) const override {

      Vector<> ashape(l2ndof);
      l2trig.CalcShape(ip, ashape);

      /*  Looking at the pattern of Dubiner shape functions,
	  Paulina extracted L2 shape functions p(i) that are
	  orthogonal to pols of degree k-1 by this formula:
	  
	  shape(0) = l2shape(k)
	  shape(1) = l2shape(2*k)
	  shape(i) = l2shape( (i+1)*k -(i-1)*(i)/2 ), 1<i<k+1.
      */
      
      // shape(0) and shape(1)
      shape(0)= ashape(order);
      shape(1)= ashape(2*order);
      
      for ( int i = 2; i<ndof; i++){
	shape(i) = ashape( -(i-1)*(i)/2 + (i+1)*order);
      }
    }
    
    virtual void CalcDShape (const IntegrationPoint & ip, 
			     BareSliceMatrix<> dshape) const override {
      STACK_ARRAY(double, mem, 2*l2ndof );  // get fast stack memory
      FlatMatrix<> l2dshape(l2ndof,2,&mem[0]);
      l2trig.CalcDShape(ip, l2dshape);
      // dshape(0) and dshape(1)
      dshape(0,0) = l2dshape(order,0);
      dshape(0,1) = l2dshape(order,1);
      dshape(1,0)=  l2dshape(2*order,0);
      dshape(1,1)=  l2dshape(2*order,1);
      // dshape(i) i >2
      for ( int i = 2; i<ndof; i++){
	dshape(i,0) = l2dshape((i+1)*order-(i-1)*(i)/2,0);
	dshape(i,1) = l2dshape((i+1)*order-(i-1)*(i)/2,1);
      }    
      
    }
  };
}

#endif 
