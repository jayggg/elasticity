#ifndef FILE_BDBEQUATIONS3
#define FILE_BDBEQUATIONS3

#include <fem.hpp>
#include "GGtet.hpp"

// T_BDBIntegrator is colling DIffopIHDivSigma
namespace ngfem
{


  class HDivMassIntegratorNsym3D
  //  :public  T_BDBIntegrator<DiffOpIdHDivSigma3D, DiagDMat<DiffOpIdHDivSigma3D::DIM_DMAT>, GGtet>
      :public  T_BDBIntegrator<DiffOpIdHDivSigma3D, DiagDMat<9>, GGtet>

  {
    typedef  T_BDBIntegrator<DiffOpIdHDivSigma3D, DiagDMat<9>, GGtet> BASE;
  public:
    using  T_BDBIntegrator<DiffOpIdHDivSigma3D, DiagDMat<9>, GGtet>::T_BDBIntegrator;
    
    virtual string Name () const { return "HDivSigma-Mass"; }
  };
  
   // do I need  template class DMatCompliance { }; ?
  

  
  class HDivRobinIntegratorNsym3D
    : public T_BDBIntegrator<DiffOpBoundIdHDiv3D, DiagDMat<9>, GGnrm3d>
  {
    typedef T_BDBIntegrator<DiffOpBoundIdHDiv3D, DiagDMat<9>, GGnrm3d> BASE;
  public:
    using T_BDBIntegrator<DiffOpBoundIdHDiv3D, DiagDMat<9>, GGnrm3d>::T_BDBIntegrator;
    
    virtual string Name () const { return "HDivRobin"; }
    virtual bool BoundaryForm () const { return 1; }
  };

  class HDivDivIntegratorNsym3D
    : public T_BDBIntegrator<DiffOpDivHDivSigma3D, DiagDMat<3>, GGtet>
  {
    typedef T_BDBIntegrator<DiffOpDivHDivSigma3D, DiagDMat<3>, GGtet> BASE;
  public:
    using T_BDBIntegrator<DiffOpDivHDivSigma3D, DiagDMat<3>, GGtet>::T_BDBIntegrator;
    
    virtual string Name () const { return "HDivsigma-Div"; }
  };
  

  
  
  
} // namespace ngfem

#endif // FILE_BDBEQUATIONS3
