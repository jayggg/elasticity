#include <comp.hpp>
#include "GGfes3d.hpp"
#include "integrator3d.hpp"

using namespace ngcomp;
using namespace ngfem;


// This space is BDM_k & GGfes3d
// BDM_k has (k+1)*(k+2)/2 degrees of freedom on each face:
//       
//
// and (k-1)(k+1)(k+2)/2 interior degrees of freedom.
//
// total ndofs = 4 * (k+1)*(k+2)/2 + (k-1)(k+1)(k+2)/2 = (k+1)*(k+2)*(k+3)/2
//
// Interior buubles : 3*(k+1)*(k+2)/2
//
// and we have 3 copies of this space

namespace ngcomp  {
  
  GGfes3d::GGfes3d(shared_ptr<MeshAccess> ama, const Flags & flags)  
    : FESpace (ama, flags)   {

    _k = int(flags.GetNumFlag ("order",0)); // reading flag
    nobubbles = flags.GetDefineFlag("nobubbles");
    discont = flags.GetDefineFlag("discontinuous"); 
    
    if (nobubbles)  
      cout << "Constructor of H(div) x H(div) of degree = "
	   << _k << endl;    
    else {
      cout << "Constructor of GG stress space of degree = "
	   << _k + 1 << "  (k = " << _k << ")" << endl;
      if (_k==0) 
       	throw Exception("\n ***Error: GGFESpace._k() must be > 0 \n");
    }
	
    evaluator[VOL] =       // modeled after element.hpp & bdbequations.hpp
      make_shared<T_DifferentialOperator<DiffOpIdHDivSigma3D>>(); 
    evaluator[BND] =
      make_shared<T_DifferentialOperator<DiffOpBoundIdHDiv3D>>();
    flux_evaluator[VOL] =  // to calculate row-wise divergence of gridfunctions 
      make_shared<T_DifferentialOperator<DiffOpDivHDivSigma3D>>(); 
    auto one = make_shared<ConstantCoefficientFunction>(1);
    integrator[VOL] =      // see integrator.hpp
      make_shared<HDivMassIntegratorNsym3D> (one);
    integrator[BND] = make_shared<HDivRobinIntegratorNsym3D> (one);
  }
  
  
  void GGfes3d :: Update(LocalHeap & lh)  {
    FESpace::Update(lh);
    ndof = 0;
    nfaces = ma->GetNFaces();
    n_cell = ma->GetNE();

    int nbubb_dofs = 3*(_k+1)*(_k+2)/2;
    int ncell_dofs  = 3*(_k-1)*(_k+1)*(_k+2)/2;
    int nfaces_dofs =3*4*(_k+1)*(_k+2)/2;
    
    int  ii = 3*nfaces*(_k+1)*(_k+2)/2;    // 3 copies 
   
    /* Counting interior degrees of freedom:

       k=1: 
      
       GG  has interior bubbles  
       BDM x BDM has no interior dofs 

       k>1:  Both have interior dofs.       
    */		

    first_element_dof.SetSize(n_cell+1);

    if (!nobubbles) {
     
      if (_k ==1) {  
    	for (int i = 0; i < n_cell; i++, ii+=nbubb_dofs) // add bubles 
    	  first_element_dof[i] = ii;
	first_element_dof[n_cell] = ii;
      }
    }
    if (_k > 1) { // check with 1.	  			 		   
      
      if (nobubbles) {
	for (int i = 0; i < n_cell; i++, ii+=ncell_dofs) // interior
	  first_element_dof[i] = ii;
	first_element_dof[n_cell] = ii;
      }
      else { // interior + bubbles
	for (int i = 0; i < n_cell; i++, ii+=ncell_dofs+nbubb_dofs)  
	  first_element_dof[i] = ii;
        first_element_dof[n_cell] = ii;
      }	
    }
    
    ndof = ii ;

    
    
    /* Setting coupling types of dofs */

    ctofdof.SetSize(ndof);

    // lowest order edge dofs are wire basket dofs
    ctofdof = WIREBASKET_DOF; 

    // remaining edge dofs are marked interface dofs 
    for (auto facet : Range(nfaces))
      ctofdof[GetFacetDofs(facet)] = INTERFACE_DOF;
    
    // interior dofs are local
    for (auto el : Range (ma->GetNE()))
      ctofdof[GetElementDofs(el)] = LOCAL_DOF;


    /* If discontinuous, then redo dof counts & ctofdofs */
    
    if (discont) {

      int toteltdof;
      ii = 0 ;
      if (nobubbles)
	toteltdof = nfaces_dofs + ncell_dofs;
      else
	toteltdof =  nfaces_dofs + ncell_dofs + nbubb_dofs; // need to be added the bubbles
    
      for (int i = 0; i < n_cell; i++, ii+=toteltdof) 
	first_element_dof[i] = ii;

      ndof = ii;
      first_element_dof[n_cell] = ii;

      ctofdof.SetSize(ndof);
      ctofdof = LOCAL_DOF;

    }
  }

  
  void  GGfes3d :: GetDofNrs (ElementId ei, Array<int> & dnums) const {
    	
    dnums.SetSize(0);
    int elnr = ei.Nr();

    if (discont)  {
      if (ei.VB() == VOL)
	dnums += GetElementDofs (elnr);
      return;
    } 

    if (ei.VB() == VOL ) {

      ArrayMem<int,4> fanums; 
      fanums = ma->GetElFacets(ei);
      int nfaces = ma->GetNFaces();

      // first (lowest order) face dofs 
      for (int i = 0; i < fanums.Size(); i++)
	dnums +=  GetLowFacesDofs(fanums[i]);

      // the higher order edge dofs 
      for (int i = 0; i < fanums.Size(); i++)
	dnums +=  GetFacetDofs(fanums[i]);

      // interior dofs
      if (_k==1) {
	if (!nobubbles)
	  dnums += GetElementDofs (elnr);
      }
      else if (_k>1) 
	dnums += GetElementDofs (elnr);
	
		if (!DefinedOn (ei))
	  dnums = -1;

    }

    if (ei.VB() == BND) { // This is needed to make dirichlet bc work out


      Array<int> fanums;
      fanums = ma->GetElFacets (ei);
 
      for (int i = 0; i < fanums.Size(); i++)
	dnums += GetLowFacesDofs(fanums[i]);

      for (int i = 0; i < fanums.Size(); i++)
	dnums +=  GetFacetDofs(fanums[i]);

      if (!DefinedOn (ei))
	dnums = -1;

    }

    if(ei.VB()==BBND)  dnums.SetSize(0);

  }

  FiniteElement &
  GGfes3d :: GetFE (ElementId ei, Allocator & lh) const {


    if (ei.IsVolume()) {
      
      Ngs_Element ngel = ma->GetElement(ei);
      ELEMENT_TYPE eltype = ngel.GetType();

      switch (eltype) {

      case ET_TET:
	{
	  GGtet * el = new (lh.Alloc(sizeof(GGtet))) GGtet(_k,nobubbles);
	  ArrayMem<int, 4> vvnums; // 4 vertices for a tetrahedra
	  vvnums = ma->GetElVertices(ei);
	  el->SetVertexNumbers(vvnums);         
                       
	  return *el;		
	}
      default:
	throw Exception(string("GetFE: unsupported element ")+
			ElementTopology::GetElementName(ma->GetElType(ei)));
	
      }
    }
    else {
 
      switch (ma->GetElType(ei))  {
	
      case ET_TRIG:
	{

	  int porder; 
	  if (discont) porder = -1;
	  else porder = order;
	  GGnrm3d *fe = new (lh.Alloc(sizeof(GGnrm3d))) GGnrm3d(porder);
	  if (discont) return *fe;
	  ArrayMem<int, 3> vnums; // 3 vertices of triangle
	  vnums = ma->GetElVertices(ei);
	  fe->SetVertexNumbers(vnums);
	  fe->ComputeNDof();
	  
	  return *fe;	
	}
      default:
	throw Exception(string("GetFE: unsupported element ")+
			ElementTopology::GetElementName(ma->GetElType(ei)));
      }
    }
  }

  // /// Return the reference-element
  
  // const FiniteElement & GGfes3d :: GetFE (int elnr, LocalHeap & lh) const {

  //   GGtet * el = new (lh.Alloc(sizeof(GGtet))) GGtet(_k,nobubbles);
  //   ArrayMem<int, 4> vvnums; // 4 vertices for a tetrahedra
  //   ma->GetElVertices(elnr, vvnums); 
  //   el->SetVertexNumbers(vvnums);         
                       
  //   return *el;		
  // }

  // /// Return the reference boundary element
  
  // const FiniteElement & GGfes3d :: GetSFE (int selnr, LocalHeap & lh) const {

  //   GGnrm3d * fe = 0;
  //   int porder; 
  //   if (discont) porder = -1;
  //   else porder = order;
  //   fe = new (lh) GGnrm3d(porder);
  //   if (discont) return *fe;
  //   ArrayMem<int,3> vnums; // 3 vertices of triangle
      
  //   switch (ma->GetSElType(selnr))  {
    
  //   case ET_TRIG:
  //     {   
  // 	ma->GetSElVertices(selnr, vnums);
  // 	fe->SetVertexNumbers(vnums);
  // 	fe->ComputeNDof();
  // 	break;
  //     }
  //   default:
  //     throw Exception(string("GetSFE: unsupported element ")+
  //   		      ElementTopology::GetElementName(ma->GetSElType(selnr)));
  //   }
  //   return *fe;	
  // }
			

  
  static RegisterFESpace<GGfes3d> initifes ("ggfes3d");

} // namespace closing

	
 
