#ifndef FILE_L2ORTHFESPACE3D_HPP
#define FILE_L2ORTHFESPACE3D_HPP

/* 
   Space L2orthfes3d contains polinomial w in P_{k} such that
   
   ( w, p) = 0 for all p in P_{k-1} on each element
   
   here k = order.
*/

using namespace ngcomp;

namespace ngcomp {
  
  class l2orthFEspace3d : public FESpace {
    
    int order;
    int ndof;    
    Array<int> first_cell_dof;
    
  public:
    
    l2orthFEspace3d (shared_ptr<MeshAccess> ama, const Flags & flags);
    
    ~l2orthFEspace3d () {;}

    virtual string GetClassName () const { return "l2orthfes3d"; }
    
    virtual void Update(LocalHeap & lh);

    virtual size_t GetNDof () const { return ndof; }
    
    virtual void GetDofNrs (ElementId ei, Array<int> & dnums) const;
    
    virtual FiniteElement & GetFE (ElementId ei, Allocator & lh) const;
    //virtual const FiniteElement & GetSFE (int selnr, LocalHeap & lh) const;
  };
  
  
}

#endif 
