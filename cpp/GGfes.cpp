#include <comp.hpp>
#include "GGfes.hpp"
#include "integrator.hpp"

using namespace ngcomp;
using namespace ngfem;

namespace ngcomp  {

  
  GGfes::GGfes(shared_ptr<MeshAccess> ama, const Flags & flags)  
    : FESpace (ama, flags)   {

    _k = int(flags.GetNumFlag ("order",0)); // reading flag
    nobubbles = flags.GetDefineFlag("nobubbles");
    discont = flags.GetDefineFlag("discontinuous"); 

    if (nobubbles)  
      cout << "Constructor of H(div) x H(div) of degree = "
	   << _k << endl;    
    else {
      cout << "Constructor of GG stress space of degree = "
	   << _k + 1 << "  (k = " << _k << ")" << endl;
      if (_k==0) 
	throw Exception("\n ***Error: GGFESpace._k() must be > 0 \n");
    }
	
    evaluator[VOL] =        // modeled after element.hpp & bdbequations.hpp
      make_shared<T_DifferentialOperator<DiffOpIdHDivSigma>>();
    evaluator[BND] =        // called when using set on BND 
      make_shared<T_DifferentialOperator<DiffOpBoundIdHDiv>>();
    flux_evaluator[VOL] =   // to calculate row-wise divergence of gridfunctions 
      make_shared<T_DifferentialOperator<DiffOpDivHDivSigma>>();
    
    auto one = make_shared<ConstantCoefficientFunction>(1);
    integrator[VOL] =       // see integrator.hpp
      make_shared<HDivMassIntegratorNsym> (one);
    integrator[BND] = make_shared<HDivRobinIntegratorNsym> (one);

  }


  // void GGfes :: Update(LocalHeap & lh)  {

  //   ndof = 0;
  //   nedges = ma->GetNEdges();
  //   n_cell = ma->GetNE();
    
  //   int  ii = nedges*2*(_k+1);   // total edge dofs

  //   /* Counting interior degrees of freedom:

  //      k=1: 
      
  //      GG  has interior bubbles  
  //      BDM x BDM has no interior dofs 

  //      k>1:  Both have interior dofs.       
  //   */		

  //   first_element_dof.SetSize(n_cell+1);

  //   if (!nobubbles) {
      
  //     if (_k ==1) {  
  // 	for (int i = 0; i < n_cell; i++, ii+=_k+1) 
  // 	  first_element_dof[i] = ii;
  //       first_element_dof[n_cell] = ii;
  //     }
  //   }
  //   if (_k > 1) {	  			 		   
      
  //     if (nobubbles) {
  // 	for (int i = 0; i < n_cell; i++, ii+=2*(_k+1)*(_k-1)) 
  // 	  first_element_dof[i] = ii;
  // 	first_element_dof[n_cell] = ii;
  //     }
  //     else {
	
  // 	for (int i = 0; i < n_cell; i++, ii+=2*(_k+1)*(_k-1)+_k+1) 
  // 	  first_element_dof[i] = ii;
  //       first_element_dof[n_cell] = ii;
  //     }	
  //   }
    
  //   ndof = ii ;

    
  //   /* Setting coupling types of dofs */

  //   ctofdof.SetSize(ndof);

  //   // lowest order edge dofs are wire basket dofs
  //   ctofdof = WIREBASKET_DOF; 

  //   // remaining edge dofs are marked interface dofs 
  //   for (auto facet : Range(nedges))
  //     ctofdof[GetFacetDofs(facet)] = INTERFACE_DOF;
    
  //   // interior dofs are local
  //   for (auto el : Range (ma->GetNE()))
  //     ctofdof[GetElementDofs(el)] = LOCAL_DOF;


  //   /* If discontinuous, then redo dof counts & ctofdofs */
    
  //   if (discont) {

  //     int toteltdof;
  //     ii = 0 ;
  //     if (nobubbles)
  // 	toteltdof = 3*2*(_k+1) + 2*(_k+1)*(_k-1) ;
  //     else
  // 	toteltdof = 3*2*(_k+1) + 2*(_k+1)*(_k-1)+_k+1 ;
    
  //     for (int i = 0; i < n_cell; i++, ii+=toteltdof) 
  // 	first_element_dof[i] = ii;

  //     ndof = ii;
  //     first_element_dof[n_cell] = ii;

  //     ctofdof.SetSize(ndof);
  //     ctofdof = LOCAL_DOF;

  //   }
  // }

  void GGfes :: Update(LocalHeap & lh)  {


    // Set fine_facet (refined meshes have non fine_facet edges which
    // do not contribute to dofs)

    fine_facet.SetSize(ma->GetNFacets());
    fine_facet = false;
    for(auto el : ma->Elements(VOL))
      fine_facet[el.Facets()] = true;

    UpdateDofTables(); 
    UpdateCouplingDofArray();

  }

  void GGfes :: UpdateDofTables() {

    // Set first_wb_dof,  first_facet_dof, first_element_dof

    size_t nfa = ma->GetNFacets();
    size_t nel = ma->GetNE();

    first_wb_dof.SetSize(nfa+1);
    first_facet_dof.SetSize(nfa+1); 
    first_element_dof.SetSize(nel+1); 

    ndof = 0;
    
    // Set first_wb_dof:

    for(auto i : Range(nfa)) {
      
      first_wb_dof[i] = ndof;

      if(!fine_facet[i])
	continue;            // Skip over non fine_facet edges

      ndof += 2;             // Edge wirebasket dofs come from (vector) P_0
    }
    first_wb_dof.Last() = ndof;
    
    // Set first_facet_dof:

    for(auto i : Range(nfa)) {

      // First facet dof is the first interface dof that is not a wirebasket dof.
      
      first_facet_dof[i] = ndof;

      if(!fine_facet[i])
	continue;            // Skip over non fine_facet edges

      ndof += 2*_k;          // Facet dofs come from (vector) twice P_k \ P_0
    }
    first_facet_dof.Last() = ndof;


    if (discont) ndof = 0;   // No wirebasket or facet dofs in the discont case

    
    // Set first_element_dof (interior dofs) :
    
    for(auto i : Range(nel)) {

      ElementId ei(VOL, i);
      first_element_dof[i] = ndof;

      if (!nobubbles) 
	if (_k == 1)

	  // In this case:
	  //     GG  has _k+1 interior bubbles  
	  //     BDM x BDM has no interior dofs 

	  ndof += _k+1;

      if (_k > 1) {

	// In this case:
	//      Both GG and BDM x BDM have _k+1 interior bubbles

	if (nobubbles)
	  ndof += 2*(_k+1)*(_k-1);
	else
	  ndof += 2*(_k+1)*(_k-1) + _k+1;
      }

      if (discont)  // include dofs of this element's facets as element dofs
	
	for (auto f : ma->GetElFacets(ei)) {
	  ndof += first_wb_dof[f+1] - first_wb_dof[f];
	  ndof += first_facet_dof[f+1] - first_facet_dof[f];
	}
      
    }
    first_element_dof.Last() = ndof;    

  }

  void GGfes :: UpdateCouplingDofArray() {

    // Set ctofdof
    
    ctofdof.SetSize(ndof);

    if (discont)  { 
      ctofdof = LOCAL_DOF;
      return;
    } 

    for (auto f : Range(ma->GetNFacets())) {
      if (fine_facet[f]) {
	ctofdof[GetWBEdgeDofs(f)] = WIREBASKET_DOF;
	ctofdof[GetFacetDofs(f)] = INTERFACE_DOF;
      }
    }

    for (auto el : Range (ma->GetNE()))
      ctofdof[GetElementDofs(el)] = LOCAL_DOF;

  }

  
  void  GGfes :: GetDofNrs (ElementId ei, Array<int> & dnums) const {

    // Returns dofs of element number elnr such that 
    //    - lowest order edge dofs are first,
    //    - then the higher oder edge dofs, 
    //    - and then the interior dofs.
			
    dnums.SetSize(0);
    int elnr = ei.Nr();
    
    if (discont)  {
      if (ei.VB() == VOL)
	dnums += GetElementDofs (elnr);
      return;
    }
    
    if (ei.VB() == VOL) {

      ArrayMem<int,3> fanums; 
      fanums = ma->GetElEdges (ei);
      int nedges = ma->GetNEdges();

      // first (lowest order) edge dofs 
      for (int i = 0; i < fanums.Size(); i++)
	dnums +=  GetWBEdgeDofs(fanums[i]);

      // the higher order edge dofs 
      for (int i = 0; i < fanums.Size(); i++)
	dnums +=  GetFacetDofs(fanums[i]);

      // interior dofs
      if (_k==1) {
	if (!nobubbles)
	  dnums += GetElementDofs (elnr);
      }
      else if (_k>1) 
	dnums += GetElementDofs (elnr);
	
	  if (!DefinedOn (ElementId(BND,elnr)))
	dnums = -1;

    }

    if (ei.VB() == BND)  { // This is needed to make dirichlet bc work out

      Array<int> fanums;
      fanums = ma->GetElFacets (ei);

      for (int i = 0; i < fanums.Size(); i++)
	dnums += GetWBEdgeDofs(fanums[i]);

      for (int i = 0; i < fanums.Size(); i++)
	dnums +=  GetFacetDofs(fanums[i]);

      if (!DefinedOn (ElementId(BND,elnr)))
	dnums = -1;

    }

    if(ei.VB()==BBND)  dnums.SetSize(0);

  }
  
  /// Return the reference-element

  FiniteElement & 
  GGfes :: GetFE(ElementId ei, Allocator & lh) const {



    if (ei.IsVolume()) {

      Ngs_Element ngel = ma->GetElement(ei);
      ELEMENT_TYPE eltype = ngel.GetType();

      switch (eltype) {

      case ET_TRIG:
	{	  
	  GGtrg * el = new (lh.Alloc(sizeof(GGtrg))) GGtrg(_k,nobubbles);
	  ArrayMem<int, 3> vvnums;
	  vvnums = ma->GetElVertices(ei);
	  el->SetVertexNumbers(vvnums);         
                             
	  return *el;
	}
      default:
	throw Exception ("illegal element in GGfes::GetFE");
      }
    }
    else {

      GGnrm * fe = 0;
      int porder; 
      if (discont) porder = -1;
      else porder = order;
      fe = new (lh) GGnrm(porder);
      if (discont) return *fe;
      ArrayMem<int,2> vnums;
      
      switch (ma->GetElType(ei))  {

      case ET_SEGM:
	{
	  vnums = ma->GetElVertices(ei);
	  fe->SetVertexNumbers(vnums);
	  fe->ComputeNDof();
	  break;
	}
	
      default:
	throw Exception(string("GetFE: unsupported element ")+
			ElementTopology::GetElementName(ma->GetElType(ei)));
      }
      return *fe;	
    }
  }
  
  // const FiniteElement & GGfes :: GetFE (int elnr, LocalHeap & lh) const {

  //   GGtrg * el = new (lh.Alloc(sizeof(GGtrg))) GGtrg(_k,nobubbles);
  //   ArrayMem<int, 3> vvnums;
  //   ma->GetElVertices(elnr, vvnums); 
  //   el->SetVertexNumbers(vvnums);         
                       
  //   return *el;		
  // }

  // /// Return the reference boundary element
  
  // const FiniteElement & GGfes :: GetSFE (int selnr, LocalHeap & lh) const {

  //   GGnrm * fe = 0;
  //   int porder; 
  //   if (discont) porder = -1;
  //   else porder = order;
  //   fe = new (lh) GGnrm(porder);
  //   if (discont) return *fe;
  //   ArrayMem<int,2> vnums;
      
  //   switch (ma->GetSElType(selnr))  {

  //   case ET_SEGM:

  //     ma->GetSElVertices(selnr, vnums);
  //     fe->SetVertexNumbers(vnums);
  //     fe->ComputeNDof();
  //     break;
      
  //   default:
  //     throw Exception(string("GetSFE: unsupported element ")+
  //   		      ElementTopology::GetElementName(ma->GetSElType(selnr)));
  //   }
  //   return *fe;	
  // }
			

  
  static RegisterFESpace<GGfes> initifes ("ggfes");

} // namespace closing

	
 
