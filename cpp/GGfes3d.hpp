// read hdivsymhofespace.hpp
#include <fem.hpp>
#ifndef FILE_GGFES3d_HPP
#define FILE_GGFES3d_HPP

#include <comp.hpp>
#include "GGtet.hpp"

namespace ngcomp
{

  class GGfes3d : public FESpace
  {
private: 
   
    int _k;
    
    bool nobubbles; 

    /// Hybridized (discontinuous) space or conforming space.
    bool discont;



    int ndof,  nedges, n_cell,  nfaces;   
    Array<int> first_element_dof;
    
  public:

    GGfes3d (shared_ptr<MeshAccess> ama, const Flags & flags);
    virtual ~GGfes3d () {;}

    virtual string GetClassName () const  {return "GGspace";}
    virtual void Update(LocalHeap & lh);
    virtual size_t GetNDof () const { return ndof; }
    
    virtual void GetDofNrs (ElementId ei, Array<int> & dnums) const;
    
    virtual FiniteElement & GetFE (ElementId ei, Allocator & lh) const;
    
    IntRange GetElementDofs (int nr) const {
      return IntRange(first_element_dof[nr],first_element_dof[nr+1]); 
    }
    
    IntRange GetLowFacesDofs(int ednr) const {
      return IntRange(3*(_k+1)*(_k+2)/2*ednr, 3*(_k+1)*(_k+2)/2*ednr+3); 
    }
    
    IntRange GetFacetDofs(int ednr) const {
      return IntRange(3*(_k+1)*(_k+2)/2*ednr+3,
		      3*(_k+1)*(_k+2)/2*ednr + 3*(_k+1)*(_k+2)/2); 
    }

    virtual void GetVertexDofNrs(int vnr, Array<int> & dnums) const {
      dnums.SetSize0();
    }

    virtual void GetFaceDofNrs (int ednr, Array<int> & dnums) const {
      dnums.SetSize0(); 
      if (discont) return; 
      dnums += GetLowFacesDofs(ednr); 
      dnums += GetFacetDofs  (ednr);
    }

    virtual void GetInnerDofNrs (int elnr, Array<int> & dnums) const {
      dnums = GetElementDofs (elnr);
    }

  };

}    

#endif
