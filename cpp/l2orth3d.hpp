#ifndef FILE_L2ORTHTET_HPP 
#define FILE_L2ORTHTET_HPP

/*
   Space L2orth contains polynomials w in P_{k} such that
   ( w, p) = 0 for all p in P_{k-1} on each element
   here k = order is >=1.
   
   Note that we are using a template to obtain the shape 
   functions, (instead of calling them directly), since
   we are interested in playing with 1st and 2nd derivatives
   later in the GG space.
 
   3D: 
   n˚ dofs = (k+1)(k+2)/2
   order = k   
*/

using namespace ngfem;

namespace ngfem { 
  
  class L2orthTet : public ScalarFiniteElement<3> {   
    
    int vnums[4];
    int k;
    L2HighOrderFE<ET_TET> l2tetra;  
    int l2ndof = l2tetra.GetNDof(); //(order+1)*(order+2)*(order+3)/6
    
    
  public:
    
    L2orthTet (int _k): ScalarFiniteElement<3> ((_k+1)*(_k+2)/2, _k),
			l2tetra(_k) { k = _k; }
    
    
    
    virtual ELEMENT_TYPE ElementType() const override { return ET_TET; }
    
    void SetVertexNumbers(FlatArray<int> & avnums) {
      l2tetra.SetVertexNumbers(avnums);
      for (int i=0; i<avnums.Size(); i++) vnums[i] = avnums[i];
    }
    
    
    int Order() { return k ;}
    
    virtual void CalcShape (const IntegrationPoint & ip, 
			    BareSliceVector<> shape) const override{
      
      AutoDiffDiff<3> ax[3]; // x,y,z coordinates

      for (int i=0; i<3; i++)
	ax[i] = AutoDiffDiff<3> (ip(i),i);
      
      //Vector<AutoDiffDiff<3> > l2shape( (k+1)*(k+2)*(k+3) / 6 );
      Vector<AutoDiffDiff<3> > S( (k+1)*(k+2) / 2 );
      
      T_L2orthShape<AutoDiffDiff<3> > (ax,S); 
     
      for( int i = 0; i< (k+1)*(k+2) / 2; i++)
      shape(i) = S(i).Value();
      
  /*    shape(0) = l2shape(k).Value();
      
      // first (k+1) dofs are the same as the 2D problem:
      
      for ( int i = 1; i<k+1; i++){   
	shape(i) = l2shape((i+1)*k-(i-1)*(i)/2 ).Value();
	//cout<< " counting  "<< i << ",  inside  "<< (i+1)*k-(i-1)*(i)/2 << endl;
	
      }
      
      // updating some values : 
      int ll = k*(k+3)/2; // = (k+1)*(k+2)/2 - 1 (it is ok) 
      int w = k;
      int kk = k;
      
      while ( w>0){
	for ( int i = 1; i<w+1; i++){
	  shape( kk +i) = l2shape(ll + i*w- i*(i-1)/2).Value();
	}
	ll = ll + w*(w+1)/2; // the last inside l2shape
	kk = kk+w;
	w = w-1;
      }
    */  
      
      
    }
    
    virtual void CalcDShape (const IntegrationPoint & ip, 
			     BareSliceMatrix<> dshape) const override{

      STACK_ARRAY(double, mem, 3*l2ndof );  // get fast stack memory
      FlatMatrix<> l2dshape(l2ndof,3,&mem[0]);
      l2tetra.CalcDShape(ip, l2dshape);
      
      dshape(0,0) = l2dshape(k,0);
      dshape(0,1) = l2dshape(k,1);
      dshape(0,2) = l2dshape(k,2);
      
      // dshape(i) i >1
      for ( int i = 2; i<ndof; i++){
	dshape(i,0) = l2dshape((i+1)*k-(i-1)*(i)/2,0);
	dshape(i,1) = l2dshape((i+1)*k-(i-1)*(i)/2,1);
	dshape(i,1) = l2dshape((i+1)*k-(i-1)*(i)/2,1);
      }
      
      // updating some values : 
      int ll = k*(k+3)/2; // = (k+1)*(k+2)/2 - 1 (it is ok) 
      int w = k;
      int kk = k;
      
      while ( w>0){
	for ( int i = 1; i<w+1; i++){
	  dshape( kk +i,0)= l2dshape(ll + i*w- i*(i-1)/2,0);
	  dshape( kk +i,1)= l2dshape(ll + i*w- i*(i-1)/2,1);
	  dshape( kk +i,2)= l2dshape(ll + i*w- i*(i-1)/2,2);
	  
	}
	ll = ll + w*(w+1)/2; // the last inside l2shape
	kk = kk+w;
	w = w-1;
      }
      
    }
    
    template<typename Tx>
    void T_L2orthShape(Tx x[], SliceVector<Tx>  shape) const {   
    Vector< Tx > l2shape( (order+1)*(order+2)*(order+3) / 6 );
    T_L2CalcShape< Tx > (x,l2shape);
    
    shape(0) = l2shape(order);
      for ( int i = 1; i<order+1; i++)   
	shape(i) = l2shape((i+1)*order-(i-1)*(i)/2 );
	//cout<< " counting  "<< i << ",  inside  "<< (i+1)*k-(i-1)*(i)/2 << endl;
	  
      // updating some values : 
      int ll = order*(order+3)/2; // = (k+1)*(k+2)/2 - 1 (it is ok) 
      int w = order;
      int kk = order;
      
      while ( w>0){
	for ( int i = 1; i<w+1; i++){
	  shape( kk +i) = l2shape(ll + i*w- i*(i-1)/2);
	}
	ll = ll + w*(w+1)/2; // the last inside l2shape
	kk = kk+w;
	w = w-1;
      }
    
    
    
    }
    
    template<typename Tx>
    void T_L2CalcShape(Tx x[], SliceVector<Tx>  shape) const {   
      Tx lami[4] = { x[0], x[1], x[2], 1-x[0]-x[1]-x[2] };
      unsigned char sort[4] = { 0, 1, 2, 3 };
      
      if (vnums[sort[0]] > vnums[sort[1]]) Swap (sort[0], sort[1]);
      if (vnums[sort[2]] > vnums[sort[3]]) Swap (sort[2], sort[3]);
      if (vnums[sort[0]] > vnums[sort[2]]) Swap (sort[0], sort[2]);
      if (vnums[sort[1]] > vnums[sort[3]]) Swap (sort[1], sort[3]);
      if (vnums[sort[1]] > vnums[sort[2]]) Swap (sort[1], sort[2]);
      
      Tx lamis[4];
      for (int i = 0; i < 4; i++)
	lamis[i] = lami[sort[i]];
      
      size_t ii = 0;
      size_t order = this->order; // = k
      LegendrePolynomial leg;
      JacobiPolynomialAlpha jac1(1);    
      leg.EvalScaled1Assign 
	(order, lamis[2]-lamis[3], lamis[2]+lamis[3],
	 SBLambda ([&](size_t k, Tx polz) LAMBDA_INLINE
		   {
		     //JacobiPolynomialAlpha jac(2*k+1);
		     JacobiPolynomialAlpha jac2(2*k+2);
		     
		     jac1.EvalScaledMult1Assign
		       (order-k, lamis[1]-lamis[2]-lamis[3], 1-lamis[0], polz, 
			SBLambda ([&] (size_t j, Tx polsy) LAMBDA_INLINE
				  {
				    //JacobiPolynomialAlpha jac(2*(j+k)+2);
				    jac2.EvalMult1Assign(order - k - j, 2 * lamis[0] - 1, polsy, 
							 SBLambda([&](size_t j, Tx val)
								  {
								    shape[ii] = val; 
								    ii++;
								  }));
				    jac2.IncAlpha2();
		    
				  }));
		     jac1.IncAlpha2();
		   }));
    }
    
  };    
  
}

#endif 

