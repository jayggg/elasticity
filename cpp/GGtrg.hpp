#ifndef FILE_GGTRG_HPP
#define FILE_GGTRG_HPP

#include <hdivhofe.hpp>
#include "l2hofe.hpp"


/*
  This is the implementation of a stress element for weakly
  symmetric mixed methods in elasticity.
  
  Definition of the GG element: 
  
  P_k + curl( b * curl A_k)
  
  where A_k is skew symmetric matrices whose entries are polynomials
  of degree k and are orthogonal to all poynomials of degree k-1,
  and b is the cubic bubble.
  
*/ 

namespace ngfem {
  
  class GGtrg : public FiniteElement {
  protected:
    
    int vnums[3];
    enum { DIM_STRESS = 4};
    HDivHighOrderFE<ET_TRIG> hdivtrg;
    int hdivdof = hdivtrg.GetNDof();
    
    bool nobubble;
    
  public:
    
    /// Contruct GG element by default. If omitbubble=true, then just
    /// two copies of BDM will be created instead.
    
    GGtrg (int k, bool omitbubbles=false) : hdivtrg(k)  {
      
      
      nobubble = omitbubbles;
      
      if (nobubble)
	ndof  = 2*hdivtrg.GetNDof() ; 
      else
	ndof  = 2*hdivtrg.GetNDof() + k+1;
      
      /* NOTE: hdivtrg(0) = lowest order RT
       *       hdivtrg(1) = lowest order BDM (vector linear)
       *       hdivtrg(2) = BDM2 (vector quadratic)
       * 
       * WARNING: If you ask an hdivtrg object for its order, you
       * don't get the order you input!
       * 
       *       hdivtrg(k).Order()  returns  k+1 !
       */
      
      order = k+1; // k+1, with  bubbles
    }

    int Order() { return order ;}
    
    virtual ELEMENT_TYPE ElementType() const override { return ET_TRIG; }
    virtual ~GGtrg () { ; };
    
    void SetVertexNumbers(FlatArray<int> & avnums)    {
      hdivtrg.SetVertexNumbers(avnums);
      for (int i=0; i<avnums.Size(); i++) vnums[i] = avnums[i];
    }
    
    /// Return "shape" functions at reference element integration point "ip".
    /// Matrix  shape functions are returned as vectors using this ordering:
    ///      M = [ M(0)  M(1) ]
    ///          [ M(2)  M(3) ].
    
    void CalcShape (const IntegrationPoint & ip,
		    BareSliceMatrix<> shape) const {

      STACK_ARRAY(double, mem, 2*hdivdof);  // get fast stack memory
      FlatMatrix<>  halfshape(hdivdof,2,&mem[0]);
      hdivtrg.CalcShape(ip, halfshape);
      int k = order - 1 ;
      
      for (int i=0; i<hdivdof;i++) {
	
	shape(2*i,0) = halfshape(i,0);
	shape(2*i,1) = halfshape(i,1);
	shape(2*i,2) = 0.0;
	shape(2*i,3) = 0.0;
	
	shape(2*i+1,0) = 0.0;  	
	shape(2*i+1,1) = 0.0;
	shape(2*i+1,2) = shape(2*i,0);
	shape(2*i+1,3) = shape(2*i,1);
      }
      
      if (!nobubble) {
	  
	AutoDiffDiff<2> ax[2];  // x, y coordinates
	for (int i=0; i<2; i++)
	  ax[i] = AutoDiffDiff<2> (ip(i),i);
	
	// Pick up all L2 shape functions 
	Vector<AutoDiffDiff<2> >  l2shape( (k+1)*(k+2) / 2 );
	Vector<AutoDiffDiff<2> >  S(k+1);
	S =0.0;
	T_L2CalcShape<AutoDiffDiff<2>> (ax, l2shape); // Dubiner basis 

	// Make bubbles using part of L2 shape functions:
	//     For efficiency: We should be computing only those L2 shape
	//     functions that are orthogonal to pols of degree k-1, but since
	//     we do not know how to do this, we compute all L2 shapes.
	
	AutoDiffDiff<2> b = ax[0]*ax[1]*(1-ax[0]-ax[1]); // cubic bubble
	double B = b.Value();
	double Bx = b.DValue(0);
	double By = b.DValue(1);
	
	/*  Looking at the pattern of Dubiner shape functions,
	    Paulina extracted L2 shape functions p(i) that are
	    orthogonal to pols of degree k-1 by this formula:
	    
	    p(0) = S(k)
	    p(i) = S( (i+1)*k -(i-1)*(i)/2 ), 1<i<k+1.
	    
	    Then put 
	    A = [ 0 p ]
	        [-p 0]
	    and 
	       shape = curl (b curl A).
	*/      
	
	// p(0) : 
	S(0) = l2shape(k);
	for ( int i = 1; i<k+1; i++) 
	  S(i) = l2shape((i+1)*k-(i-1)*(i)/2);
	  
	// p(i) for i>1 :
	for (int i=0; i<k+1;i++) {
	  shape(2*hdivdof+i,0) = S(i).DDValue(1,0)*B + By*S(i).DValue(0);
	  shape(2*hdivdof+i,1) =-S(i).DDValue(0,0)*B - Bx*S(i).DValue(0);
	  shape(2*hdivdof+i,2) = S(i).DDValue(1,1)*B + By*S(i).DValue(1);
	  shape(2*hdivdof+i,3) =-S(i).DDValue(0,1)*B - Bx*S(i).DValue(1);
	}
      }
    }
          

    void CalcDivShape (const IntegrationPoint & ip,
		       FlatMatrixFixWidth<2> divshape) const {

      Vector<> divhalfshape(hdivdof);
      hdivtrg.CalcDivShape(ip, divhalfshape);
      divshape =0.0;      
      for (int i= 0; i<hdivdof;i++) {     
	divshape(2*i,0)   = divhalfshape(i);
	divshape(2*i,1)   = 0.0;         
	divshape(2*i+1,0) = 0.0;
	divshape(2*i+1,1) = divhalfshape(i);
      }
      // since the div of bubbles is 0, not need to calculate. 
    }
			
    template <typename MAT> void
    AssignTensor ( const Mat<2> & sigma, int & ii, MAT & shape ) const {

      shape ( ii, 0 ) = sigma(0,0);  // [ S_0    S_1 ]
      shape ( ii, 1 ) = sigma(0,1);  // [ S_2    S_3 ]
      shape ( ii, 2 ) = sigma(1,0);
      shape ( ii, 3 ) = sigma(1,1);
    }
    
  private:
    
    template<typename Tx>
    void T_L2CalcShape(Tx x[], SliceVector<Tx>  sshape) const     {   
      
      DubinerBasis3::Eval (order-1, x[0], x[1], sshape);  //k = order-1
      
    }
    
    // inline AutoDiffDiff<2> curl2bbl(const AutoDiffDiff<2> & u,
    // 				    const AutoDiffDiff<2> & v) {
    
    //   AutoDiffDiff<2> hv;
    //   hv.Value() = 0.0;
    //   hv.DValue(0) = u.DDValue(1,0)*v.Value() + u.DValue(1)*v.DValue(0);
    //   hv.DValue(1) = -u.DDValue(0,0)*v.Value()- u.DValue(0)*v.DValue(0);
    //   hv.DValue(2) = u.DDValue(1,1)*v.Value() + u.DValue(1)*v.DValue(0);
    //   hv.DValue(3) = -u.DDValue(0,1)*v.Value()- u.DValue(0)*v.DValue(1);
    //   return hv;
    // }
    
  };
  

  class GGnrm : public FiniteElement {
  protected:
    int vnums[2];
    HDivHighOrderNormalSegm<TrigExtensionMonomial> sn;
    
  public:
    
    GGnrm(int k) : sn(k) { order = k;}
    
    virtual ELEMENT_TYPE ElementType() const override { return ET_SEGM; }
    
    virtual ~GGnrm() {};
    
    void SetVertexNumbers (FlatArray<int> & avnums) {
      sn.SetVertexNumbers(avnums);
      for (int i = 0; i < avnums.Size(); i++)  vnums[i] = avnums[i];
    }
    
    virtual void ComputeNDof () {  ndof = 2*(order + 1); }
    
    // int EdgeOrientation (int enr) const {

    //   const EDGE * edges = ElementTopology::GetEdges (this->ElementType());
    //   return (vnums[edges[enr][1]] > vnums[edges[enr][0]]) ? 1 : -1;
    // }
    
    void CalcShape (const IntegrationPoint & ip,
		    SliceMatrix<> shape) const {
      shape = 0.0;
      STACK_ARRAY(double, mem, order+1);
      FlatVector<> halfshape(order+1,&mem[0]);
      sn.CalcShape(ip, halfshape);
      
      for (int i=0; i<order+1; i++) {
	shape(2*i,0) = halfshape(i);
	shape(2*i,1) = 0;
	
	shape(2*i+1,0) = 0;
	shape(2*i+1,1) = halfshape(i);
      }
    }
    
  };
  
  
  // DiffOpIdHDivSigma: Calculates Piola mapped shape functions 
  //   This is used by the FE space for evaluator[VOL]

  class DiffOpIdHDivSigma : public DiffOp<DiffOpIdHDivSigma>  { 

  public:
    
    enum { DIM = 1 };
    enum { DIM_SPACE = 2 };
    enum { DIM_ELEMENT = 2 };
    enum { DIM_DMAT = 4 }; // number of entries of the matrix
    enum { DIFFORDER = 0 };

    // This makes the finite element 2 x 2  matrix-valued in python interface:
    
    static Array<int> GetDimensions() { return Array<int> ({2, 2}); }
    
    /// All DiffOps must have GenerateMatrix to create B in BDB-integrator.
   
    template <typename FEL, typename SIP, typename MAT> static void
    GenerateMatrix (const FEL & bfel, const SIP & sip,
  		    MAT & mat, LocalHeap & lh) {

      // This returns DIM_DMAT x  fel.GetNDof() matrix B in "mat".
      
      const GGtrg & fel = dynamic_cast<const GGtrg&> (bfel);
      
      int nd = fel.GetNDof();
      // Jacobian written as a vector ( 2 x 2):
      //  [DT11 ; DT12 ] = [ dx Tx | dx Ty ]
      //  [DT21 ; DT22 ]   [ dy Tx | dy Ty ]
      Mat<2> jac = sip.GetJacobian(); 
      
      
      double det = fabs (sip.GetJacobiDet());
      
      // bool debug = sip.GetTransformation().GetElementNr() == 62;
      // cout << "diffophdivsigma for el "
      //      <<  sip.GetTransformation().GetElementNr() << endl;
      
      FlatMatrix<> shape(nd, DIM_DMAT, lh);
      // shape are ( ndofs X 4) 
      fel.CalcShape (sip.IP(), shape);
      
      Mat<DIM_DMAT> trans;
      for (int i = 0; i < DIM_DMAT; i++)  {

	Mat<2> indicator, hm, sigma;
	// indicator is a 2x2 matrix with 1 or 0 
	indicator = 0.0;
	
	switch (i)
	  {
	  case 0: indicator(0,0) = 1.0; break;
	  case 1: indicator(0,1) = 1.0; break;
	  case 2: indicator(1,0) = 1.0; break;
	  case 3: indicator(1,1) = 1.0; break; 
	  }
	
	hm = indicator * Trans(jac); // will return:
	// case 0 : [  DT11 ; DT21  \\ 0 ; 0 ] 
	// case 1 : [   DT12 ; DT22  \\ 0 ;  0 ]
	// case 2 : [  0 ; 0 \\ DT11 ; DT21  ] 
	// case 3 : [  0 ; 0   \\ DT12 ; DT22 ]
	hm *= (1.0 / (det));
	
	fel.AssignTensor(hm, i, trans);
	
	// trans ( i, 0 ) = hm(0,0);  
	// trans ( i, 1 ) = hm(0,1);
	// trans ( i, 2 ) = hm(1,0);
	// trans ( i, 3 ) = hm(1,1);
	
      }
      
      // Trans = 
      // [ hm0 (0,0) | hm0 (0,0) | hm0 (0,0) | hm0 (0,0) ]
      // [ hm1 (0,1) | hm1 (0,1) | hm1 (0,1) | hm1 (0,1) ]
      // [ hm2 (1,0) | hm2 (1,0) | hm2 (1,0) | hm2 (1,0) ]
      // [ hm3 (1,1) | hm3 (1,1) | hm3 (1,1) | hm3 (1,1) ]
      // =  ( 1/ det )* 
      //  [ DT11 | DT21 | 0.0 | 0.0 ]
      //  [ DT12 | DT22 | 0.0 | 0.0 ]
      //  [ 0.0  | 0.0  | DT11| DT21] 
      //  [ 0.0  | 0.0  | DT12| DT22]  
      
      // Return B of BDB-integrator in output argument "mat"
      // Piola transform: 
      //      [  4 x 4 ]*[   4 x ndofs  ]    
      mat = Trans(trans) * Trans (shape);
      // 1/det * [DT11 DT12 |   0    0 ] [sigma[0] ]
      //         [DT21 DT22 |   0    0 ] [sigma[1] ]
      //         [  0    0  | DT11 DT12] [sigma[2] ]
      //         [  0    0  | DT21 DT22] [sigma[3] ]  
      // mat = [1/det *  DT* sigma[0:1] ]
      //       [1/det *  DT* sigma[2:3] ]
      
    }
    
    
    /// For better matrix-free performance, we should optimize  Apply
    //  (not yet optmized!)
    
    template <typename FEL, typename SIP, class TVX, class TVY>  static void
    Apply (const FEL & bfel, const SIP & sip,
	   const TVX & x, TVY & y, LocalHeap & lh)  {
      
      typedef typename TVX::TSCAL TSCAL;
      
      Vec<DIM_DMAT,TSCAL> hx;
      
      const GGtrg & fel = dynamic_cast<const GGtrg&> (bfel);
      
      int nd = fel.GetNDof();
      
      Mat<2> jac = sip.GetJacobian();
      double det = fabs (sip.GetJacobiDet());
      
      FlatMatrix<> shape(nd, DIM_DMAT, lh);
      fel.CalcShape (sip.IP(), shape); //CalcShape.
      
      Mat<DIM_DMAT> trans;
      for (int i = 0; i < DIM_DMAT; i++) {
	
	Mat<2> indicator, hmm;
	indicator = 0.0;
	
	switch (i)
	  {
	  case 0: indicator(0,0) = 1.0; break; //Correct order
	  case 1: indicator(0,1) = 1.0; break;
	  case 2: indicator(1,0) = 1.0; break;
	  case 3: indicator(1,1) = 1.0; break; 
	  }
	
	hmm = indicator * Trans(jac);
	hmm *= (1.0 / (det));
	
	fel.AssignTensor(hmm, i, trans);
      }
      hx = Trans(shape) * x;  
      y = Trans(trans) * hx; 
      // Given x,  returns: y= 1/det [DT ; 0 \\ 0 ; DT ] [shape] x.vec 
      
    }
    
    
  };
  
    
  // DiffOpDivHDivSigma: Computes divergence of mapped shape functions
  //   This is used by the FE space for flux_evaluator[VOL]
   
  class DiffOpDivHDivSigma : public DiffOp<DiffOpDivHDivSigma>  {
    
    
  public:
    enum { DIM = 1 };
    enum { DIM_SPACE = 2 };
    enum { DIM_ELEMENT = 2 };
    enum { DIM_DMAT = 2 }; // it has two component the divergence
    enum { DIFFORDER = 1 };
    enum { DIM_STRESS = 4 }; //dim of the stress is 4
    enum { D = 2};
    
    template <typename FEL, typename SIP, typename MAT>
    static void GenerateMatrix (const FEL & bfel, const SIP & sip,
				MAT & mat, LocalHeap & lh)
    {
      const GGtrg & fel = 
	dynamic_cast<const GGtrg&> (bfel);
      
      int nd = fel.GetNDof();
      
      FlatMatrixFixWidth<D> div_shape(nd, lh);
      fel.CalcDivShape (sip.IP(), div_shape);
      
      
      Mat<D> jac = sip.GetJacobian();
      double det = fabs (sip.GetJacobiDet());
      
      mat = (1/det)* Trans(div_shape);
      //  [2 x ndofs ]
    }
    

    static void GenerateMatrix (const FiniteElement &,
				const MappedIntegrationPoint<D,D,Complex> & mip,
				SliceMatrix<Complex, ORDERING::ColMajor> & mat,
				LocalHeap & lh)  {
      
      throw Exception ("GenerateMatrix complex not supported, diffopdivhdivsym");
    }
    
    // The following allows you to write ‘div(sigma)’ rather than sigma.Deriv()
    static string Name() { return "div"; }

    
  };
  
  ////////////////////////////////////////////////////////////////////////////
  // DiffOpBoundIdHDiv3D:
  //   * Analogous to DiffOpIdVecHDivBoundary for standard Hdiv.
  //   * Calculates mapped trace element shape functions.
  //   * Its GenerateMatrix gives values of G n for each shape
  //     function G in the trace element GGnrm.
  //   * Used by the space GGfes in its evaluator[BND].
   
  
  class DiffOpBoundIdHDiv : public DiffOp<DiffOpBoundIdHDiv>
  {
    
  public:
    enum { DIM = 1 };
    enum { DIM_SPACE = 2 };
    enum { DIM_ELEMENT = 1 };
    enum { DIM_DMAT = 4 }; // 4 entries
    enum { DIFFORDER = 0 };
    enum { D = 2 };

    // This makes the boundary shape functions 2 x 2 matrix-valued:
    static Array<int> GetDimensions() { return Array<int> ({2, 2}); }

    
    template <typename FEL, typename SIP, typename MAT>
    static void GenerateMatrix (const FEL & bfel, const SIP & sip,
				MAT & mat, LocalHeap & lh)
    {
      const GGnrm & fel = 
	dynamic_cast<const GGnrm&> (bfel);
      
      int nd = fel.GetNDof();
      
      FlatMatrix<> shape(nd,D, lh);  // [ ndofs x 2 ]
      fel.CalcShape (sip.IP(), shape);
      
      // For the case of Hdiv, DiffOpIdVecHDivBoundary returns:
      
      // mat = [ n[0] ] * [ s[0], s[1], s[2] , ... s[n] ] = n[0]s[0] n[0]s[1] .....
      //       [ n[1] ]                                     n[1]s[0] n[1]s[1] .....
      
      
      // scaled normal vector is given by
      //    scaled_nv = [n(0), n(1)]^T
      auto scaled_nv = (1.0/sip.GetJacobiDet()) *sip.GetNV(); 
      
      // For our GG space:
      //   mat =[ n[0] *shape(j,0) ]  of size:  4 x ndofs
      //        [ n[1] *shape(j,0) ]
      //        [ n[0] *shape(j,1) ]
      //        [ n[1]* shape(j,1) ]      
      
      for (int j = 0; j < nd; j++ )
	{
	  mat(0, j) =  shape(j,0) * scaled_nv(0);
	  mat(1, j) =  shape(j,0) * scaled_nv(1);
	  mat(2, j) =  shape(j,1) * scaled_nv(0);
	  mat(3, j) =  shape(j,1) * scaled_nv(1);
	}
      
    }
  };
  
  
  
  
}


#endif
