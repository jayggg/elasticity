#ifndef FILE_GGFES_HPP
#define FILE_GGFES_HPP

#include <fem.hpp>
#include <comp.hpp>
#include "GGtrg.hpp"

namespace ngcomp {

  class GGfes : public FESpace {
  
  private:

    // Element is P_k + curl(b*curl A_k), store value of k in _k:
    int _k;
    
    // Option to remove bubbles.
    bool nobubbles;

    // Hybridized (discontinuous) space or conforming space.
    bool discont;
    				      
    // 1st interior dof#, for each element
    Array<int> first_element_dof;

    // 1st wirebasket dof#, for each edge
    Array<int> first_wb_dof;

    // 1st interface dof# that is not a wirebasket dof, for each edge 
    Array<int> first_facet_dof;
    
    // refined meshes have non fine_facet edges which do not contribute to dofs
    Array<bool> fine_facet;

    int ndof,  nedges, n_cell;
    
  public:

    GGfes (shared_ptr<MeshAccess> ama, const Flags & flags);
    virtual ~GGfes () {;}

    virtual string GetClassName () const override {return "GGstress";}

    virtual void Update(LocalHeap & lh) override;
    virtual void UpdateDofTables() override;  
    virtual void UpdateCouplingDofArray() override;

    virtual size_t GetNDof () const override  { return ndof; }
    
    virtual void GetDofNrs (ElementId ei, Array<int> & dnums) const override;

    virtual FiniteElement & GetFE (ElementId ei, Allocator & lh) const override;
    
    IntRange GetElementDofs (int nr) const {
      return IntRange(first_element_dof[nr],first_element_dof[nr+1]);
    }
    
    IntRange GetWBEdgeDofs(int ednr) const {
      return IntRange(first_wb_dof[ednr], first_wb_dof[ednr+1]);
    }
    
    IntRange GetFacetDofs(int ednr) const {
      return IntRange(first_facet_dof[ednr], first_facet_dof[ednr+1]);
    }

    virtual void GetVertexDofNrs(int vnr, Array<int> & dnums) const override {
      dnums.SetSize0();
    }

    virtual void GetEdgeDofNrs (int ednr, Array<int> & dnums) const override {
      dnums.SetSize0();
      if (discont) return; 
      dnums += GetWBEdgeDofs(ednr);
      dnums += GetFacetDofs  (ednr);
    }

    virtual void GetInnerDofNrs (int elnr, Array<int> & dnums) const override {
      dnums = GetElementDofs (elnr);
    }
    
  };

}    

#endif
