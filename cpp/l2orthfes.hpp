#ifndef FILE_L2ORTHFESPACE_HPP
#define FILE_L2ORTHFESPACE_HPP

/* 
   Space L2orth contains polinomial w in P_{k} such that
   ( w, p) = 0 for all p in P_{k-1} on each element
   here k = order is >=1 .
*/

using namespace ngcomp;

namespace ngcomp {
  
  class l2orthFEspace : public FESpace  {
    
    int order;
    int ndof;    
    Array<int> first_cell_dof;
    
  public:
    
    l2orthFEspace (shared_ptr<MeshAccess> ama, const Flags & flags);
    
    ~l2orthFEspace () {;}
    
    virtual string GetClassName () const { return "l2orthfes"; }
    
    virtual void Update(LocalHeap & lh);
    virtual size_t GetNDof () const { return ndof; }
    
    virtual void GetDofNrs (ElementId ei, Array<int> & dnums) const ;
    
    virtual FiniteElement & GetFE (ElementId ei, Allocator & lh) const;
  };
  
  
}

#endif 
