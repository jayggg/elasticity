#include <comp.hpp>    
#include "l2orth3d.hpp"
#include "l2orthfes3d.hpp"

namespace ngcomp {
  
  l2orthFEspace3d::l2orthFEspace3d (shared_ptr<MeshAccess> ama,
				    const Flags & flags)
    : FESpace (ama, flags) {
    
    order = int(flags.GetNumFlag ("order", 1));
    if (order==0) {
      throw Exception("\n *Error: l2orthfes3d.Order() must be greater than 0 \n"); };
    evaluator[VOL] = make_shared<T_DifferentialOperator<DiffOpId<3>>>();
    flux_evaluator[VOL] = make_shared<T_DifferentialOperator<DiffOpGradient<3>>>();
    evaluator[BND] = make_shared<T_DifferentialOperator<DiffOpIdBoundary<3>>>();
    integrator[VOL] = GetIntegrators().CreateBFI("mass", ma->GetDimension(), make_shared<ConstantCoefficientFunction>(1));
  }
  
  void l2orthFEspace3d::Update(LocalHeap & lh) {
    
    int n_cell = ma->GetNE();  
    int ii = 0;
    
    first_cell_dof.SetSize (n_cell+1);
    for (int i = 0; i < n_cell; i++, ii+=(order+1)*(order+2)/2)
      first_cell_dof[i] = ii;
    first_cell_dof[n_cell] = ii;
    ndof = ii;
  }
  
  void l2orthFEspace3d::GetDofNrs (ElementId ei, Array<int> & dnums) const {
    int elnr = ei.Nr();
    dnums.SetSize(0);
    if (!DefinedOn (ei) || ei.VB()!=VOL) return;
    int first = first_cell_dof[elnr];
    int next  = first_cell_dof[elnr+1];
    for (int i = first; i < next; i++)  dnums.Append (i);   
  }
  
  FiniteElement & l2orthFEspace3d::GetFE (ElementId ei, Allocator & lh) const {
    int elnr = ei.Nr();
    Ngs_Element ngel = ma->GetElement(ei);
    ELEMENT_TYPE eltype = ngel.GetType();
    
    switch(eltype) {
    
    case ET_TET: {    
      L2orthTet * tetra = new (lh) L2orthTet(order);
      ArrayMem<int, 4> vvnums;	  
      vvnums = ma->GetElVertices(ei);	
      tetra->SetVertexNumbers(vvnums);         
    
      return *tetra;
    
    }
    default:
      throw Exception ("illigal element in l2orth3d:GetFE");
    }
  }
  
  //const FiniteElement &
  //l2orthFEspace3d::GetSFE(int selnr, LocalHeap & lh) const {;}
    
  static RegisterFESpace<l2orthFEspace3d> initifes ("l2orthfes3d");
}
